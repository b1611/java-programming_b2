package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CollectionsIntro {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 54, 4, 1, 6, 5, 16, 6, 5));
//        System.out.println(sortArrayListDescending(list));
//        System.out.println(sortArrayListAscending(list));

        Collections.sort(list);
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
//        Collections.shuffle(list);
//        System.out.println(list);
        Collections.swap(list, 0, list.size()-1);
        Collections.swap(list, 1, list.size()-2);
        System.out.println(list);

        System.out.println(Collections.frequency(list, 100));
        int frequency = Collections.frequency(list, 5);
        Collections.replaceAll(list, 6, 666);
        System.out.println(list);
    }

    public static ArrayList<Integer> sortArrayListDescending(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i) > list.get(j)) {
//                      a              b
                    int temp;
                    temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }

    public static ArrayList<Integer> sortArrayListAscending(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i) < list.get(j)) {
//                      a              b
                    int temp;
                    temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }
}

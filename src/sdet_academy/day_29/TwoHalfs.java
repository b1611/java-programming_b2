package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class TwoHalfs {
    public static void main(String[] args) {
/**
 * ArrayList of Integers
 * First half should be reversed
 * Second half - shuffle
 * if ArrayList is odd leave the middle element there
 */
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 7, 8, 9, 10));
        System.out.println(halfArrayMethod(list));         //     i  i i  i   j  j  j  j
    }

    public static ArrayList<Integer> halfArrayMethod(ArrayList<Integer> list) {
        ArrayList<Integer> beginning = new ArrayList<>();
        ArrayList<Integer> end = new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        int middleElement;
        for (int i = 0, j = list.size() / 2; i < list.size() / 2; i++, j++) {
            if (list.size() % 2 == 0) {
                beginning.add(list.get(i));
                end.add(list.get(j));
            } else {
                beginning.add(list.get(i));
                end.add(list.get(j + 1));
            }
        }
        Collections.reverse(beginning);
        Collections.shuffle(end);
        if (list.size() % 2 == 1) {
            middleElement = list.get(list.size() / 2);
            beginning.add(middleElement);
        }
        for (Integer each : beginning) {
            result.add(each);
        }
        for (Integer each : end) {
            result.add(each);
        }
        return result;
    }
}

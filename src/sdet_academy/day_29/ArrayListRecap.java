package sdet_academy.day_29;

import java.util.ArrayList;

public class ArrayListRecap {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
     //   List<String> list1 = new ArrayList<>(); - List is a parent of the ArrayList, will learn more during the
        // collections class

        list.add("Radu");
        list.add("Slava");
        System.out.println(list);
//        list.remove(0);
        list.remove("Radu");
        System.out.println(list);
        System.out.println(list.get(0));
        list.set(0, "Luba");
        System.out.println(list);
        list.clear();
        System.out.println(list);



    }
}

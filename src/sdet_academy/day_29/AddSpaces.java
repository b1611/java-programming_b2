package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;

public class AddSpaces {
    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>(Arrays.asList("car", "dog", "cat", "lime"));
        System.out.println(list);

        //result [c a r, c a t, d o g, l i m e]

        //  System.out.println(spaceSTR("car"));
        System.out.println(space(list));

    }
    public static ArrayList <String> space (ArrayList<String> list){

        for (int i = 0; i < list.size() ; i++) {
            list.set(i, spaceSTR(list.get(i)));
        }
        return list;
    }

    public static String spaceSTR(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            result += str.charAt(i) + " ";
        }
        return result.trim();
    }
}

package sdet_academy.day_29;

import sdet_academy.day_28.ConvertStringIntoListMethod;

import java.util.ArrayList;

public class MakeVowelsToUpperCase {
    public static void main(String[] args) {
        /**
         * [a,b,c,d,e,f,g,h,i]
         * [A,b,c,d,E,f,g,h,I]
         * vowels: a, e, i, o, u, y
         */
        ArrayList<String> list = ConvertStringIntoListMethod.convertStringIntoArrayListMethod("a, b, c, d, e, f, g, h, i");

        long startTimeModifyingList = System.nanoTime();
        System.out.println(upperCaseVowelsV1(list));
        long endTimeModifyingList = System.nanoTime();
        System.out.println("Modifying existing list will take V1 " + (endTimeModifyingList-startTimeModifyingList) + " nanoSec");


        long startTimeModifyingList2 = System.nanoTime();
        System.out.println(upperCaseVowelsV2(list));
        long endTimeModifyingList2 = System.nanoTime();
        System.out.println("Modifying existing list will take V2 " + (endTimeModifyingList2-startTimeModifyingList2) + " nanoSec");

    }

    public static ArrayList<String> upperCaseVowelsV1(ArrayList<String> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals("a") ||
                    list.get(i).equals("e") ||
                    list.get(i).equals("i") ||
                    list.get(i).equals("o") ||
                    list.get(i).equals("u") ||
                    list.get(i).equals("y")) {
                list.set(i, list.get(i).toUpperCase());
            }
        }
        return list;
    }

    public static ArrayList<String> upperCaseVowelsV2(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).equals("a") ||
                    list.get(i).equals("e") ||
                    list.get(i).equals("i") ||
                    list.get(i).equals("o") ||
                    list.get(i).equals("u") ||
                    list.get(i).equals("y")){
                result.add(list.get(i).toUpperCase());
            }else{
                result.add(list.get(i));
            }
        }
        return result;
    }

}

package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;

public class PalindromeEachElement {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("civ ic", "rAdar", "cat", "dog", "noon", "moon"));
        //result [civ ic, rAdarm, noon]
        System.out.println(isPalindrome("moon"));
        System.out.println(palindromeMethod(list));
        System.out.println(palindromeMethod2(list));

    }
    public static ArrayList<String> palindromeMethod2(ArrayList<String> list){

        for (int i = 0; i < list.size() ; i++) {
            if(!isPalindrome(list.get(i))){
                list.remove(list.get(i));
                i--;
                //         list.remove(i);
                //       i--;
            }

        }
        return list;
    }

    public static ArrayList<String> palindromeMethod(ArrayList<String> list){
        ArrayList<String> result=new ArrayList<>();

        for (String each : list) {
            if(isPalindrome(each)){
                result.add(each);
            }
        }return result;
    }

    public static boolean isPalindrome(String str) {

        str = str.toLowerCase().replace(" ", "");

        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;

            }
        }return true;
    }
}

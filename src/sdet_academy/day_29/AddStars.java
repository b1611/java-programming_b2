package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;

public class AddStars {
    //[car, cat, dog, lime] - ArraylistOf Strings
    //[*car*, *cat*, *dog*, *lime*]
    public static void main(String[] args) {
        ArrayList<String> list=new ArrayList<>(Arrays.asList("car", "cat", "dog", "lime"));
        String character="$";
        System.out.println(addStars(list,character));
    }
    public static ArrayList<String>addStars(ArrayList<String> list,String ch){
        for (int i = 0; i < list.size(); i++) {
            list.set(i,ch+list.get(i)+ch);

        }
        return list;
    }
}

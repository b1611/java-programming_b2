package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RemoveDuplicates {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Toyota", "Ford", "Honda", "Toyota", "Mercedes", "Mercedes", "Ford"));
        /**
         * [Ford, Honda, Mercedes, Toyota]
         */

        System.out.println(removeDuplicates(list));
        System.out.println(removeDuplicatesV2(list));
    }

    public static ArrayList<String> removeDuplicates(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            if (!result.contains(list.get(i))) {
                result.add(list.get(i));
            }
        }
        Collections.sort(result);
        return result;
    }

    public static ArrayList<String> removeDuplicatesV2(ArrayList<String> list) {
        int count;
        for (int i = 0; i < list.size(); i++) {
            count=0;
            for (int j = 0; j < list.size(); j++) {
                if(list.get(i).equals(list.get(j))){
                    count++;
                }
            }
            if(count!=1){
                list.remove(i);
            }
        }
        Collections.sort(list);
        return list;
    }
}

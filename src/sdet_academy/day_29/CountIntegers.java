package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Collections;

public class CountIntegers {
    public static void main(String[] args) {
        int[] num = {2, 31, 3, 1, 23, 12, 6, 5, 64, 65, 4, 6, 3, 3, 2, 1, 3, 2, 3};

        //  ArrayList <Integer> list = new ArrayList<>(Arrays.asList(2,31,3,1,23,12,6,5,64,65,4,6,3,3,2,1,3,2,3));

        int target = 12;
        System.out.println(numInt(num, target));

        //  System.out.println(numInt(list, target));

    }

    public static int numInt(int[] num, int target) {
        ArrayList<Integer> num2 = new ArrayList<>();
        for (Integer each : num) {
            num2.add(each);

        }

        return Collections.frequency(num2, target);
    }
}


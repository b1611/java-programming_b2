package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.List;

public class RawArrayList {
    public static void main(String[] args) {
        /**
         * RAW ArrayList
         */

        ArrayList list = new ArrayList();
        list.add("Hello");
        list.add('z');
        list.add(10);
        list.add(true);
        list.add(20.58);
        System.out.println(list);

        List list2 = new ArrayList<>();
        list2.add("Hello");
        list2.add('z');
        list2.add(10);
        list2.add(true);
        list2.add(20.58);
        System.out.println(list2);
    }
}

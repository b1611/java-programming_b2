package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RemoveEven {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        System.out.println(replaceAllEven(list));
    }

    public static ArrayList<Integer> replaceAllEven(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0) {
                Collections.replaceAll(list, list.get(i), 0);
            }
        }
        return list;
    }
}

package sdet_academy.day_29;

import sdet_academy.day_28.ConvertStringIntoListMethod;
import java.util.ArrayList;

public class GetMiddleCharacters {
    public static void main(String[] args) {

        /**
         * [Java, C#, C++, Kotlin, Ruby, Pascal, JavaScript, Scala, Groovy, PHP, TypeScript, C]
         * list.get(i)
         * [av, 2, +, tl, ub....1]
         */

        ArrayList<String> list = ConvertStringIntoListMethod.convertStringIntoArrayListMethod("Java, C#, C++, Kotlin, Ruby, Pascal, JavaScript, Scala, Groovy, PHP, TypeScript, C");
        System.out.println(getMiddleCharacters(list));
    }

    public static ArrayList<String> getMiddleCharacters(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            String temp = list.get(i);
            if (temp.length() == 1) {
                result.add("1");
            } else if (temp.length() == 2) {
                result.add("2");
            } else if (temp.length() % 2 == 0) {
                result.add("" + temp.charAt(temp.length() / 2 - 1) + temp.charAt(temp.length() / 2));
            } else {
                result.add("" + temp.charAt(temp.length() / 2));
            }
        }

        return result;
    }
}

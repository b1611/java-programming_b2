package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class CountFrequency {
    public static void main(String[] args) {

        /**
         * Create a method, that will count the frequency of the character in our str
         */
        String str = "Java is not JavaScript";
        char targetCharacter = 'a';
        // result: 4
        System.out.println(countCharacter(str, targetCharacter));

    }
    public static int countCharacter(String str, char target){
        ArrayList<String> list = new ArrayList<>(Arrays.asList(str.split("")));
        return Collections.frequency(list, "" + target);
    }
}

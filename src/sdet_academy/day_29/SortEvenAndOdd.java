package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SortEvenAndOdd {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        /**
         * [2,4,6,8,1,3,5,7,9]
         */
        System.out.println(sortEvenAndOddMethod(list));
        System.out.println(sortEvenAndOddMethodV2(list));
    }
    public static ArrayList<Integer> sortEvenAndOddMethod(ArrayList<Integer> list){
        ArrayList<Integer> even = new ArrayList<>();
        ArrayList<Integer> odd = new ArrayList<>();
        ArrayList<Integer> result = new ArrayList<>();
        Collections.sort(list);

        for (Integer each : list) {
            if(each%2==0){
                even.add(each);
            }else {
                odd.add(each);
            }
        }

        for (Integer each : even) {
            result.add(each);
        }

        for (Integer each : odd) {
            result.add(each);
        }
        return result;
    }

    public static ArrayList<Integer> sortEvenAndOddMethodV2(ArrayList<Integer> list){
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                if(list.get(i)%2==0){
                    int temp;
                    temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }
}

package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SwapThreeElements {
    public static void main(String[] args) {
        ArrayList<Integer> list=new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
        // RESULT (                                           9 2 3 4 1 6 7 8 5
        System.out.println(swapThreeElements(list));
    }

    public static ArrayList<Integer> swapThreeElements(ArrayList<Integer> list){

        if(list.size()%2==1&& list.size()>=3){
            Collections.swap(list,0,list.size()/2);
            Collections.swap(list,0,list.size()-1);
            return list;
        }
        return null;

    }
}

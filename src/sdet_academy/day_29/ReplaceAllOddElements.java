package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;

public class ReplaceAllOddElements {
    public static void main(String[] args) {
        /**
         * [1,2,3,4,5,6,7,8,9]
         * [2,2,4,4,6,6,8,8,8]
         * [1,1,1,2,3,5,6,7,9]
         * [2,2,2,2,6,6,6,6,6]
         */
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
        System.out.println(replaceOdd(list));

    }
    public static ArrayList<Integer> replaceOdd(ArrayList<Integer> list){

        for (int i = 0; i < list.size()-1; i++) {
            if(list.get(i)%2==1){
                list.set(i, list.get(i+1));
            }
            list.set(list.size()-1, list.get(list.size()-2));
        }
        return list;
    }
}

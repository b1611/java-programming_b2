package sdet_academy.day_29;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class CheckBooleanArrayList {
    public static void main(String[] args) {
        /**
         * [true, false, false, true, false, true]
         * [8, 3, 7, 6, 1, 6] - return randomly generated even number for true and odd number for false
         */
        ArrayList<Boolean> list = new ArrayList<>(Arrays.asList(true, false, false, true, false, true));
        System.out.println(fillUpRandomNumbers(list));

    }

    public static ArrayList<Integer> fillUpRandomNumbers(ArrayList<Boolean> list){
        ArrayList<Integer> result = new ArrayList<>();
        Random random = new Random();
        int odd;
        int even;

        for (int i = 0; i < list.size(); i++) {
            odd = random.nextInt(20); //7
            even = random.nextInt(20); //7 +1
            if(odd%2==0){
                odd+=1;
            }
            if(even%2==1){
                even+=1;
            }

            if(list.get(i)==true){
                result.add(even);
            }else {
                result.add(odd);
            }
        }
        return result;
    }
}

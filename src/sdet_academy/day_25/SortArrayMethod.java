package sdet_academy.day_25;

import java.util.Arrays;

public class SortArrayMethod {
    public static void main(String[] args) {
        /**
         * Create a custom method that will sort an array of int in the descending order
         * [56,2,63,63,62,45,0,4]
         */

        int[] numArr = {56,2,63,63,62,45,0,4};
        int[] numArr2 = {64,1,3,13,1,6,4,1,35,7,641};
        int[] numArr3 = {1,3,1,654,5,61,32,168,64,16};

        System.out.println(Arrays.toString(sortArrayDescending(numArr)));
        System.out.println(Arrays.toString(sortArrayDescending(numArr2)));
        System.out.println(Arrays.toString(sortArrayDescending(numArr3)));
        System.out.println(Arrays.toString(sortArrayAscending(numArr)));
        System.out.println(Arrays.toString(sortArrayAscending(numArr2)));
        System.out.println(Arrays.toString(sortArrayAscending(numArr3)));

    }

    public static int[] sortArrayDescending(int[] arr){

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if(arr[i]>arr[j]){
                    int temp;
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    public static int[] sortArrayAscending(int[] arr){

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if(arr[i]<arr[j]){
                    int temp;
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
}

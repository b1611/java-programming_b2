package sdet_academy.day_25;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveOddElement {
    public static void main(String[] args) {
        /**
         * Remove element from an array if the element length is odd.
         */

        String[] strArr = {"Red", "Yellow", "Blue", "Green", "Purple", "Pink", "Black", "White", "Brown", "Grey"};

//         Streaming through an array - will learn later
        long startTimeModifyingList = System.nanoTime();
        List<String> collect = Arrays.stream(strArr).filter(x -> x.length() % 2 == 0).collect(Collectors.toList());
        System.out.println(collect);
        long endTimeModifyingList = System.nanoTime();
        System.out.println("Modifying existing list will take " + (endTimeModifyingList-startTimeModifyingList) + " nanoSec");
//                          [Yellow, Blue, Purple, Pink, Grey] - String[]


        long startTimeModifyingArray = System.nanoTime();
        System.out.println(Arrays.toString(removeOddElementMethodLonger(strArr))); //106333 nanosec > 0.000106333 sec
        long endTimeModifyingArray = System.nanoTime();
        System.out.println("Modifying existing array will take " + (endTimeModifyingArray-startTimeModifyingArray) + " nanoSec");


        long startTimeModifyingArrayTwo = System.nanoTime();
        System.out.println(Arrays.toString(removeOddElementMethodShorter(strArr)));
        long endTimeModifyingArrayTwo = System.nanoTime();
        System.out.println("Modifying existing arrayTwo will take " + (endTimeModifyingArrayTwo-startTimeModifyingArrayTwo) + " nanoSec");


    }

    public static String[] removeOddElementMethodShorter(String[] strArr){
        String temp = "";
        for (int i = 0; i < strArr.length; i++) {
            if(strArr[i].length()%2==0){
                temp+= strArr[i] + ",";
            }
        }
        return temp.split(",");
    }

    public static String[] removeOddElementMethodLonger(String[] strArr){
        int count =0;

        for (String eachString : strArr) {
            if(eachString.length()%2==0){
                count++;
            }
        }

        String[] resultArr = new String[count];

        for (int i = 0, j = 0; i < strArr.length; i++) {
            if(strArr[i].length()%2==0){
                resultArr[j++] = strArr[i];
            }
        }

        return resultArr;
    }
}

package sdet_academy.day_25;

import java.util.Arrays;

public class IsAnagramMethod {
    public static void main(String[] args) {

        /**
         * create a custom method that will check if 2 Strings are anagrams
         * silent - listen
         */

        String strOne= "Sil ent";
        String strTwo= "listEn";

        boolean anagramMethod = isAnagramMethod(strOne, strTwo);
        System.out.println(anagramMethod);

    }

    public static boolean isAnagramMethod(String strOne, String strTwo){
//        boolean result;
        char[] charStrOne = strOne.toLowerCase().replace(" ", "").toCharArray();
        Arrays.sort(charStrOne);
        char[] charStrTwo = strTwo.toLowerCase().replace(" ", "").toCharArray();
        Arrays.sort(charStrTwo);

        // OPTION 1
//        if(Arrays.equals(charStrOne, charStrTwo)){
//            result = true;
//        }else {
//            result = false;
//        }
// return result;

        // OPTION2
        return Arrays.equals(charStrOne, charStrTwo);
    }
}

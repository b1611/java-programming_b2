package sdet_academy.day_25;

import java.util.Arrays;

public class DoubleEachElementMethod {
    public static void main(String[] args) {
        int[] arr = {1, 0, 3, 4, 5};
        //          [ 2,0,6,8,10]

        System.out.println(Arrays.toString(doubleEachMethod(arr)));

    }

    public static int[] doubleEachMethod(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * 2;
        }
        return arr;
    }
}

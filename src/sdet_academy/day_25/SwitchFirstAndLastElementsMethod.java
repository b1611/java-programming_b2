package sdet_academy.day_25;

import java.util.Arrays;

public class SwitchFirstAndLastElementsMethod {
    public static void main(String[] args) {
        int[] arr = {1, 0, 3, 4, 5};
        //          [5,0,3,4,1]

        System.out.println(Arrays.toString(switchFirstAndLast(arr)));
    }

    public static int[] switchFirstAndLast(int[] arr){
        int temp; // Slava's method
        temp = arr[0];
        arr[0] = arr[arr.length-1];
        arr[arr.length-1] = temp;
        return arr;
    }
}

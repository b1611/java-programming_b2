package sdet_academy.day_25;

public class ReverseString {
    public static void main(String[] args) {
        String str = "Java is fun";

        String reverseStr = reverseCustomMethod(str);
        System.out.println(reverseStr);
        System.out.println(reverseCustomMethod(str));
        System.out.println(reverseCustomMethodBuilder(str));
    }

    public static String reverseCustomMethod(String stringToReverse){
        String result = "";
        for (int i = stringToReverse.length()-1; i >=0; i--) {
            result+=stringToReverse.charAt(i);
        }
        return result;
    }

//    public static String reverseCustomMethodBuilder(String stringToReverse){
//        StringBuilder stringBuilder = new StringBuilder(stringToReverse);
//        StringBuilder result = stringBuilder.reverse();
//        return result.toString();
//    }

    public static String reverseCustomMethodBuilder(String stringToReverse){
        StringBuilder stringBuilder = new StringBuilder(stringToReverse);
        return stringBuilder.reverse().toString();
    }

}

package sdet_academy.day_25;

import java.util.Arrays;

public class KeepOddElementsMethod {
    public static void main(String[] args) {


        int[] numArr = {1,5,8,4,6,9,4};
        //       int   [1,5,9]

        System.out.println(Arrays.toString(keepOddOnly(numArr)));

    }

    public static int[] keepOddOnly(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 1) {
                count++;
            }
        }


        int[] resultArr = new int[count];

        for (int i = 0, j = 0; i < arr.length; i++) {
            if(arr[i]%2==1){
                resultArr[j++] = arr[i];
            }
        }
        return resultArr;
    }
}

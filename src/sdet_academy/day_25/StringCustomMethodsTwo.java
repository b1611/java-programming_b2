package sdet_academy.day_25;

public class StringCustomMethodsTwo {
    public static void main(String[] args) {
        String str = "HELLO"; //HellO
        String result = upperCaseFirstAndLastOnly(str);
        System.out.println(result);
    }

    public static String upperCaseFirstAndLastOnly(String str) {

        // OPTION 1
//        if (str.isEmpty() || str.isBlank()) {
//            return null;
//        } else{
//            result += ("" + str.charAt(0)).toUpperCase();// H...........O
//            result += str.substring(1, str.length() - 1); //Hell
//            result += ("" + str.charAt(str.length() - 1)).toUpperCase();
//
//            return result;
//        }

//          OPTION2
        if (!str.isEmpty() && !str.isBlank()) {
            String result = "";
            str = str.toLowerCase(); // hello
            result += ("" + str.charAt(0)).toUpperCase();// H...........O
            result += str.substring(1, str.length() - 1); //Hell
            result += ("" + str.charAt(str.length() - 1)).toUpperCase();

            return result;
        }
        return null;
    }
}

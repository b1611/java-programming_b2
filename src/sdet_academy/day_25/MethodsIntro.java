package sdet_academy.day_25;

public class MethodsIntro {
    public static void main(String[] args) {
        String word = "Python";

        char firstCharacter = printFirstCharacter(word);
        System.out.println(firstCharacter);

        printAAndZ();
        int anInt = getInt();
        System.out.println(anInt);

    }


    public static char printFirstCharacter(String str) {
        return str.charAt(0);
    }

    public static void printAAndZ(){
        System.out.println("A");
        System.out.println("Z");
    }

    public static int getInt(){
        int a = 100;
        return a;
    }

}

package sdet_academy.day_25;

import java.util.Arrays;

public class StringCustomMethods {

    public static char getLastCharacter(String str){
        return str.charAt(str.length()-1);
    }

//    public static void getLastCharacter(String str){ // not a good practice since we cannot introduce the local variable to it and cannot reuse in our code
//        System.out.println(str.charAt(str.length() - 1));
//    }


    public static String returnWithoutLastCharacter(String str){
        return str.substring(0, str.length()-1);
    }

    public static String middleCharacter(String str){
        //  OPTION 1
//        String result = "";
//        if(str.length()%2==1){
//            result ="" +  str.charAt(str.length()/2);
//        }else {
//            result ="" + str.charAt(str.length()/2-1) + str.charAt(str.length()/2);
//        }
//        return result;


//          OPTION 2
//        if(str.length()%2==1){
//            return "" +  str.charAt(str.length()/2);
//        }else if(str.length()%2==0){
//            return "" + str.charAt(str.length()/2-1) + str.charAt(str.length()/2);
//        }
//        return null;

        //OPTION 3
        if(str.length()%2==1){
            return "" +  str.charAt(str.length()/2);
        }else{
            return "" + str.charAt(str.length()/2-1) + str.charAt(str.length()/2);
        }
    }


    public static void main(String[] args) {
        String str = "Hello World";
        String str2 = "Java";
        String str3 = "Pythonnnnnn";
        String str4 = "The end";


        char lastCharacter = getLastCharacter(str);
        char lastCharacterStr2 = getLastCharacter(str2);
        char lastCharacterStr3 = getLastCharacter(str3);
        char lastCharacterStr4 = getLastCharacter(str4);

        System.out.println(lastCharacter);
        System.out.println(returnWithoutLastCharacter(str));


        String[] arr = new String[5];

        System.out.println(Arrays.toString(arr));

    }
}

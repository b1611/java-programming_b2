package sdet_academy.day_25;

public class KeepOnlyUniqueMethod {
    public static void main(String[] args) {
        String str = "Java is my love";
        //Jismyloe

        System.out.println(keepOnlyOneOfEach(str));
        System.out.println(keepOnlyUnique(str));

    }

    public static String keepOnlyOneOfEach(String str) {

        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (!result.contains("" + str.charAt(i))) {
                result += str.charAt(i);
            }
        }
        return result;
    }

    public static String keepOnlyUnique(String str) {
        int count;
        String result = "";

        for (int i = 0; i < str.length(); i++) {
            count=0;
            for (int j = 0; j < str.length(); j++) {
                if(str.charAt(i)==str.charAt(j)){
                    count++;
                }
            }
            if(count==1){
                result+=str.charAt(i);
            }
        }
        return result;
    }
}

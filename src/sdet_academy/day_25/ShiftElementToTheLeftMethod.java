package sdet_academy.day_25;

import java.util.Arrays;

public class ShiftElementToTheLeftMethod {
    public static void main(String[] args) {
        int[] arr = {1, 0, 3, 4, 5};
        //    result [0,3,4,5,1]

        System.out.println(Arrays.toString(shiftToTheLeft(arr)));

        System.out.println(1%5);
    }

    public static int[] shiftToTheLeft(int[] arr){
        int[] replace = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            replace[i] = arr[(i+1)% arr.length];
            //    i = 0             1 % 5 = 1
            //    i = 1             2 % 5 = 2
            //    i = 2             3 % 5 = 3
            //    i = 3             4 % 5 = 4
            //    i = 4             5 % 5 = 0
        }
        return replace;
    }

//    public static int[] shiftToTheLeft(int[] arr){
//        int[] result = new int[arr.length];
//
//        for (int i = 0; i < arr.length-1; i++) {
//            result[i] = arr[i+1]; //[0, 3, 4, 5, 0]
//        }
//        result[result.length-1] = arr[0];
//
//        return result;
//    }
}

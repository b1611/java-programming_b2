package sdet_academy.day_25;

import java.util.Arrays;

public class ReverseArray {
    public static void main(String[] args) {
        /**
         * create a method that will sort an array
         */
        int[] numArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        System.out.println(Arrays.toString(reverseArrayInt(numArr)));

        int[][] mdNumArr = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
                {10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
                {100,200,300,5000}
        };

        // OPTION 1 INVESTIGATE
//        for (int[] eachArray : mdNumArr) {
//            int[] temp = reverseArrayInt(eachArray);
//            eachArray = temp;
//        }

        for (int i = 0; i < mdNumArr.length; i++) {
            mdNumArr[i] = reverseArrayInt(mdNumArr[i]);
        }

        System.out.println(Arrays.deepToString(mdNumArr));

    }

    public static int[] reverseArrayInt(int[] numArr) {
        int[] resultArr = new int[numArr.length];
        for (int i = 0, j = numArr.length - 1; i < numArr.length; i++, j--) {
            resultArr[i] = numArr[j];
        }
        return resultArr;
    }
}

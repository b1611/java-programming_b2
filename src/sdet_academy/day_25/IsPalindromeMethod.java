package sdet_academy.day_25;

public class IsPalindromeMethod {
    public static void main(String[] args) {

        String str = "Civ ic";
        System.out.println(isPalindromeMethod(str));

    }

    public static boolean isPalindromeMethod(String str) {
        boolean isPalindrome = true;
        str = str.toLowerCase().replace(" ", "");
        //OPTION 1
//        for (int i = 0; i < str.length()/2; i++) {
//            if(str.charAt(i)==str.charAt(str.length()-1-i)){
//                isPalindrome = true;
//            }else{
//                isPalindrome = false;
//                break;
//            }
//
//        }

        // OPTION 2
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;
            }
        }
        return isPalindrome;
    }
}

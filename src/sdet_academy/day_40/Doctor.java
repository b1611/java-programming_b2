package sdet_academy.day_40;

public class Doctor {
    public static String clinicName;
    public static boolean parkingFull;
    public String doctorName;
    public double doctorSalary;

    /**
     * Create 2 instance variables and 2 static variables
     * Create 2 instance methods and 2 static methods
     * Create 2  constractors - empty one and with all variables please
     */

    public Doctor() {
    }

    public Doctor(String clinicName, boolean parkingFull, String doctorName, double doctorSalary) {
        Doctor.clinicName = clinicName;
        Doctor.parkingFull = parkingFull;
        this.doctorName = doctorName;
        this.doctorSalary = doctorSalary;

    }

    public void acceptsPatient(String patientName) {
        System.out.println(doctorName + " is accepting " + patientName);
    }

    public void performsSurgery() {
        System.out.println(doctorName + " is performing surgery and gets payed " + doctorSalary);
    }

    public static void parkingIsFull() {
        System.out.println("the parking is full and hasParking is " + parkingFull);
    }

    public static void clinicClaim() {
        System.out.println(clinicName + " has an outstanding claim");
    }
}

package sdet_academy.day_40;

public class FootballPlayer {
    public String name;
    public static int playerNumber;

    public FootballPlayer(int playerNumber){
        FootballPlayer.playerNumber=playerNumber;
    }
    public FootballPlayer(){

    }
    public void attack(){

    }
    public static void play(String name){
        System.out.println("Playing");
    }
     public static void pass(){
        play("Liuba");
        FootballPlayer player = new FootballPlayer(5);
        player.attack();
     }

}

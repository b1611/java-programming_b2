package sdet_academy.day_40;

import static sdet_academy.day_40.Doctor.parkingFull;
import static sdet_academy.day_40.Doctor.parkingIsFull;

public class DoctorObject {
    public static void main(String[] args) {
        Doctor.clinicName = "Ortodont";
        parkingFull = true;
        System.out.println(Doctor.clinicName);
        System.out.println(parkingFull);

        Doctor doctorOne = new Doctor();
        doctorOne.doctorName = "Dr. Tatiana";
        doctorOne.doctorSalary = 1_200_000;
        System.out.println(doctorOne.doctorName);
        System.out.println(doctorOne.doctorSalary);

        Doctor.clinicClaim();
        parkingIsFull();
        doctorOne.acceptsPatient("Nick");
        doctorOne.performsSurgery();

        System.out.println("*******************************************");
        Doctor doctorTwo = new Doctor("SmileDirect", false, "Dr X", 1_300_000);

        doctorTwo.performsSurgery();
        doctorTwo.acceptsPatient("Jimmy");

        Doctor.clinicClaim();
        parkingIsFull();
        parkingIsFull();
    }
}

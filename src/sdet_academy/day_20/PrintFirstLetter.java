package sdet_academy.day_20;

public class PrintFirstLetter {
    public static void main(String[] args) {

        String arr[] = {"Sun", "Moon", "Clouds", "Snow"};
        char firstLetter;
        char lastLetter;

        for (int i=0; i<arr.length; i++){
            firstLetter = arr[i].charAt(0);
            lastLetter = arr[i].charAt(arr[i].length()-1);
            System.out.println(firstLetter); // - will return the first letter of each element
            System.out.println(lastLetter); //- will return the last letter of each element
            //                  Sun.charAt(arr[i].length()-1)
        }
    }
}

package sdet_academy.day_20;

public class CountFrequency {
    public static void main(String[] args) {
        int[] numArr = {1,1,2,6,4,5,4,3,1,2};
        int count = 0;
        String result = "";

        //              1 - 3
        //              1 - 3
        //              2 - 2
        //              6 - 1
        //              ......
        //              2 - 2

        for (int i = 0; i< numArr.length; i++){
            for (int j = 0; j< numArr.length; j++){
                if(numArr[i] == numArr[j]){
                    count++;
                }
            }
            result = numArr[i] + " - " + count;
            System.out.println(result);
            count = 0;
        }
    }
}

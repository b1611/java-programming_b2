package sdet_academy.day_20;

import java.util.Arrays;

public class SortTheWordsByLength {
    public static void main(String[] args) {

        String[] strArr = {"LubaJava", "IrynaPython", "SlavaC#", "RaduC++", "JamesJavaScript", "AngieRuby"};
        //                      8           12  ...                                                 9
        String temp;

        for (int i = 0; i<strArr.length; i++){
            for (int j = 0; j<strArr.length; j++){
                if(strArr[i].length()<strArr[j].length()){
                    temp = strArr[i];
                    strArr[i] = strArr[j];
                    strArr[j] = temp;
                }
            }

        }
        System.out.println(Arrays.toString(strArr));
    }
}

package sdet_academy.day_20;

import java.util.Arrays;

public class ReplaceVowels {
    public static void main(String[] args) {
        String arr[] = {"Sun", "Moon", "Clouds", "Snow"};
        //  [S*n, M**n, Cl**ds, Sn*w]
        //    if (arrChar[i] == 'a' || arrChar[i] == 'e' || arrChar[i] == 'i' || arrChar[i] == 'o' || arrChar[i] == 'u' || arrChar[i] == 'y')

//        String[] resultArr = new String[arr.length];
//
//        for (int i = 0; i< arr.length; i++){
//         resultArr[i] = arr[i].replace("a", "*")
//                 .replace("e", "*")
//                 .replace("i", "*")
//                 .replace("o", "*")
//                 .replace("u", "*")
//                 .replace("y", "*");
//        }
//        System.out.println(Arrays.toString(resultArr));

        for (int i = 0; i< arr.length; i++){
            arr[i] = arr[i].replace("a", "*")
                    .replace("e", "*")
                    .replace("i", "*")
                    .replace("o", "*")
                    .replace("u", "*")
                    .replace("y", "*");
        }
        System.out.println(Arrays.toString(arr));
    }
}

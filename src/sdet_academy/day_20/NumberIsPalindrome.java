package sdet_academy.day_20;

public class NumberIsPalindrome {
    public static void main(String[] args) {

        /**
         * Check if the number is palindrome
         */
        long num = 23456765432l; // int can handle the range of -2147483648 to 2147483647

//        String str = String.valueOf(num); - option 1
        String str = "" + num; // - option 2
        boolean isPalindrome = true;

        for (int i =0, j=str.length()-1; i<str.length(); i++, j--){
            if(str.charAt(i)!= str.charAt(j)){
                isPalindrome = false;
            }
        }

        System.out.println(isPalindrome);

    }
}

package sdet_academy.day_20;

import java.util.Arrays;

public class SortingAnArray {
    public static void main(String[] args) {
//
//        int[] arr = {1,3,4,5,6,7,8,0};
//        int temp;
//
//        for (int i = 0; i< arr.length; i++){
//            for (int j = 0; j<arr.length; j++){
//                if(arr[i]<arr[j]){ // ascending order. if arr[i]>arr[j] - descending order
////                  a        b
//                    temp = arr[i];
//                    arr[i] = arr[j];
//                    arr[j] = temp;
//                }
//            }
//        }
        String[] arr = {"Java","Python", "C++", "c#"};
        String temp;

        for (int i = 0; i< arr.length; i++){
            for (int j = 0; j<arr.length; j++){
                if(arr[i].length()>arr[j].length()){ // ascending order. if arr[i]>arr[j] - descending order
//                  a        b
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }


        System.out.println(Arrays.toString(arr));
    }
}

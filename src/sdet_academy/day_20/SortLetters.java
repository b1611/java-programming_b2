package sdet_academy.day_20;

import java.util.Arrays;

public class SortLetters {
    public static void main(String[] args) {
        char[] charArr = {'a', 'i', 'h', 'r', 'p', 'z', 'l'};
        //              [a, h, i, l, p, r, z]
//        char temp;
//
//        for (int i = 0; i<charArr.length; i++){
//            for (int j = 0; j<charArr.length; j++){
//                if(charArr[i]>charArr[j]){
//                  temp = charArr[i];
//                  charArr[i] = charArr[j];
//                  charArr[j] = temp;
//                }
//            }
//        }
        for (int i = 0; i<charArr.length; i++){
            for (int j = 0; j<charArr.length; j++){
                if(charArr[i]<charArr[j]){
                    //x          y
                    charArr[i] = (char) (charArr[i] + charArr[j]);
                    charArr[j] = (char) (charArr[i] - charArr[j]);
                    charArr[i] = (char) (charArr[i] - charArr[j]);
                }
            }
        }
        System.out.println(Arrays.toString(charArr));
    }
}

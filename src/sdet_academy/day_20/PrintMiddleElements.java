package sdet_academy.day_20;

public class PrintMiddleElements {
    public static void main(String[] args) {
        /**
         * Please create a program that will print the middle element if the array length is odd and 2 middle elements if
         * the array length is even
         * int[] arr = [1,2,3,4,5] - length is odd
         * print 3
         * [] = [5,6,7]
         * print 6
         *
         * [] = [4,6,5,8,7,9] - length is even
         * print 5,8
         * [] =  [8,9,7,4]
         * print 9, 7
         *
         * MAKE IT DYNAMIC
         * BONUS: Please make sure the array is not empty!
         * EXTRA BONUS POINTS: Solve it two different ways
         */
        int[] arr = {1, 2, 5, 8, 9, 4};
        String result = "";

        if (arr.length == 0) {
            result = "Invalid Array";
        } else if (arr.length % 2 == 0) {
            result = arr[arr.length / 2 - 1] + "," + arr[arr.length / 2];
        } else {
            result = "" + arr[arr.length / 2];
        }
        System.out.println(result);
    }
}

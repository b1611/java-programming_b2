package sdet_academy.day_20;

public class SubstructHalfs {
    public static void main(String[] args) {
        int[] arr = {5, 3, 4, 2, 1};
        //           0  1  2  3  4

        /**
         * add first half
         * add second half(if the length of array is odd, omit the middle character)
         * substract second half from first half
         */

        int firstHalf = 0;
        int secondHalf = 0;
        int result;

        for (int i = 0, j = arr.length / 2; i < arr.length; i++, j++) {
            if (arr.length % 2 == 0 && i < arr.length / 2) {
                firstHalf += arr[i];
                secondHalf += arr[j];
            } else if (arr.length % 2 == 1 && i < arr.length / 2) {
                firstHalf += arr[i];
                secondHalf += arr[j + 1];
            }
        }
        System.out.println(firstHalf);
        System.out.println(secondHalf);
        result = firstHalf - secondHalf;
        System.out.println(result);
    }
}

package sdet_academy.day_20;

public class FindMax {
    public static void main(String[] args) {
        /**
         * find the max element in array
         */
        int[] arr = {1, 2, 3, 4, 5, 10};
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (max < arr[i]) {
                max = arr[i];
            }
        }
        System.out.println(max);
    }
}

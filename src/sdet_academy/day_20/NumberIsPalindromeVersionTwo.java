package sdet_academy.day_20;

public class NumberIsPalindromeVersionTwo {
    public static void main(String[] args) {
        int num = 123454321;
        // reverseNum = 123...1 - reversing the original num
        int originalNum = num;
        boolean isPalindrome;
        int reversNum = 0;
        int remainder;

        while (num > 0) {
            remainder = num % 10; //any number remainder by 10 will give you the last digit
            reversNum = (reversNum * 10) + remainder;
            num = num / 10; //will remove the last digit from any number
            System.out.println("remainder = " + remainder);
            System.out.println("reversNum = " + reversNum);
            System.out.println("num = " + num);
        }

        if (originalNum == reversNum) {
            isPalindrome = true;
        } else {
            isPalindrome = false;
        }

        System.out.println(isPalindrome);
    }
}

package sdet_academy.day_20;

import java.util.Arrays;

public class RemoveDuplicates {
    public static void main(String[] args) {
        int[]arr={1,2,2,45,6,45,7,8};
        String temp="";
        int count=0;
        for(int i=0; i<arr.length;i++){
            count=0;
            for(int j=0;j<arr.length;j++){
                if(arr[i]==arr[j]){
                    count++;
                }
            }
            if(count==1){
                temp+=arr[i]+",";
            }

        }
        System.out.println(temp);
        String[] split = temp.split(",");
        System.out.println(Arrays.toString(split));

        int[] resultArr = new int[split.length];

        for (int i = 0; i<split.length; i++){
            resultArr[i] = Integer.parseInt(split[i]);
        }
        System.out.println(Arrays.toString(resultArr));
        System.out.println(resultArr[0] + resultArr[1]);
        System.out.println(split[0] + split[1]);
    }
}

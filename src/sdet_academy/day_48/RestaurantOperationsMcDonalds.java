package sdet_academy.day_48;

public interface RestaurantOperationsMcDonalds {
    int NUMBER_OF_EMPLOYEES = 150;
    String BRANCH_MANAGER_NAME = "Slava Matei";
    String HEAD_CHEF = "Radu Nenescu";
    int LOCATION_CAPACITY = 35;
    char HEALTH_INSPECTION_GRADE = 'A';

    void serve();
    void clean();

    default char getHealthInspectionGrade(){
        return HEALTH_INSPECTION_GRADE;
    }
}

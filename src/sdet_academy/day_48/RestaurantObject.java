package sdet_academy.day_48;

public class RestaurantObject {
    public static void main(String[] args) {
        McDonalds mcDonalds = new McDonalds();
        ChickFillA chickFillA = new ChickFillA();

        mcDonalds.open();
        mcDonalds.close();
        mcDonalds.restaurantName = "McDonald on the Fifth Ave";
        System.out.println(mcDonalds.getRestaurantName());
        Restaurant.location = "NYC";
//        chickFillA.locatedAt(); - accessing static methodsvariables through the name of the object
        // is highly discouraged
//        mcDonalds.locatedAt();

        chickFillA.restaurantName = "Chick Fill A in Philadelphia";
        System.out.println(chickFillA.getRestaurantName());
        System.out.println(mcDonalds.getHealthInspectionGrade());
        System.out.println(RestaurantOperationsMcDonalds.HEALTH_INSPECTION_GRADE);

//        chickFillA.serve();
//        chickFillA.clean();
        System.out.println(RestaurantOperationsChickFillA.NUMBER_OF_EMPLOYEES);
        chickFillA.serve();
        chickFillA.clean();
    }
}

package sdet_academy.day_48;

public class ChickFillA extends Restaurant implements RestaurantOperationsChickFillA{

    @Override
    public void open() {
        System.out.println("Chick Fill A is open 6 days a week");
    }

    @Override
    public void close() {
        System.out.println("Chick Fill A is closed on Sunday so McDonalds can make at least some money");
    }

    @Override
    public void serve() {
        System.out.println("Serving our Michelin star chicken sandwiches");
    }

    @Override
    public void clean() {
        System.out.println("Cleaning our Chick Fill A Location");
    }
}

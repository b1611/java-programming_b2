package sdet_academy.day_48;

public abstract class Restaurant {
    static String location;
    String restaurantName;


    public abstract void open();
    public abstract void close();

    public static void locatedAt(){
        System.out.println("The restaurant is located at " + location);
    }

    public String getRestaurantName(){
        return restaurantName;
    }
}

package sdet_academy.day_48;

public interface RestaurantOperationsChickFillA {
    int NUMBER_OF_EMPLOYEES = 250;
    String BRANCH_MANAGER_NAME = "Angie An";
    String HEAD_CHEF = "Iryna B";
    int LOCATION_CAPACITY = 50;
    char HEALTH_INSPECTION_GRADE = 'B';

    void serve();
    void clean();
}

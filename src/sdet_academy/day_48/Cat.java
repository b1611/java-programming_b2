package sdet_academy.day_48;

public class Cat extends Animal{

    /**
     * has Is-A relationship with the Animal
     */

    CatsToy catsToy = new CatsToy();

    public void meow(){
        System.out.println("Cat says meow");
    }

    @Override
    public void sleep() {
        System.out.println("The cat is sleeping");
    }

   public String getToy(){
       return catsToy.catsToy;
   }

   public String getToyColor(){
        return catsToy.toysColor;
   }

   public void getPlayToy(){
        catsToy.playToy();
   }

   public void getBreakToy(){
       catsToy.breakToy();
   }
}

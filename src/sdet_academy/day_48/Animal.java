package sdet_academy.day_48;

public class Animal {
    String animalName = "Animal";

    public void hunt(){
        System.out.println("Animal is hunting");
    }

    public void sleep(){
        System.out.println("Animal is sleeping");
    }

    public void animalExists(){
        System.out.println("Animal Exists");
    }

}

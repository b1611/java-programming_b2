package sdet_academy.day_48;

public class AnimalObject {
    public static void main(String[] args) {

//        Cat cat = new Cat();
//        System.out.println(cat.getToy());
//        System.out.println(cat.getToyColor());
//        cat.getPlayToy();
//        cat.getBreakToy();

//        Animal animal = new Animal();
//        Whale whale = new Whale();
//        animal.animalExists();
//        whale.animalExists();

        Whale whale = new Whale();
        Animal animal = whale; // upcasting, happens automatically
        animal.animalExists();
        ((Whale) animal).eatFish();


        Animal animal1 = new Whale();
        Whale whale1 = (Whale) animal1; //downcasting, does not happen automatically
        Whale whale2 = new Whale();

        whale1.animalExists();
        whale1.eatFish();
        whale1.releaseWater();

        whale2.animalExists();
        whale2.eatFish();
        whale2.releaseWater();



    }
}

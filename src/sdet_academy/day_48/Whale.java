package sdet_academy.day_48;

public class Whale extends Animal{
    String whaleName = "Mobidik";

    @Override
    public void animalExists() {
        System.out.println("The whale exists");
    }

    public void eatFish(){
        System.out.println("The whale is eating fish");
    }

    public void releaseWater(){
        System.out.println("The whale releasing water");
    }
}

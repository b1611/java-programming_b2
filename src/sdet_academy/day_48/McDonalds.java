package sdet_academy.day_48;

public class McDonalds extends Restaurant implements RestaurantOperationsMcDonalds{

    @Override
    public void open() {
        System.out.println("McDonalds is open");
    }

    @Override
    public void close() {
        System.out.println("McDonalds is closed");
    }

    @Override
    public void serve() {
        System.out.println("Serving a cheap burgers");
    }

    @Override
    public void clean() {
        System.out.println("Cleaning the place");
    }
}

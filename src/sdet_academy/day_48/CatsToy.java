package sdet_academy.day_48;

public class CatsToy {

    String catsToy = "Ball";
    String toysColor = "Green";

    public void playToy(){
        System.out.println("Playing with the toy");
    }

    public void breakToy(){
        System.out.println("The toy is broken");
    }
}

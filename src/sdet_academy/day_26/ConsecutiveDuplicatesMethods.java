package sdet_academy.day_26;

public class ConsecutiveDuplicatesMethods {
    public static void main(String[] args) {
        /**
         * Given an array nums, print true if there are 2 consecutive elements  with same value.
         * If no consecutive duplicates are detected in your code then print false.
         *
         * nums → [1, 5, 5, 1, 1] → true
         * nums → [1, 8, 5, 5, 0] → true
         * nums → [1, 5, 4, 1, 5] → false
         * nums → [1, 4, 4, 1, 99] → true
         */

        int[] numArr = {1, 4, 4, 1, 99};


        System.out.println(consecutiveDuplicates(numArr));
    }

    public static boolean consecutiveDuplicates(int[] numArr) {

        for (int i = 0; i < numArr.length-1; i++) {
            if (numArr[i] == numArr[i + 1]){
                return true;
            }
        }
        return false;
    }


}

package sdet_academy.day_26;

public class VarArgsIntro {
    public static void main(String[] args) {
        double a = 100.5;
        System.out.println(sumNumbers(a, 1, 2, 3, 1, 6, 1, 651, 951, 91, 9, 81, 9198, 9, 19, 19, 19, 8, 91, 91, 98, 8, 19, 5, 1, 0));

        System.out.println(sumNumbers("Hello ", " World", "This is", "Me"));

    }


    public static int sumNumbers(int... nums) { //varargs - various number of arguments
        int result = 0;
        for (int eachNum : nums) {
            result += eachNum;
        }
        return result;
    }


    public static double sumNumbers(double... nums) { //varargs - various number of arguments
        int result = 0;
        for (double eachNum : nums) {
            result += eachNum;
        }
        return result;
    }

    public static String sumNumbers(String... str) { //varargs - various number of arguments
        String result = "";
        for (String eachStr : str) {
            result += eachStr;
        }
        return result;
    }
}

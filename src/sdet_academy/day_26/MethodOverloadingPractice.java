package sdet_academy.day_26;

import java.util.Arrays;

public class MethodOverloadingPractice {
    public static void main(String[] args) {

        /**
         * Create an overloaded method, that can accept multiple int and return an array of them
         * Example:
         * 1,3,5,6 > [1,3,5,6]
         *
         * The method can also accept a multiple Strings and return an array of them
         *
         * Java, Python, C# > [Java, Python, C#]
         */

        System.out.println(Arrays.toString(addToArray(1,2,3,4,5,6,7,8,9)));
        System.out.println(Arrays.toString(addToArray("Java", "Python", "C#")));

    }


    public static int[] addToArray(int ... nums){
        int[] resultArr = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            resultArr[i] = nums[i];
        }

        return resultArr;
    }


    public static String[] addToArray(String ... strs){
        String[] resultArr = new String[strs.length];

        for (int i = 0; i < strs.length; i++) {
            resultArr[i] = strs[i];
        }
        return resultArr;
    }



}

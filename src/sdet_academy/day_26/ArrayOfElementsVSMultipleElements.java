package sdet_academy.day_26;

public class ArrayOfElementsVSMultipleElements {
    public static void main(String[] args) {
        /**
         * String[......]  - "...,..,.,.,.,"
         * String1, ...Stringn = "..,..,..,..,"
          */


    }

    public static String example(String[] arr){
        String result = "";

        for (String each : arr) {
            result += each;
        }
        return result;
    }

//    public static String example(String ... arr){ //INVESTIATE WHY NOT OVERLOADING
//        String result = "";
//
//
//        for (String each : arr) {
//
//        }
//        return result;
//    }
}

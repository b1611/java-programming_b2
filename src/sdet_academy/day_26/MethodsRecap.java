package sdet_academy.day_26;

public class MethodsRecap {
    public static void main(String[] args) {
        int a = 120;
        int b = 50;

        System.out.println(calculateRemainder(a, b));
    }

    public static int calculateRemainder(int numOne, int numTwo){
        return numOne%numTwo;
    }
}

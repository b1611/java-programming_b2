package sdet_academy.day_26;

import java.util.Arrays;

public class GetArrayOfBoolean {
    public static void main(String[] args) {

        int [] numArr = {1,2,3,4,5,6,7,8,9,10};
        //              [false, true......]
        System.out.println(Arrays.toString(getBooleanArray(numArr)));


    }

    public static boolean[] getBooleanArray(int[] arr){

        boolean[] resultArr = new boolean[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(arr[i]%2==0){
                resultArr[i] = true;
            }else {
                resultArr[i] = false;
            }
        }
        return resultArr;
    }
}

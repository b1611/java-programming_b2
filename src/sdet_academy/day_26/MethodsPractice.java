package sdet_academy.day_26;

import java.util.Arrays;

public class MethodsPractice {
    public static void main(String[] args) {
        String[] strArr = {"Florida", "California", "New York", "North Dakota", "Pennsylvania", "New Jersey"};
        System.out.println(Arrays.toString(getTheLongestElements(strArr)));

    }

    public static String[] getTheLongestElements(String[] strArr){
        String longestString = "";
        String result = "";

        for (String each : strArr) {
//            System.out.println(each +" " +  each.length());
            if(longestString.length()<each.length()){
                longestString = each;
            }
        }
//        System.out.println(longestString); // length is 12

        for (String each : strArr) {
            if(each.length()==longestString.length()){
                result+=each + ",";
            }
        }

        return result.split(","); // will return String[]
    }
}

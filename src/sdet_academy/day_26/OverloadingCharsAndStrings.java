package sdet_academy.day_26;

public class OverloadingCharsAndStrings {
    public static void main(String[] args) {

        String str = "Today is a hot day in Florida";
        System.out.println(charsOrString(str, 'a'));
        System.out.println(charsOrString(str, "cold"));
        System.out.println(charsOrString('a', str));

        /**
         * create a custom method that can accept String or char:
         * - if we pass String as an argument:
         * Example:
         * "hot"
         * result:
         * Today is a hothot day in Florida
         * - if we pass a char:
         * Example:
         * 'a'
         * result:
         * Tod#y is # hot d#y in Florid#
         */

    }

    public static String charsOrString(String strOne, char characterOne) {
        return strOne.replace(characterOne, '#');
    }

    public static String charsOrString(char characterOne, String strOne) {
        return strOne.replace(characterOne, '#');
    }

    public static String charsOrString(String strOne, String strTwo) {

        String newStrTwo = strTwo;

        if (strOne.contains(strTwo)) {
            newStrTwo = strTwo + strTwo;
        }
        return strOne.replace(strTwo, newStrTwo);
    }

}

package sdet_academy.day_26;

public class MethodOverloadingIntro {
    public static void main(String[] args) {
        int a = 10;
        int b = 15;
        int c = 100;

        double x = 100.5;
        double y = 101.7;

//        System.out.println(sumNumbers(100,500));
//        System.out.println(sumNumbers(a, b, c));

    }

    /**
     * Overloading on the number of arguments
     *   public static int sumNumbers(int numOne, int numTwo) {
     *         return numOne + numTwo;
     *     }
     *
     *     public static int sumNumbers(int numOne, int numTwo, int numThree) {
     *         return numOne + numTwo + numThree;
     *     }
     */

    /**
     * Overloading on data type of arguments
     * public static int sumNumbers(int numOne, int numTwo) {
     *             return numOne + numTwo;
     *          }
     *
     *  public static double sumNumbers(double numOne, double numTwo) {
     *         return numOne + numTwo;
     *     }
     */


    /**
     * Overloading by the order of the arguments
     public static double sumNumbers(double numOne, long numTwo) {
     return numOne + numTwo;
     }

     public static double sumNumbers( long numTwo, double numOne) {
     return numOne + numTwo;
     }
     */

}

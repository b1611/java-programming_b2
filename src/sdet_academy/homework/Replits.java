package sdet_academy.homework;

public class Replits {
    public static void main(String[] args) {

/**
 1. When word has **odd number of characters and:**
 **3 or more characters, print middle letter**
 2. **Single character, print that character 3 times**
 3. When word has **even number of characters and**:
 **4 or more characters**, print the middle 2 characters
 **2 characters,** print those 2 characters twice
 4. make sure the str is not empty
 */


        String str = "a ";
//        str = str.trim();
        String result = "";
//
//        if (str.isEmpty() || str.isBlank()) {
//            System.out.println("str can not be empty or blank");
//        } else {
//            if (str.length() == 1) {
//                result = str.repeat(3);
//            } else if (str.length() == 2) {
//                result = str.repeat(2);
//            } else if (str.length() % 2 == 1) {
//                result = "" + str.charAt(str.length() / 2);
//            } else {
//                result = "" + str.charAt(str.length() / 2 - 1) + str.charAt(str.length() / 2);
//            }
//        }
//        System.out.println(result);

//        if (str.length() == 1 && !str.isEmpty() && !str.isBlank()){
//            result = str.repeat(3);
//        } else if (str.length() == 2 && !str.isEmpty() && !str.isBlank()) {
//            result = str.repeat(2);
//        } else if (str.length() % 2 == 1 && !str.isEmpty() && !str.isBlank()) {
//            result = "" + str.charAt(str.length() / 2);
//        } else if (str.length() % 2 == 0 && !str.isEmpty() && !str.isBlank()) {
//            result = "" + str.charAt(str.length() / 2 - 1) + str.charAt(str.length() / 2);
//        }else {
//            System.out.println("str can not be empty or blank");
//        }
//        System.out.println(result);

        if (str.isEmpty() || str.isBlank() || str.contains(" ")) {
           result =  "str is invalid";
        } else if (str.length() == 1) {
            result = str.repeat(3);
        } else if (str.length() == 2) {
            result = str.repeat(2);
        } else if (str.length() % 2 == 1) {
            result = "" + str.charAt(str.length() / 2);
        } else {
            result = "" + str.charAt(str.length() / 2 - 1) + str.charAt(str.length() / 2);
        }
        System.out.println(result);

    }
}

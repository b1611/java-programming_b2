package sdet_academy.homework;

import java.util.Scanner;

public class Liuba {
    public static void main(String[] args) {

        Scanner circle = new Scanner(System.in);
        System.out.println("To find area and perimeter of circle please enter diameter of this circle");
        int diameterOfCircle = circle.nextInt();
        System.out.println("You enter diameter of circle " + diameterOfCircle);
        System.out.println("The area of circle is " + diameterOfCircle * diameterOfCircle * 3.14);
        System.out.println("The perimeter of circle is " + 2 * (diameterOfCircle * 3.14));

        Scanner triangle = new Scanner(System.in);

        System.out.println("To find perimeter of triangle enter side A ");
        int sideA = triangle.nextInt();
        System.out.println("side A is " + sideA);
        System.out.println("Enter side B ");
        int sideB = triangle.nextInt();
        System.out.println("Side B is " + sideB);
        System.out.println(" Please enter side C ");
        int sideC = triangle.nextInt();
        System.out.println("Side C is " + sideC);
        System.out.println("The perimeter of triangle is " + (sideA + sideB + sideC));

        Scanner rectangle = new Scanner(System.in);

        System.out.println("To find perimeter and area of rectangle please enter side A and  B ");
        System.out.println("Please enter side A");
        int rectangleSideA = rectangle.nextInt();
        System.out.println("The side A is " + rectangleSideA);
        System.out.println("Please enter side B");
        int rectangleSideB = rectangle.nextInt();
        System.out.println("The side B is " + rectangleSideB);
        System.out.println("The perimeter of rectangle is " + 2 * (rectangleSideA + rectangleSideB));
        System.out.println("The area of rectangle is " + rectangleSideA * rectangleSideB);


    }
}

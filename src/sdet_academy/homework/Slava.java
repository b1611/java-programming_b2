package sdet_academy.homework;

public class Slava {
    public static void main(String[] args) {
        double volume = 99;

        if (volume ==0){
            System.out.println("Non-Alcoholic Drink");
        } else if(volume <=5 && volume >=0.1){
            System.out.println("Beer");
        } else if (volume<=11 && volume >5){
            System.out.println("Beer or Champagne");
        } else if(volume<=15 && volume>11){
            System.out.println("Beer,Champagne or Wine");
        } else if(volume<=16 && volume>15){
            System.out.println("Beer,Champagne,Wine or Sake");
        } else if (volume<=40 && volume>16) {
            System.out.println("Beer,Champagne,Wine,Sake or Whiskey");
        } else if (volume>40 && volume<=100) {
            System.out.println("Sambuca or Gasoline");
        } else {
            System.out.println("Invalid Data");

        }
    }
}

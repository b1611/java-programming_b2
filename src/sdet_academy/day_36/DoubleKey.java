package sdet_academy.day_36;

import java.util.LinkedHashMap;
import java.util.Map;

public class DoubleKey {
    public static void main(String[] args) {
        //{Angie=teacher, Luba=actress, Radu=singer, Iryna=scientist, Myroslava=model, Slava=astronaut}
        //{MyroslavaMyroslava=model, AngieAngie=teacher, IrynaIryna=scientist, LubaLuba=actress, RaduRadu=singer, SlavaSlava=astronaut}

        Map<String, String> map = new LinkedHashMap();
        map.put("Angie", "teacher");
        map.put("Luba", "actress");
        map.put("Radu", "singer");
        map.put("Iryna", "scientist");
        map.put("Myroslava", "model");
        map.put("Slava", "astronaut");

        System.out.println(doubleKey(map));
        map.forEach((key, value) -> {
            System.out.println(key);
            System.out.println(value);
            if(key.startsWith("A")){
                System.out.println("It's A");
            }else{
                System.out.println("It's Not A");
            }
        });

    }
    public static Map<String,String> doubleKey (Map<String,String> map){
        Map<String,String> result= new LinkedHashMap<>();

        // Version1
       /* for (String each : map.keySet()) {
          result.put(each.repeat(2),map.get(each));
        }*/
// Version 2
        for (Map.Entry<String, String> StringEntry : map.entrySet()) {
            result.put(StringEntry.getKey().repeat(2), StringEntry.getValue());

        }
        return result;
    }
}

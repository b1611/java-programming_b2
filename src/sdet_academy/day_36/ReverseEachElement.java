package sdet_academy.day_36;

import sdet_academy.day_25.ReverseString;
import sdet_academy.day_35.ReverseElement;

import java.util.LinkedHashMap;
import java.util.Map;

public class ReverseEachElement {
    public static void main(String[] args) {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(1, "Teacher");
        map.put(2, "Austronaut");
        map.put(3, "Singer");
        map.put(4, "Actress");
        map.put(5, "Scientist");
        map.put(6, "Model");

//        map.forEach((key,value)->{
//            map.replace(key, ReverseString.reverseCustomMethod(value));
//
//        });
        System.out.println(map);

        System.out.println(reverseEachValue(map));
    }
    public static Map<Integer, String> reverseEachValue(Map<Integer, String> map){
        for (Map.Entry<Integer, String> eachEntry : map.entrySet()) {
            String tempKey = "" + eachEntry.getKey();
            String tempValue = eachEntry.getValue();
            System.out.println(tempKey + " = " + tempValue);
            map.replace(eachEntry.getKey(), tempKey + " = " + tempValue);
        }
        return map;
    }
}

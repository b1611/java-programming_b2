package sdet_academy.day_36;

import java.util.LinkedHashMap;
import java.util.Map;

public class NewClassSomething {
    public static void main(String[] args) {
        // {Angie = te#cher, Luba = #ctress}

        Map<String, String> map = new LinkedHashMap();
        map.put("Angie", "teacher");
        map.put("Luba", "actress");
        map.put("Radu", "singer");
        map.put("Iryna", "scientist");
        map.put("Myroslava", "model");
        map.put("Slava", "astronaut");
        System.out.println(map);

        for (Map.Entry<String, String> eachEntry : map.entrySet()) {
                                //  Angie=teacher
            System.out.println(eachEntry);
            System.out.println(eachEntry.getKey());
            System.out.println(eachEntry.getValue());
            map.put(eachEntry.getKey(), eachEntry.getValue().replace("a", "#"));
        }

        System.out.println(map);

    }
}

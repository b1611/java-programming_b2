package sdet_academy.day_36;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class ConvertMap {
    public static void main(String[] args) {
        // [Angie=teacher, Luba=actress, Radu=singer, Iryna=scientist, Myroslava=model, Slava=astronaut]

        Map<String, String> map = new LinkedHashMap();
        map.put("Angie", "teacher");
        map.put("Luba", "actress");
        map.put("Radu", "singer");
        map.put("Iryna", "scientist");
        map.put("Myroslava", "model");
        map.put("Slava", "astronaut");


        ArrayList<String> result= new ArrayList<>();

        map.forEach((key,value)->{
            result.add(key+"="+value);

        });

        System.out.println(result);

        System.out.println(convertMap(map));

        System.out.println(Arrays.toString(convert(map)));

    }
    public static ArrayList<String> convertMap(Map<String,String> map){
        ArrayList<String> result= new ArrayList<>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            result.add(entry.getKey()+"="+ entry.getValue());

        }
        return result;
    }

    public static String [] convert (Map<String,String> map){
        String [] result = new String[map.size()];
        int index= 0;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            result[index]=  entry.getKey() + "=" + entry.getValue();
            index++;

        }
        return result;

    }
}

package sdet_academy.day_36;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapStream {
    public static void main(String[] args) {

        Map<String, String> map = new LinkedHashMap();
        Map<String, String> result = new LinkedHashMap();
        map.put("Angie", "teacher");
        map.put("Luba", "actress");
        map.put("Radu", "singer");
        map.put("Iryna", "scientist");
        map.put("Myroslava", "model");
        map.put("Slava", "astronaut");

        System.out.println(map);

        map.forEach((key, value) -> {
//            if(key.startsWith("A") || key.endsWith("a")){
//                System.out.println("true");
//            }else{
//                System.out.println("false");
//            }
            result.put(key.repeat(2), value);
//            map.put(key + key, value); - will give us out of memory error
        });
        System.out.println(result);
    }
}

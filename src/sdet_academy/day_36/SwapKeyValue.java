package sdet_academy.day_36;

import java.util.LinkedHashMap;
import java.util.Map;

public class SwapKeyValue {
    public static void main(String[] args) {
        //  {1=teacher, 2=actress, 3=singer, 4=scientist, 5=model, 6=astronaut}
        // teacher = 1; actress=2....etc
        Map<Integer, String> map = new LinkedHashMap();

        map.put(1, "teacher");
        map.put(2, "actress");
        map.put(3, "singer");
        map.put(4, "scientist");
        map.put(5, "model");
        map.put(6, "astronaut");
        System.out.println(reverseKeyWithValue(map));

    }
    public static Map<String, Integer> reverseKeyWithValue (Map<Integer, String> map) {
        Map<String, Integer> result = new LinkedHashMap();

        map.forEach((key,value)->{
            result.put(value,key);
        });
        return result;
    }
}

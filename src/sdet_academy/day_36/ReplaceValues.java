package sdet_academy.day_36;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReplaceValues {
    public static void main(String[] args) {

        // {Angie = te#cher, Luba = #ctress}

        Map<String, String> map = new LinkedHashMap();
        map.put("Angie", "teacher");
        map.put("Luba", "actress");
        map.put("Radu", "singer");
        map.put("Iryna", "scientist");
        map.put("Myroslava", "model");
        map.put("Slava", "astronaut");

        System.out.println(map);
        System.out.println(replaceValueMethod2(map));

    }

    public static Map<String, String> replaceValueMethod(Map<String, String> map) {
        for (Map.Entry<String, String> eachEntry : map.entrySet()) {
            map.replace(eachEntry.getKey(), eachEntry.getValue().replace("a", "#"));
        }
        return map;
    }

    public static Map<String, String> replaceValueMethod2(Map<String, String> map) {
        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, String> eachEntry : map.entrySet()) {
            result.put(eachEntry.getKey().repeat(2), eachEntry.getValue());
        }
        return result;
    }

}


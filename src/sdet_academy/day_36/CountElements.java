package sdet_academy.day_36;

import java.util.*;

public class CountElements {
    public static void main(String[] args) {
        String[]arr={"a", "b", "a", "c", "b"};
        System.out.println(countKeys(arr));

    }
    public static Map<String, Integer> countKeys(String[]arr){
        Map<String,Integer>map=new HashMap<>();
        ArrayList<String> list=new ArrayList<>(Arrays.asList(arr));

        for (String s : arr) {
            map.put(s, Collections.frequency(list,s));
        }
        return map;
    }
}

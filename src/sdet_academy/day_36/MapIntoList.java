package sdet_academy.day_36;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MapIntoList {
    public static void main(String[] args) {
        ArrayList<String>list=new ArrayList<>(Arrays.asList("blue","green","black","white","purple"));
        //{                                                  blue=green,black=white     purple=null
        //if the list size is odd, add null for the value
        System.out.println(convertArrayListToMapMethod(list));

    }
    public static Map<String,String> convertArrayListToMapMethod(ArrayList<String> list){
        Map<String,String>map=new HashMap<>();
        for (int i = 0; i < list.size()-1; i++) {
            map.put(list.get(i),list.get(i+1));
            i+=1;
        }
        return map;
    }
}

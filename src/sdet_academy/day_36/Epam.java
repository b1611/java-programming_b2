package sdet_academy.day_36;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Epam {
    public static void main(String[] args) {
        /*
    Write a method which will take List<String> as parameter and return map Map<String,Integer>
    ,which contains only entities started from letter "A".As a map key use
    Input string: List<String> names = Arrays.asList("Alex","Bob","Alice","Jack");
    Output: Map<String,Integer> map = {Alex=4, Alice=5};
    Suggestion for candidates: use Java streams feature
     */

        List<String> names = Arrays.asList("Alex","Bob","alice","Jack");
        System.out.println(returnMap(names));
        Map<String, Integer> map = returnMap(names);
        map.forEach((key, value) -> {
            System.out.println(key);
            System.out.println(value);
            if(key.startsWith("A")){
                System.out.println("It's A");
            }else{
                System.out.println("It's Not A");
            }
        });
    }
    public static Map<String, Integer> returnMap(List<String> list) {
        Map<String, Integer> mapSomething = new LinkedHashMap<>();
        list.stream().filter(s->s.startsWith("A") || s.startsWith("a")).forEach(s-> mapSomething.put(s,s.length()));
        return mapSomething;
    }
}

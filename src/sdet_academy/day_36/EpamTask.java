package sdet_academy.day_36;

import java.util.*;

public class EpamTask {
    public static void main(String[] args) {
     /*
    Write a method which will take List<String> as parameter and return map Map<String,Integer>
    ,which contains only entities started from letter "A".As a map key use
    Input string: List<String> names = Arrays.asList("Alex","Bob","Alice","Jack");
    Output: Map<String,Integer> map = {Alex=4, Alice=5};
    Suggestion for candidates: use Java streams feature
     */
        List<String> list = new ArrayList<>(Arrays.asList("Alex", "Bob", "Alice", "Jack"));
        System.out.println(countElementLength(list));
        System.out.println(countElementLengthStreams(list));
    }

    public static Map<String, Integer> countElementLength(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        for (String s : list) {
            if (s.startsWith("A")) {
                map.put(s, s.length());
            }
        }
        return map;
    }

    public static Map<String, Integer> countElementLengthStreams(List<String> list) {
        Map<String, Integer> map = new HashMap<>();
        list.stream().filter(s -> s.startsWith("A")).forEach(s -> map.put(s, s.length()));
        return map;
    }
}

package sdet_academy.day_14;

public class RecapWhileAndDoWhile {
    public static void main(String[] args) {

        int oranges = 100;
        int pineapples = 30;

        while (oranges > 10){
            System.out.println("In the loop " + oranges);
            oranges-=10;
        }

        do{
            System.out.println("pineapples = " + pineapples);
            pineapples++;
        }while (pineapples<20);

    }
}

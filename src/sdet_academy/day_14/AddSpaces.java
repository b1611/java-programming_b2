package sdet_academy.day_14;

public class AddSpaces {
    /**
     * Create an str= "Java"
     * add space between every single letter
     * we should not have any spaces in the beginning and at the end
     * output should be "J a v a"
     */
    public static void main(String[] args) {

        String str = "Java";
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            result += str.charAt(i) + " ";
        }
        result = result.trim();
        System.out.println(result);
    }
}

package sdet_academy.day_14;

import java.util.Scanner;

public class ATM {
    public static void main(String[] args) {
        /**
         * Given expectedPin = 1234, please create a program that will take an actualPin from user using scanner.
         * Use do while loop until the pin numbers match or the number of attempts is more than 3
         */
        Scanner scanner = new Scanner(System.in);
        int expectedPin = 1234;
        int actualPin;
        int attempt = 0;
        do {
            System.out.println("Please enter your pin");
            actualPin = scanner.nextInt();
            if (expectedPin == actualPin) {
                System.out.println("They are match");
            } else if (attempt == 2) {
                System.out.println("It was your third attempt");
                attempt++;
            } else {
                attempt++;
                System.out.println("Try again");
            }
        } while (expectedPin != actualPin && attempt < 3);
    }
}

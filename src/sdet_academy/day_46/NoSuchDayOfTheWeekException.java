package sdet_academy.day_46;

public class NoSuchDayOfTheWeekException extends RuntimeException{
    public NoSuchDayOfTheWeekException(String errorMessage){
        super(errorMessage);
    }
}

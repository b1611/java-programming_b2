package sdet_academy.day_46;

public class DaysOfTheWeek {
    public static void main(String[] args) {
        String day = "Mondayy";

        switch (day) {
            case "Sunday":
                System.out.println("THis is Sunday");
                break;
            case "Monday":
                System.out.println("THis is Monday");
                break;
            case "Tuesday":
                System.out.println("THis is Tuesday");
                break;
            case "Wednesday":
                System.out.println("THis is Wednesday");
                break;
            case "Thursday":
                System.out.println("THis is Thursday");
                break;
            case "Friday":
                System.out.println("THis is Friday");
                break;
            case "Saturday":
                System.out.println("THis is Saturday");
                break;
            default:
//                System.out.println("No such day");
                throw new NoSuchDayOfTheWeekException("There is no such day as " + day);
        }

        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");
        System.out.println("END");

    }
}

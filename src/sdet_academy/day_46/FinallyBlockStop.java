package sdet_academy.day_46;

public class FinallyBlockStop {
    public static void main(String[] args) {


        String str = null;
        try {
            System.out.println(str.length());
            System.out.println("Will this line run?");
        }catch (NullPointerException e){
            System.out.println("Catching NullPointerException");
            System.exit(1);
        }finally {
            System.out.println("Finally block is executed");
        }

        System.out.println("END");
    }
}

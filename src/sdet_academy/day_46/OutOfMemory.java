package sdet_academy.day_46;

public class OutOfMemory {
    public static void main(String[] args) {

        try {
            myMethod();
        }catch (Throwable e){
            System.out.println("Catching recursion");
        }
        System.out.println("END");
    }

    public static int count = 0;

    public static void myMethod() {
        System.out.println(count++);
        myMethod();
    }
}

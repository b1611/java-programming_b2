package sdet_academy.day_16;

public class CountTriplets {
    public static void main(String[] args) {
        /***
         * We'll say that a "triple" in a string is a char appearing three times in a row. Print out the number of triples in the given string. The triples may overlap.
         *
         *
         * Example:
         * input: abcXXXabc
         * output: 1
         *
         * Example:
         * input: xxxabyyyycd
         * output: 3
         *
         * Example:
         * input: java
         *
         * output: 0
         */


        String name = "java";
        int output=0;
        for(int i =0; i<name.length()-2;i++){
            // System.out.println(name.charAt(i+2));
            if(name.charAt(i)==name.charAt(i+1) && name.charAt(i)==name.charAt(i+2)){
                output++;
            }
        } System.out.println(output);
    }
}

package sdet_academy.day_16;

public class HomeWorkPractice {
    public static void main(String[] args) {

        /**
         Create a variable that will only have 1 characters(Example: 'c').
         Create another variable called converter.
         The idea is, that every character in your text,
         has a corresponding digit on your cell phone(Example: abc - 2, def - 3, ... wxyz - 9).
         Create a method that will convert the characters into the numbers.
         Examples:
         c = 2;
         p =  7;
         f = 3;
         etc.
         */

        char tempCharacter = '*';
        String phoneNumber = "+1.800.555.SDET"; // +1 800 555 7338
        //                      1 800.555.SDET = 1 800 555 7338
        //                      800.555.SDET = 800 555 7338
        phoneNumber = phoneNumber.toLowerCase();
        int digit;
        String result = "";

        for (int i = 0; i < phoneNumber.length(); i++) {
            tempCharacter = phoneNumber.charAt(i);
            if(tempCharacter=='+'){
                result+=tempCharacter;
            }else if (Character.isDigit(tempCharacter)) {
                result += tempCharacter;
            } else if (Character.isLetter(tempCharacter)) {
                if (tempCharacter == 'a' || tempCharacter == 'b' || tempCharacter == 'c') {
                    digit = 2;
                    result += digit;
                } else if (tempCharacter == 'd' || tempCharacter == 'e' || tempCharacter == 'f') {
                    digit = 3;
                    result += digit;
                } else if (tempCharacter == 'g' || tempCharacter == 'h' || tempCharacter == 'i') {
                    digit = 4;
                    result += digit;
                } else if (tempCharacter == 'j' || tempCharacter == 'k' || tempCharacter == 'l') {
                    digit = 5;
                    result += digit;
                } else if (tempCharacter == 'm' || tempCharacter == 'n' || tempCharacter == 'o') {
                    digit = 6;
                    result += digit;
                } else if (tempCharacter == 'p' || tempCharacter == 'q' || tempCharacter == 'r' || tempCharacter == 's') {
                    digit = 7;
                    result += digit;
                } else if (tempCharacter == 't' || tempCharacter == 'u' || tempCharacter == 'v') {
                    digit = 8;
                    result += digit;
                } else if (tempCharacter == 'w' || tempCharacter == 'x' || tempCharacter == 'y' || tempCharacter == 'z') {
                    digit = 9;
                    result += digit;
                }
            } else if (!Character.isLetterOrDigit(tempCharacter)) {
                result += " ";
            }
        }
        System.out.println(result);
    }
}

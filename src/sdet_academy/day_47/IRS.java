package sdet_academy.day_47;

public interface IRS {
    int NUMBER_OF_AGENTS = 100_000;

    void doAudit();
    void giveFines();
}

package sdet_academy.day_47;

public abstract class Bank {
    public abstract int InterestRate();

    public static void recession(){
        System.out.println("We are in recession");
    }

    public void makeMoney(){
        System.out.println("The bank is making money");
    }
}

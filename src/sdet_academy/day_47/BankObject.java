package sdet_academy.day_47;

import static sdet_academy.day_47.Bank.recession;

public class BankObject {
    public static void main(String[] args) {
        Chase chase = new Chase();
        BOA boa = new BOA();
        Truist truist = new Truist();
//        Bank bank = new Bank(); // abstract class has constructor but we cannot create an object

        System.out.println(chase.InterestRate());
        System.out.println(boa.InterestRate());
        System.out.println(truist.InterestRate());

        recession();
        chase.makeMoney();
        System.out.println(ChaseEmployee.BRANCH_ID);
        chase.closeChaseBank();
        chase.doAudit();
    }
}

package sdet_academy.day_47;

public abstract class Movie {
    double totalRevenue;

    public abstract void watch();
    public abstract void stopWatching();

    public static void premiere(){
        System.out.println("The grand Premiere is here!");
    }

    public double geTotalRevenue(){
        return totalRevenue;
    }
}

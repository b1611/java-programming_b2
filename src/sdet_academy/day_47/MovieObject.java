package sdet_academy.day_47;

public class MovieObject {
    public static void main(String[] args) {
        LordOfRings lordOfRings = new LordOfRings();
        lordOfRings.totalRevenue = 9000000;
        System.out.println(lordOfRings.geTotalRevenue());
        Movie.premiere();
        lordOfRings.watch();
        lordOfRings.stopWatching();

        LordOfTheRingsSequel lordOfTheRingsSequel = new LordOfTheRingsSequel();
        lordOfTheRingsSequel.totalRevenue = 5000000;
        System.out.println(lordOfTheRingsSequel.geTotalRevenue());
        lordOfTheRingsSequel.FrodoIsAlive();
        lordOfRings.casting();
        lordOfTheRingsSequel.casting();
    }
}

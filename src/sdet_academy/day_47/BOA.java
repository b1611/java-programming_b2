package sdet_academy.day_47;

public class BOA extends Bank implements IRS{

    @Override
    public int InterestRate() {
        return 6;
    }

    @Override
    public void doAudit() {
        System.out.println("Auditing the BoA");
    }

    @Override
    public void giveFines() {
        System.out.println("BoA goes down and files for bankruptcy");
    }
}

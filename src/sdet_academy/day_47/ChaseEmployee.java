package sdet_academy.day_47;

public interface ChaseEmployee extends IRS, Government{
    int BRANCH_ID = 39;

    void openChaseBank();

    default void closeChaseBank(){
        System.out.println("Closing the chase bank doors");
    }
}

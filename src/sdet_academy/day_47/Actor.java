package sdet_academy.day_47;

public interface Actor {
    String ACTOR_NAME = "Elaija Wood";
    int AGE = 25;

    void casting();

    default void gotTheRole(){
        System.out.println("I got the role!");
    }
}

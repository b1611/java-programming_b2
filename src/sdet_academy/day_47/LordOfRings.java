package sdet_academy.day_47;

public class LordOfRings extends Movie implements Actor{

    @Override
    public void watch() {
        System.out.println("Watching Lord of Rings");
    }

    @Override
    public void stopWatching() {
        System.out.println("Stopped watching Lord of Rings season two");
    }

    @Override
    public void casting() {
        System.out.println("Casting for Lord of the Rings");
    }
}

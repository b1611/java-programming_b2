package sdet_academy.day_47;

public interface Producer {
    String STUDIO_NAME = "Warner Bros";
    String STUDIO_LOCATION = "Los Angeles";

    void action();
    default void cut(){
        System.out.println("AND CUT!!!!");
    }
}

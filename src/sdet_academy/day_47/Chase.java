package sdet_academy.day_47;

public class Chase extends Bank implements ChaseEmployee, IRS{

    @Override
    public int InterestRate() {
        return 5;
    }

    @Override
    public void openChaseBank() {
        System.out.println("Opening Chase Bank Doors");
    }

    @Override
    public void closeChaseBank() {
        System.out.println("Overriden Method close Doors");
    }

    @Override
    public void doAudit() {
        System.out.println("IRS does the audit");
    }

    @Override
    public void giveFines() {
        System.out.println("You are going to jail");
    }

    @Override
    public void rule() {

    }
}

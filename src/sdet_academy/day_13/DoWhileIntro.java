package sdet_academy.day_13;

public class DoWhileIntro {
    public static void main(String[] args) {

        int melon = 100;
        while(melon<10){
            System.out.println("melon = " + melon);
            melon++;
        }


        do{
            System.out.println("melon = " + melon);
            melon++;
        }while (melon<10);


    }
}

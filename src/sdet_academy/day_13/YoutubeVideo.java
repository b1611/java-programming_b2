package sdet_academy.day_13;

public class YoutubeVideo {
    public static void main(String[] args) throws InterruptedException {

        /**
         * Create a code, that will print an emoji of your favorite animal 20 times
         * Please slow down the execution of the program for 1 sec.
         * run the code, execute, build, compile the code
         */
        int count = 0;

        while (count <= 20) {
            System.out.println("Favorite  animal is \uD83D\uDC36 with ❤ no \uD83D\uDC0A " + count);
            count++;
            Thread.sleep(1000); // 1 sec delay
        }
    }
}

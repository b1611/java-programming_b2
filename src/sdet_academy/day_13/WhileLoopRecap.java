package sdet_academy.day_13;

public class WhileLoopRecap {
    public static void main(String[] args) {


        int oranges = 25;
        int lemons = 60;

        while (oranges > 0 || lemons < 100) {
            System.out.println("oranges = " + oranges);
            System.out.println(lemons);
            lemons += 5;
            oranges--;
        }


    }
}

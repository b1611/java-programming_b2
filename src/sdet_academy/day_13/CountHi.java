package sdet_academy.day_13;

public class CountHi {
    public static void main(String[] args) {
        /**
         * Create a program that will have str "hi Angie, hi Slava, hi Radu, hi Iryna, hi Liuba, hi James, hi Myroslava"
         * Please count how many "hi" are in the str, using while loop
         */

        String name = "Hi Angie, Hi Kihi, hi Slava, hi Radu, hi Iryna, hi Liuba, hi James, hi Myroslava";
        name = name.toLowerCase();
        int count = 0;

        while (name.contains("hi ")) {
            count++;
            name = name.substring(name.indexOf("hi ") + 2);
            System.out.println(name);
        }
        System.out.println(count);
    }
}

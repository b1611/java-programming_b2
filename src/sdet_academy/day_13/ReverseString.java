package sdet_academy.day_13;

public class ReverseString {
    public static void main(String[] args) {

        String str = "Python is not good"; // avaJ
        String result = "";
        int count = str.length()-1; //3 > 2 > 1 > 0 > break
        String character;

        while(count>=0){
            character = "" + str.charAt(count);
            result += character.toUpperCase();
            count--;
        }

        System.out.println(result);
        System.out.println(result);


    }
}

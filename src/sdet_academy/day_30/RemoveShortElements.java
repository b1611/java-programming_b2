package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveShortElements {
    public static void main(String[] args) {
        /**
         * Remove all the elements if their length is less than 5
         * Example:
         * [blue, grey, brown, purple, gold, pink, golden]
         * result:
         * [brown, purple, golden]
         *
         * final result:
         * [BROWN, PURPLE, GOLDEN]
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("blue", "grey", "brown", "purple", "gold", "pink", "golden"));


        System.out.println(removeShort(list, 6));

    }
    public static ArrayList<String> removeShort(ArrayList<String> list, int target){
        list.removeIf(x->x.length()<target);
        list.replaceAll(x->x.toUpperCase());
        return list;
    }
}

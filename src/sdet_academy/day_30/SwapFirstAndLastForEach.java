package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class SwapFirstAndLastForEach {
    public static void main(String[] args) {
        /**
         * [blue, red, purple]
         * [elub, der, eurplp]
         */
        ArrayList<String> list=  new ArrayList<>(Arrays.asList("blue", "red", "purple"));

        list.replaceAll(x->swapLetters(x));
        System.out.println(list);

    }

    public static String swapLetters(String str) {
        String firstLetter;
        String lastLetter;
        String middle;
        firstLetter = "" + str.charAt(0);
        lastLetter = "" + str.charAt(str.length() - 1);
        middle = str.substring(1, str.length()-1);
        String result = lastLetter + middle + firstLetter;
        return result;
    }
}

package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class EvenOrOdd {
    public static void main(String[] args) {

        /**
         * Given an ArrayList<String> return an ArrayList<String>
         *     that will return true or false depending on if the element is even or not
         *     Example:
         *     [Java, C#, Kotlin, C++]
         *     result:
         *     [true, true, true, false]
         */
        ArrayList<String> list=new ArrayList<>(Arrays.asList("Java", "C#", "Kotlin", "C++"));
        list.replaceAll(s-> String.valueOf(s.length()%2==0));
        System.out.println(list);
    }
}

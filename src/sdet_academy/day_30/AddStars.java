package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class AddStars {
    public static void main(String[] args) {

        //[car, cat, dog, lime] - ArraylistOf Strings
        //[*car*, *cat*, *dog*, *lime*]

        ArrayList<String> list = new ArrayList<>(Arrays.asList("car", "cat", "dog", "lime"));
        System.out.println(addStars(list));


    }

    public static ArrayList<String> addStars(ArrayList<String> list) {
        list.replaceAll(n -> "*" + n + "*");
        return list;
    }
}

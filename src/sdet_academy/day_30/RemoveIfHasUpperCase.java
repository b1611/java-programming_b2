package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveIfHasUpperCase {
    public static void main(String[] args) {

        /**
         * Given the arraylist of String, remove the element if it has at least 1 upper case
         * Example:
         * [Java, java, Python, python, jaVa]
         * [java, java, python, python, java]
         * [java, python]
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "java", "Python", "python", "jaVa"));

        list.removeIf( x -> !x.equals(x.toLowerCase()));
        System.out.println(list);

    }
}

package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class ModuleEachElement {
    public static void main(String[] args) {

        /**
         * Create a dynamic method that will accept ArrayList<Integer> and int
         * We should use our int for our module
         * [1,2,3,4,5], 2
         * [1, 0, 1, 0, 1]
         */

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5));
        int num = 2;
        System.out.println(moduleEach(list,num));

    }
    public static ArrayList<Integer> moduleEach(ArrayList<Integer> list, int num){

        list.replaceAll(n -> n%num);
        return list;
    }
}

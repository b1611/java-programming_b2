package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class LambdaIntro {
    public static void main(String[] args) {

        /**
         *  Assume we have an arraylist of integers [1,2,3,4,5,6].
         *  Create a custom method that will multiply each element by 2
         *  Result [2,4,6,8,10,12]
         */

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6));
        System.out.println(multipleByTwo(list));
        list.replaceAll(n -> n-2);
        System.out.println(list);

    }

    public static ArrayList<Integer> multipleByTwo(ArrayList<Integer> list){
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i)*2);
        }
        return list;
    }
}

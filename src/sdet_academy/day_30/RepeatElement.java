package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class RepeatElement {
    public static void main(String[] args) {

        /**
         * given you have an arrayList of Strings that looks something like: [Java, Python, C++, JavaScript, Pascal]
         * you should create a custom method, that will do the double each element. Result should be:
         * Result: [JavaJava, PythonPython, C++C++, JavaScriptJavaScript, PascalPascal].
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "Python", "C++", "JavaScript", "Pascal"));
        System.out.println(repeatMethod(list));

        list.replaceAll(s->s.repeat(2));
        System.out.println(list);

        list.replaceAll(s->"" + s.length());
        System.out.println(list);

    }

    public static ArrayList<String> repeatMethod(ArrayList<String> list){
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i) + list.get(i));
        }
        return list;
    }

    public static ArrayList<String> repeatMethodLambda(ArrayList<String> list){
        list.replaceAll(s->s.repeat(2));
        return list;
    }

}

package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveIfNotPalindrome {
    public static void main(String[] args) {
        /**
         * Given you have an ArrayList<String> = [ci vic, Radar, dog, god, cat, noon] remove a non palindrome elements
         * Result: [ci vic, Radar, noon]
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("ci vic", "Radar", "cat", "dog", "noon"));
        list.removeIf(x->!isPalindrome(x));
        System.out.println(list);


    }


    public static boolean isPalindrome(String str){ //Ci vic
        str = str.replace(" ", ""); //Civic
        StringBuilder stringBuilder = new StringBuilder(str); //Civic
        stringBuilder.reverse(); //civiC
        return str.equalsIgnoreCase(stringBuilder.toString());
    }
}

package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveOddElements {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5));
        list.removeIf(x->x%2==1);
        System.out.println(list);
    }
}

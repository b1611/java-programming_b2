package sdet_academy.day_30;

import java.util.ArrayList;
import java.util.Arrays;

public class GetEachLength {
    public static void main(String[] args) {

        /**
         * Create a program that will replace the element with the number of its characters
         * Example:
         * [Java, Kotlin, C#]
         * Result:
         * [4,6,2]
         */
        ArrayList<String> str = new ArrayList<>(Arrays.asList("Java", "Kotlin", "C#"));
        System.out.println(getLength(str));

    }

    public static ArrayList<String> getLength(ArrayList<String> list) {

        list.replaceAll(n -> n.length() + "");
        return list;
    }
}

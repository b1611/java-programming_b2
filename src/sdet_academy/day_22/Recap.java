package sdet_academy.day_22;

import java.util.Arrays;

public class Recap {
    public static void main(String[] args) {

        int[] numArr = {1,8,9,4,6};

//        Arrays.sort(numArr); - will sort an array in ascending order
        // if we need to sort in descending order, we can still use Arrays.sort() and then just reverse an array

        for (int i = 0; i < numArr.length; i++) {
            for (int j = 0; j < numArr.length; j++) {
                if(numArr[i]>numArr[j]){
                    // a         b
                    int temp;
                    temp = numArr[i];
                    numArr[i] = numArr[j];
                    numArr[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(numArr));

        int[] creditScore = {750, 810};

        System.out.println(creditScore.length);
//        int[] creditScore =  new int[2];
//        creditScore[0] = 750;
//        creditScore[1] = 810;

        int averageCreditScore = 0;

//        creditScore[0] = 750;
//        creditScore[1] = 810;
//        creditScore[2] = 675;
//
//        averageCreditScore +=   creditScore[0] +   creditScore[1] +   creditScore[2];


        for (int each : creditScore) {
            averageCreditScore+=each;
        }

        averageCreditScore = averageCreditScore/creditScore.length; //3
        System.out.println(averageCreditScore);



    }
}

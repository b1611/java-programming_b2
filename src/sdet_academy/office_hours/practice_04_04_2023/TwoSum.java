package sdet_academy.office_hours.practice_04_04_2023;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        int[] nums = {2, 7, 11, 15};
        System.out.println(Arrays.toString(twoSum2(nums, 9))); // 9= 6+3   // 9-2 =7
    }

    public static int[] twoSum(int[] nums, int target) {
        int[] nums2 = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    nums2[0] = i;
                    nums2[1] = j;
                }
            }
        }
        return nums2;
    }

    public static int[] twoSum2(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> myMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (myMap.containsKey(nums[i])) {
                result[0] = i;
                result[1] = myMap.get(nums[i]);
            } else {
                myMap.put(target - nums[i], i); // k-7  v-0
            }
        }
        return result;
    }

}

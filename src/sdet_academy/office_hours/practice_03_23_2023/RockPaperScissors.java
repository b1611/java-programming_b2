package sdet_academy.office_hours.practice_03_23_2023;

import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        while (true) {
            String[] arr = {"r", "p", "s"};
            String computerMove = arr[new Random().nextInt(arr.length)];

            String playerMove;
            while (true) {
                System.out.println("Please enter your move (r, p or s)");
                playerMove = scan.nextLine();
                if (playerMove.equals("r") || playerMove.equals("p") || playerMove.equals("s")) {
                    break;
                }
                System.out.println("Please enter valid move");
            }
            System.out.println("Computer move " + computerMove);

            if (playerMove.equals(computerMove)) {
                System.out.println("The game is a tie");
            } else if (playerMove.equals("r")) {
                if (computerMove.equals("p")) {
                    System.out.println("Unfortunatly you lose ");
                } else if (computerMove.equals("s")) {
                    System.out.println("Congratulation! You won");
                }
            } else if (playerMove.equals("p")) {
                if (computerMove.equals("s")) {
                    System.out.println("Unfortunatly you lose ");
                } else if (computerMove.equals("r")) {
                    System.out.println("Congratulation! You won");
                }
            } else if (playerMove.equals("s")) {
                if (computerMove.equals("r")) {
                    System.out.println("Unfortunatly you lose ");
                } else if (computerMove.equals("p")) {
                    System.out.println("Congratulation! You won");
                }
            }
            System.out.println("Do you want to play again (y/n)");
            String playAgain = scan.nextLine();
            if (playAgain.equals("n")) {
                break;
            }
        }
    }
}

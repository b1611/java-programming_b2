package sdet_academy.office_hours.practice_03_23_2023;

public class StringRecup {
    public static void main(String[] args) {
        String a = "cat";
        String b;
        b = "rabbit";
        System.out.println(a);
        System.out.println(b);
        String c = "cat";
        System.out.println(c);

/**
 * How many String objects will be created ?
 */

        // Task 1
        String n1 = new String("Hello World"); //2

        // Task 2
        String a1 = new String("World");
        String a2 =new String("World");    //3

        //Task 3
        String st ="Hello";
        String st1 ="Hello";        //1

        //Task 3
        String str ="Hi";
        String str2 ="Hi";
        String str3 =str2;
        String str4 =new String("Hi");
        String str5 =new String("Hi");     //3
    }
}

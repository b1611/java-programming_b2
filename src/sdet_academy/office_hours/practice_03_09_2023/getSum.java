package sdet_academy.office_hours.practice_03_09_2023;
/*
  "a5bc12def100"
 */

public class getSum {

    public static int getSunFromString(String str) {
        String numFromString = "";
        int sum = 0;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i)) || str.charAt(i)=='-') {
                numFromString += str.charAt(i);

                if (i == str.length() - 1 || !Character.isDigit(str.charAt(i + 1))) {
                    sum += Integer.parseInt(numFromString);
                    numFromString = "";
                }
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(getSunFromString("a-5bc12def100k"));
    }
}

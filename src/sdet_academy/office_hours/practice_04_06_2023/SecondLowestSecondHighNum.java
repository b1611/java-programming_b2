package sdet_academy.office_hours.practice_04_06_2023;

import java.util.*;
import java.util.stream.Stream;

public class SecondLowestSecondHighNum {
    public static void main(String[] args) {
//        List<Integer> list = Arrays.asList(1, 3, 5, 10, 20);  //30  // 1
//
//        Integer secHigh = list.stream().sorted(Collections.reverseOrder()).distinct().skip(1).findFirst().get();
//        System.out.println("secHigh = " + secHigh);
//
//        Integer secLow = list.stream().sorted().distinct().skip(2).findFirst().get();
//        System.out.println("secLow = " + secLow);
//        /**
//         * reduction stream operations allow us to produce one single result from a sequence of elements,
//         * by repeatedly applying a combining operation to the elements in the sequence.
//         */
//        int sum = list.stream().reduce(0, (totalSum, each) -> totalSum + each);
//        int multiply = list.stream().reduce(1, (a, b) -> a * b);
//        Integer max = list.stream().reduce(0, (a, b) -> a > b ? a : b);
//        Integer reduce = list.stream().reduce(Integer::max).get();
//        System.out.println("Sum" + sum);
//        System.out.println("Multiply " + multiply);
//        System.out.println("Max number " + max);
//
//        List<String> strArr = Arrays.asList("a", "b", "Radu", "c");
//        String result = strArr
//                .stream()
//                .reduce("", (a, b) -> a.toUpperCase() + b.toUpperCase());
//        System.out.println(result);
//
//        String longestStr = strArr.stream().reduce("", (word1, word2) -> word1.length() > word2.length() ? word1 : word2);
//        System.out.println("longestStr = " + longestStr);

        // System.out.println(sum);
        //----------------------------------------
        Map<Integer, String> map1 = new HashMap<>();
        map1.put(1, "A");
        map1.put(2, "B");
        map1.put(3, "C");

        Map<Integer, String> map2 = new HashMap<>();
        map2.put(1, "A");
        map2.put(3, "C");
        map2.put(2, "B");

        Map<Integer, String> map3 = new HashMap<>();
        map3.put(1, "A");
        map3.put(2, "B");
        map3.put(3, "C");
        map3.put(3, "D");
        map3.put(4, "D");

        // compare on the basis key-value
        System.out.println(map1.equals(map2));
        System.out.println(map1.entrySet().equals(map2.entrySet()));
        //compare for the same key : keySet
        System.out.println(map1.keySet().equals(map3.keySet()));
        // compare by valus
       // if duplicates are not allowed
        System.out.println("Values are equal - "+new ArrayList<>(map1.values()).equals(new ArrayList<>(map2.values())));

/**
 * Compare 2 maps by Key
 */
         //1. Combine keys from 2 maps in HashSet
        Set<Integer> combined = new HashSet<>(map1.keySet()); // added keys from map1
        System.out.println(combined);
        // 2.add keys from map3
        combined.addAll(map3.keySet());
        System.out.println("All keys from two maps" +combined);
        // find the key that is not in map1
        combined.removeAll(map1.keySet());
        System.out.println("Diff key "+ combined);

    }
}

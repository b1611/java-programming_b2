package sdet_academy.office_hours.practice_03_28_2023;

public class ReverseString {
    /*
      reverseString(Liuba)
      (reverseString(iuba)) +"L"                      -> abuiL
      ((reverseString(uba)) + "i") + "L"              -> abui
      (((reverseString(ba)) + "u") +"i")+ "L"         -> abu
      (((reverseString(a)) +"b") + "u") +"i")+ "L"    -> ab
      return "a"                                      -> a
 */
    public static void main(String[] args) {
        String name = "Liuba";
        System.out.println(reverseString(name));
    }

    public static String reverseString(String str) {
        if (str == null || str.length() <= 1) {
            return str;
        }
        return reverseString(str.substring(1)) + str.charAt(0);
    }
}

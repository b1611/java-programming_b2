package sdet_academy.office_hours.practice_03_17_2023;
/*
"absde"
"abcd"
return extra letter
 */

public class FindExtraLetter {
    public static char findDifference(String a, String b) {
        char[] aArr = a.toCharArray();
        char[] bArr = b.toCharArray();
        int aCout = 0;
        int bCount = 0;

        for (int i = 0; i < aArr.length; i++) {
            aCout += aArr[i];
        }
        for (int i = 0; i < bArr.length; i++) {
            bCount += bArr[i];
        }
        return (char) (aCout - bCount);
    }

    public static void main(String[] args) {
        System.out.println(findDifference("abcde","abcd"));
    }
}

package sdet_academy.office_hours.practice_03_17_2023;

import java.util.Arrays;

/**
 * Given alphanumeric String, we need to split the string into substrings of consecutive letters or numbers,
 * sort the individual string and append them back together
 * Ex:
 * Input: "DC501GCCCA098911"
 * Output: "CD015ACCCG011899"
 */
public class SortAlfaNumeric {
    public static String sorted(String str) {
        String temp = "";
        for (int i = 0; i < str.length(); i++) {
            temp += str.charAt(i) + "";
            if (Character.isAlphabetic(str.charAt(i)) && i < str.length() - 1) {
                if (Character.isDigit(str.charAt(i + 1))) {
                    temp += ",";
                }
            }
            if (Character.isDigit(str.charAt(i)) && i < str.length() - 1) {
                if (Character.isAlphabetic(str.charAt(i+1))) {
                    temp += ",";
                }
            }
        }
        String[] arr = temp.split(",");
        String result = "";
        for (String each : arr) {
            char[] sorted = each.toCharArray();
            Arrays.sort(sorted);
            for (char ch: sorted) {
                result += ch+"";
            }
        }
     return result;
    }

    public static void main(String[] args) {
        System.out.println(sorted("DC501GCCCA098911"));
    }
}

package sdet_academy.office_hours.practice_03_27_2023;

public class RecursionCount {
    /*
    print from 1 to 100 without using any loop
     */
    public static void main(String[] args) {
        printNum(1);
    }

    public static void printNum(int num) {
        if (num <= 100) {
            System.out.println(num);
            num++;
            printNum(num);
        }
    }
}

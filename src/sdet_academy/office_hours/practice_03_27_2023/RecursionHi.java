package sdet_academy.office_hours.practice_03_27_2023;

public class RecursionHi {
    public static void main(String[] args) {
        /*
        Say Hi 10 times
         */
        sayHi(10);
    }

    public static void sayHi(int num) {
        System.out.println("Hi");
        if (num <= 1) {
            return;
        }
        num--;
        sayHi(num);
    }
}

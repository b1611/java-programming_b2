package sdet_academy.office_hours.practice_03_07_2023;

/**
 * Create a method to
 * Return the sum of the numbers in the array, returning 0 for an empty array.
 * Except the number 13 is very unlucky, so it does not count and numbers that come immediately after a 13 also do not count.
 * <p>
 * <p>
 * sum13([1, 2, 2, 1]) → 6
 * sum13([1, 1]) → 2
 * sum13([1, 2, 2, 1, 13]) → 6
 */
public class Unlucky13 {

    public static int sumOfLuckyNum(int[] nums) {
        if (nums.length == 0) return 0;
        int sum = 0;
        for (int n = 0; n < nums.length - 1; n++) {
            if (nums[n] == 13) {
                n++;
                continue;
            }
            sum += nums[n];
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] numsArr = {1, 2, 2, 1, 13, 5};
        System.out.println(sumOfLuckyNum(numsArr));
    }
}

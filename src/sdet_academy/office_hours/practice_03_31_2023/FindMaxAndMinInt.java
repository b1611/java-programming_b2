package sdet_academy.office_hours.practice_03_31_2023;

public class FindMaxAndMinInt {
    public static void main(String[] args) {
        int a = 5;
        int b = 77;
        int c = 20;
        findMax(a, b, c);
        findMin(a, b, c);
    }

    public static void findMax(int a, int b, int c) {
        int max = 0;
        while (a > 0 || b > 0 || c > 0) {
            a--;
            b--;
            c--;
            max++;
        }
        System.out.println(max);
    }

    public static void findMin(int a, int b, int c) {
        int min = 0;
        while (a > 0 && b > 0 && c > 0) {
            a--;
            b--;
            c--;
            min++;
        }
        System.out.println(min);
    }
}

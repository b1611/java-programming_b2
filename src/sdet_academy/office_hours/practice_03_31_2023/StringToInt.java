package sdet_academy.office_hours.practice_03_31_2023;

public class StringToInt {
    public static void main(String[] args) {
        /*
    Convert String to integer without using Integer.parseInt() method
     */
        String str = "123";
        System.out.println(convert(str) + 2);
        System.out.println(convert2(str));
    }
    public static int convert(String str) {
        int num = 0;
        int ascZero = '0';  //48
        for (int i = 0; i < str.length(); i++) {
            num = num * 10 + (str.charAt(i) - ascZero);
        }
        return num;
    }

    public static int convert2(String str){
        char [] ch =str.toCharArray();
        int numAsc = '0';
        int sum = 0;
        for (char each : ch) {
            sum = sum * 10 + (each - numAsc);
        }
        return sum;
    }
}

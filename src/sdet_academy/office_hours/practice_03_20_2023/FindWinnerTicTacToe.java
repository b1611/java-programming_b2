package sdet_academy.office_hours.practice_03_20_2023;

/**
 * TIC-TAC-TOE
 * Write the method won(char[]) that should determine who won in tic-tac-toe game.
 * <p>
 * - If X player won, return string: "Player X won"
 * - If O player won, return string: "Player O won"
 * <p>
 * Example:
 * <p>
 * char[] a = {'X', 'X', 'X','-', 'O', '-','-', 'O', '-'};
 * <p>
 * a[0] a[1] a[2]
 * a[3] a[4] a[5]
 * a[6] a[7] a[8]
 * <p>
 * Result: Player X won
 */

public class FindWinnerTicTacToe {
    public static String chooseWinner(char[] arr) {
        String winner = "";
        for (int i = 0, y = 0; y < arr.length; i++, y += 3) {
            String rows = "" + arr[y] + arr[y + 1] + arr[y + 2];
            String colums = "" + arr[i] + arr[i + 3] + arr[i + 6];
            String diagonals = "";

            if (i == 0) {
                diagonals = "" + arr[i] + arr[i + 4] + arr[i + 8];
            } else if (i == 2) {
                diagonals = "" + arr[i] + arr[i + 2] + arr[i + 4];
            }
            if (rows.equals("XXX") || colums.equals("XXX") || diagonals.equals("XXX")) {
                winner = "Player X won";
            } else if (rows.equals("OOO") || colums.equals("OOO") || diagonals.equals("OOO")) {
                winner = "Player O won";
            }
        }
        return winner;
    }



    public static void main(String[] args) {
        char[] a = {'X', 'X', 'X','-', 'O', '-','-', 'O', '-'};
        System.out.println(chooseWinner(a));
    }
}

package sdet_academy.office_hours.practice_03_20_2023;

public class TypeCasting {
    public static void main(String[] args) {
        byte a ;
        short b;
        char c;
        int d =1;
        long e;
        float f;
        double g;

       // d=a;
        a=(byte) d;
        System.out.println(d);
    }
}

package sdet_academy.office_hours.practice_03_02_2023;

public class LongestPalindrome {
    /**
     * "java", "apple", "telephone","racecar", "mom", "anna"
     */

    public static void main(String[] args) {
        String[] words = {"java", "apple", "telephone", "racecar", "mom", "anna"};
        String longestPalindrome = "";

        for (String each : words) {
            boolean isPalindrome = true;
            for (int i = 0; i < each.length() / 2; i++) {
                if (each.charAt(i) != each.charAt(each.length() - 1-i)) {
                    isPalindrome = false;
                    break;
                }
            }
            if (isPalindrome && each.length() > longestPalindrome.length()) {
                longestPalindrome = each;
            }

        }
        System.out.println("Longest Palindrome is : " +longestPalindrome);
    }
}

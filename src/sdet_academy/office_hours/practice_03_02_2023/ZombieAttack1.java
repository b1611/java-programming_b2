package sdet_academy.office_hours.practice_03_02_2023;

import java.util.Arrays;

public class ZombieAttack1 {
    /**
     * An array **inhabitants** represents cities and their respective populations. For example, the following arrays shows 8 cities and their respective populations:
     * <p>
     * [3, 6, 0, 4, 3, 2, 7, 1]
     * <p>
     * Some cities have a population of 0 due to a pandemic zombie disease that is wiping out the human lives. After each day, every city will lose half of its population.
     * <p>
     * Write a program to loop though each city population and make it lose half of its population **until all cities have no humans left**. Make updates to each element in the array And print the array like below for each day:
     * Day 0 [3, 6, 0, 4, 3, 2, 7, 1]
     * Day 1 [1, 3, 0, 2, 1, 1, 3, 0]
     * Day 2 [0, 1, 0, 1, 0, 0, 1, 0]
     * Day 3 [0, 0, 0, 0, 0, 0, 0, 0]
     * ---- EXTINCT ----
     * Write the program in a way that it will handle any number of people in cities, above was just an example
     */
    public static void main(String[] args) {
//        Scanner scan = new Scanner(System.in);
//        int[] population = {scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.nextInt(), scan.nextInt(),};
        int[] population = {3, 6, 0, 4, 3, 2, 7, 1};
        int[] extinctCities = new int[population.length];
        int countDays = 0;

        System.out.println("Day " + countDays + " " + Arrays.toString(population));
        do {
            for (int i = 0; i < population.length; i++) {
                population[i] = population[i] / 2;
            }
            countDays++;
            System.out.println("Day " + countDays + " " + Arrays.toString(population));
        }
        while (!Arrays.equals(population, extinctCities));

        System.out.println("---- EXTINCT ----");
    }

}

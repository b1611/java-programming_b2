package sdet_academy.office_hours.practice_03_02_2023;

import java.util.Arrays;

public class ZombieAttackSum {
    public static void main(String[] args) {

        int[] population = {3, 6, 0, 4, 3, 2, 7, 1};
        int countDays = 0;
        int sum = -1;

        System.out.println("Day " + countDays + " " + Arrays.toString(population));
        while (sum != 0) {
            sum = 0;
            for (int i = 0; i < population.length; i++) {
                population[i] = population[i] / 2;
                sum += population[i];
            }
            countDays++;
            System.out.println("Day " + countDays + " " + Arrays.toString(population));
        }

        System.out.println("---- EXTINCT ----");
    }
}

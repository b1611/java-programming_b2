package sdet_academy.office_hours.practice_03_06_2023;

import java.util.Arrays;

/**
 * Given a non-empty array of ints, return a new array containing the elements from the original array that come after the last 4 in the original array.
 * The original array will contain at least one 4. Note that it is valid in java to create an array of length 0.
 *
 *
 * post4([2, 4, 1, 2]) → [1, 2]
 * post4([4, 1, 4, 2]) → [2]
 * post4([4, 4, 1, 2, 3]) → [1, 2, 3]
 */
public class ArrayPost4 {
    public static int [] post4( int [] nums){
        int size =0;
        for (int i = nums.length-1; i >=0 ; i--) {
            if(nums[i] ==4){
                size = nums.length-1-i;
                break;
            }
        }
        int [] arr =new int [size];
        for (int j = 0; j < arr.length; j++) {
            arr[j] =nums[nums.length-size+j];
        }
        return arr;
    }

    public static void main(String[] args) {
        int []mainArray ={4, 4, 1, 2, 3};
        System.out.println(Arrays.toString(post4(mainArray)));
    }
}

package sdet_academy.office_hours.practice_03_13_2023;

import java.util.ArrayList;

/**
 * Write a program that can extract the special characters, digits and letters from a string and stores them into separate ArrayLists of Characters
 * <p>
 * Ex: str = "ABCD123$%#@&456EFG!"
 * list1: {$, %, #, @, &, !}
 * list2: {A, B, C, D, E, F, G}
 * list3: {1, 2, 3, 4, 5, 6}
 * <p>
 * Challenge: make a method that will do this action and it will return an ArrayList which holds all 3 of the other ArrayLists
 */
public class SeparateParts {
    public static void main(String[] args) {
        String str = "ABCD123$%#@&456EFG!";
        ArrayList<ArrayList<Character>> allParts = new ArrayList<>();
        ArrayList<Character> letters = new ArrayList<>();
        ArrayList<Character> digits = new ArrayList<>();
        ArrayList<Character> symbols = new ArrayList<>();
        allParts.add(letters);
        allParts.add(digits);
        allParts.add(symbols);

        for (int i = 0; i < str.length(); i++) {
            char eachLetter = str.charAt(i);
            if (Character.isLetter(eachLetter)) {
                letters.add(eachLetter);
            } else if (Character.isDigit(eachLetter)) {
                digits.add(eachLetter);
            } else {
                symbols.add(eachLetter);
            }
        }
        System.out.println("Letters " + letters);
        System.out.println("Digits " + digits);
        System.out.println("Symbol " + symbols);
        System.out.println(separateParts(str, allParts));
    }

    public static ArrayList<ArrayList<Character>> separateParts(String str, ArrayList<ArrayList<Character>> allParts) {


        for (int i = 0; i < str.length(); i++) {
            char eachLetter = str.charAt(i);

            if (Character.isLetter(eachLetter)) {
                allParts.get(0).add(eachLetter);
            } else if (Character.isDigit(eachLetter)) {
                allParts.get(1).add(eachLetter);
            } else {
                allParts.get(2).add(eachLetter);
            }
        }
        return allParts;
    }
}

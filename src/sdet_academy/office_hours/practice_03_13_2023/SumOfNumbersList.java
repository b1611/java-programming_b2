package sdet_academy.office_hours.practice_03_13_2023;

import java.util.ArrayList;

public class SumOfNumbersList {
    public static void main(String[] args) {
        String[] str = {"123", "54", "24", "5", "44"};
        System.out.println(listOfSums(str));
    }

    public static ArrayList<String> listOfSums(String[] arr) {
        ArrayList<String> list = new ArrayList<>();

        for (String each : arr) {
            int sum = 0;
            for (char digit : each.toCharArray()) {
                sum += Integer.parseInt(digit + "");
            }
            list.add(sum + "");
        }
        return list;
    }
}

package sdet_academy.office_hours.practice_03_24_2023;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MiddleAverage {
    /**
     * Write a custom method that accepts an ArrayList of integers and return average sum of elements, excluding min and max elements.
     * <p>
     * Performs the following operations:
     * Remove the minimum and maximum elements from the ArrayList.
     * Calculate the average of the remaining elements in the ArrayList.
     */
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(8, 2, 6, 4, 3, 7));
        System.out.println(calculateMidleAverage(list));
    }

    public static int calculateMidleAverage(ArrayList<Integer> nums) {
        Integer min = Collections.min(nums);
        Integer max = Collections.max(nums);
        nums.remove(min);
        nums.remove(max);

        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        return sum / nums.size();
    }
}

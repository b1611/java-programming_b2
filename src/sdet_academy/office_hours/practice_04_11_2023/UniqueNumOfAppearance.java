package sdet_academy.office_hours.practice_04_11_2023;

import java.util.*;

public class UniqueNumOfAppearance {
    /**
     * Given an array of integers arr, return true if the number of occurrences of each value in the array is unique or false otherwise.
     * Example :
     * Input: arr = [1,2,2,1,1,3]
     * Output: true
     * Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same number of occurrences.
     *
     * @param
     * @return
     */
    public static void main(String[] args) {
        int[] arr = {-3, 0, 1, -3, 1, 1, 1, -3, 10, 0};
        System.out.println(uniqueOccurrences(arr));
    }
    public static boolean uniqueOccurrences(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int each : arr) {
            if (map.containsKey(each)) map.put(each, map.get(each) + 1);
            else map.put(each, 1);
        }
        Set<Integer> set = new HashSet<>(map.values());

        return map.size() == set.size();
    }
}

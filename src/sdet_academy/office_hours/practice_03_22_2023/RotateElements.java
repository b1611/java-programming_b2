package sdet_academy.office_hours.practice_03_22_2023;

import java.util.ArrayList;

/*
Write a method to rotate the elements of an ArrayList by a specified number of positions.
EX:  position 2,
Before rotation: [red, green, blue, yellow]
After rotation: [blue, yellow, red, green]
 */
public class RotateElements {
    public static void main(String[] args) {
        ArrayList<String> colours = new ArrayList<>();
        colours.add("red");
        colours.add("green");
        colours.add("blue");
        colours.add("black");
        colours.add("yellow");
        System.out.println(colours);
       // System.out.println(rotateByPosition(colours, 3));
        rotateByPosition2(colours,3);
        System.out.println(colours);
    }

    public static ArrayList<String> rotateByPosition(ArrayList<String> list, int position) {
        for (int i = 0; i < position; i++) {
            String temp = list.remove(0);
            list.add(temp);
        }
        return list;
    }
    public static void rotateByPosition2(ArrayList<String> list, int position) {
        for (int i = 0; i < position; i++) {
            String temp = list.remove(0);
            list.add(temp);
        }
    }
}

package sdet_academy.office_hours.practice_03_10_2023;
/*
Given a string s, reverse only all the vowels in the string and return it.
The vowels are 'a', 'e', 'i', 'o', and 'u', and they can appear in both lower and upper cases, more than once.

Input: s = "hello"
Output: "holle"
 */
//    [h, e, l, l, o]

public class ReverseOnlyVowels {
    public static String reverseVowels(String str) {
        char[] chArr = str.toCharArray();
        int i = 0;
        int j = chArr.length - 1;
        char temp;

        while (i <= j) {
            if (isVowel(chArr[i]) && isVowel(chArr[j])) {
                temp = chArr[i];
                chArr[i] = chArr[j];
                chArr[j] = temp;
                i++;
                j--;
            }
            if (!isVowel(chArr[i]) && isVowel(chArr[j])) {
                i++;
            }
            if (isVowel(chArr[i]) && !isVowel(chArr[j])) {
                j--;
            } else if (!isVowel(chArr[i]) && !isVowel(chArr[j])) {
                i++;
                j--;
            }
        }
        return String.valueOf(chArr);
    }

    public static boolean isVowel(char ch) {
        return (ch == 'a' || ch == 'A' || ch == 'e' || ch == 'E' || ch == 'i' || ch == 'I' || ch == 'o' || ch == 'O' || ch == 'u' || ch == 'U');
    }

    public static void main(String[] args) {
        long start =System.currentTimeMillis();
        String s = "hello today is a sunny day";
        System.out.println(reverseVowels(s));
        long end = System.currentTimeMillis();
        System.out.println(end-start);

    }
}

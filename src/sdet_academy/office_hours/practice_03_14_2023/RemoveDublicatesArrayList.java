package sdet_academy.office_hours.practice_03_14_2023;

import java.util.ArrayList;
import java.util.Arrays;

/*
Create a method that will take an ArrayList of numbers and remove any duplicates values. The method will return an ArrayList of unique elements.
@param nums - The given ArrayList of numbers
@return - ArrayList of uniques numbers
Ex:
  Input: {1, 3, 5, 1, 4, 5, 9};
  Output: {3, 4, 9};
*/
public class RemoveDublicatesArrayList {
    public static ArrayList<Integer> removeDublicates(ArrayList<Integer> list) {
        ArrayList<Integer> uniques = new ArrayList<>();
        String checked = "";

        for (int i = 0; i < list.size(); i++) {
            int count = 0;
            if (!checked.contains(list.get(i) + "")) {
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(i) == list.get(j)) {
                        count++;
                    }
                }
                if (count == 1) {
                    uniques.add(list.get(i));
                }
                checked+= list.get(i);
            }
        }
        return uniques;
    }

    public static void main(String[] args) {
        ArrayList<Integer> digits = new ArrayList<>(Arrays.asList(1, 3, 5, 1, 3, 3, 4, 5, 9));

        System.out.println(removeDublicates(digits));
    }
}

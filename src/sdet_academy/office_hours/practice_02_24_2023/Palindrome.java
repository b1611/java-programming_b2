package sdet_academy.office_hours.practice_02_24_2023;

public class Palindrome {
    /**
     * is it palindrome int 123454321
     */
    public static void main(String[] args) {
        int num = 123454321;
        String numStr = String.valueOf(num);
        boolean isPalindrome = true;

        for (int i = 0; i < numStr.length() / 2; i++) {
            if (numStr.charAt(i) != numStr.charAt(numStr.length() - 1 - i)) {
                isPalindrome = false;
                break;
            }
        }
        System.out.println(isPalindrome);
    }
}

package sdet_academy.office_hours.practice_02_24_2023;

public class LongestSubstring {
    /**
     * "aaaaaaaaabbbcccccddddee"
     */

    public static void main(String[] args) {
        String str = "aaaabbbbbbbcccccddddeeeeeeee";
        String longestSubstring = "";
        String temp = " ";

        for (int i = 0; i < str.length() - 1; i++) {
            temp += str.charAt(i);

            if (str.charAt(i) == str.charAt(i + 1) && i == str.length() - 2) {
                temp += str.charAt(i + 1);
                if (temp.length() > longestSubstring.length()) {
                    longestSubstring = temp;
                }
            }

            if (str.charAt(i) != str.charAt(i + 1)) {
                if (temp.length() > longestSubstring.length()) {
                    longestSubstring = temp;
                }

                temp = " ";
            }
        }
        System.out.println("Longest substring is " + longestSubstring);
    }
}

package sdet_academy.office_hours.practice_03_03_2023;

import java.util.Arrays;

public class ZombieAttack2 {
    /**
     * An array **inhabitants** represents cities and their respective populations. For example, the following arrays shows 8 cities and their respective populations:
     * <p>
     * ```
     * [3, 6, 0, 4, 3, 2, 7, 0]
     * ```
     * <p>
     * Some cities have a population of 0 due to a pandemic zombie disease that is wiping out the human lives. After each day, **any city that is adjacent to a zombie-ridden city will lose half of its population (City that has a 0 population)**.
     * <p>
     * Write a program to loop though each city population and make it lose half of its population if it is adjacent(right or left) to a city with zero people until all cities have no humans are left.
     * <p>
     * Make updates to each element in the array And print the array like below for each day:
     * <p>
     * ```
     * Day 0 [3, 6, 0, 4, 3, 2, 7, 0]
     * Day 1 [3, 3, 0, 2, 3, 2, 3, 0]
     * Day 2 [3, 1, 0, 1, 3, 2, 1, 0]
     * Day 3 [3, 0, 0, 0, 3, 2, 0, 0]
     * Day 4 [1, 0, 0, 0, 1, 1, 0, 0]
     * Day 5 [0, 0, 0, 0, 0, 0, 0, 0]
     * ---- EXTINCT ----
     * ```
     * <p>
     * Write the program in a way that it will handle any number of people in cities, above was just an example.
     */
    public static void main(String[] args) {
        int[] population = {3, 6, 0, 4, 3, 2, 7, 0};
        int[] extinctCities = new int[population.length];
        int[] tempPopulation = Arrays.copyOf(population, population.length);

        int countDays = 0;

        System.out.println("Day " + countDays + " " + Arrays.toString(population));

        while (!Arrays.equals(population, extinctCities)) {
            for (int i = 0; i < population.length; i++) {
                if (population[i] == 0 && i != 0 && i != population.length - 1) {
                    tempPopulation[i - 1] = population[i - 1] / 2;
                    tempPopulation[i + 1] = population[i + 1] / 2;
                }
                if (i == 0 && population [i]==0){
                    tempPopulation[i + 1] = population[i + 1] / 2;
                }
                if(i == population.length - 1 && population [i]==0){
                    tempPopulation[i - 1] = population[i - 1] / 2;
                }
            }
            population = Arrays.copyOf(tempPopulation, tempPopulation.length);
            countDays++;
            System.out.println("Day " + countDays + " " + Arrays.toString(population));
        }
        System.out.println("---- EXTINCT ----");
    }
}

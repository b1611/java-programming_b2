package sdet_academy.office_hours.practice_03_03_2023;

import java.util.Arrays;

/**
 * Write a program that can count the even and odd number from an array of integers
 * Print Array of Odd numbers
 *  Print Array of Even numbers
 *  Input: [4,1,3,12,5]
 *  Output:
 * Even: 2
 * Odd: 3
 */
public class OddAndEvenNumbersFromArray {
    public static void main(String[] args) {
        int[] nums = {4, 1, 3, 12, 5};
        int countEven = 0;
        int countOdd = 0;

        for (int eachNumber : nums) {
            if (eachNumber % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }
        System.out.println("We have " + countEven + " even numbers in our Array");
        System.out.println("We have " + countOdd + " odd numbers in our Array");

        int[] evenNumbers = new int[countEven];
        int[] oddNumbers = new int[countOdd];

        for (int i = 0, j = 0, k = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                evenNumbers[j++] = nums[i];

            }else {
                oddNumbers[k++]=nums[i];
            }
        }
        System.out.println("Even numbers "+ Arrays.toString(evenNumbers));
        System.out.println("Odd numbers "+ Arrays.toString(oddNumbers));
    }
}

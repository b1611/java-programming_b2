package sdet_academy.office_hours.practice_02_28_2023;

/*
{4,3,1,4,5,2,4,8,5,8}
 */
public class SecondBiggestNumber {
    public static void main(String[] args) {
        int[] nums = {4, 3, 1, 5, 2, 8, 10, 9};
        //  Arrays.sort(nums);
//        System.out.println(Arrays.toString(nums));
//        System.out.println(nums[nums.length-2]);

        int max = nums[0];
        int secMax = nums[0];

        for (int eachNum : nums) {
            if (eachNum > max) {
                secMax = max;
                max = eachNum;
            }
            if (eachNum > secMax && eachNum < max) {
                secMax = eachNum;
            }
        }

//        for (int i = 0; i < nums.length-1; i++) {
//            if (nums[i] > max) {
//                secMax = max;
//                max = nums[i];
//            }
//            if (nums[i] > secMax && nums[i] < max) {
//                secMax = nums[i];
//            }
//        }
//
        System.out.println("Second max number is " + secMax);
    }
}

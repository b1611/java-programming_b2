package sdet_academy.office_hours.practice_02_28_2023;
/*
        Given a number. Print out all the prime numbers from 2 to that number
        A prime number is a number that is only divisible by 1 and itself.
         */

import java.util.Scanner;

public class PrimeInRange {
    public static void main(String[] args) {
        Scanner scan =new Scanner(System.in);
        System.out.println("Enter the number, please -");
        int num = scan.nextInt();

        for (int i = 2; i < num; i++) {
            int count =0;

            for (int j = 2; j <i ; j++) {
                if(i%j==0){
                    count++;
                }
            }
            if(count==0){
                System.out.print(i+" ");
            }
        }
        System.out.println("\nThis numbers are prime numbers in your range, thank you!");
    }
}

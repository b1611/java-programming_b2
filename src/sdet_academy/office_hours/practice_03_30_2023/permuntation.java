package sdet_academy.office_hours.practice_03_30_2023;

public class permuntation {
    /**
     * * // Find all permutations of a string. Allow duplicates.
     * * // for example, 'ab' -> 'ab', 'ba'
     * * // for 'abc' -> 'abc', 'bca', 'bac', 'cab', 'acb', and 'cba'
     * * // for 'abb' -> 'abb', 'abb', 'bab', 'bab', 'bba', and 'bba'
     */

    public static void main(String[] args) {
        String str = "abc";
        permuntationStr(str, "");

    }
    public static void permuntationStr(String str, String result) {
        if (str.length() == 0) {
            System.out.println(result);
            return;
        }
        for (int i = 0; i < str.length(); i++) { //2   abc
            String ch = str.charAt(i) + "";
            String temp = str.substring(0, i) + str.substring(i + 1); //bc
            permuntationStr(temp, result + ch);
        }
    }
}

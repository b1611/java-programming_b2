package sdet_academy.day_18;

import java.util.Arrays;

public class ArrayOfStrings {
    public static void main(String[] args) {

        String[] arr = {"Iryna", "Radu", "Luba", "Slava", "James", "Angie", "Myroslava"};
        System.out.println(Arrays.toString(arr));
        System.out.println(arr.length);
        System.out.println(arr[0]);
        System.out.println(arr[arr.length - 1]);


        String[] arrStr = new String[4];

        arrStr[0] = "Java";
        arrStr[1] = "C#"; // - C sharp, not C hashtag not C pound
        arrStr[2] = "C++";
        System.out.println(Arrays.toString(arrStr));

        for (int i = 0; i< arr.length; i++){
            if(i%2==1) {
                System.out.println(arr[i]);
            }
        }
    }
}

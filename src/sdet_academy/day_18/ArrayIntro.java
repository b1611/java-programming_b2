package sdet_academy.day_18;

import java.util.Arrays;

public class ArrayIntro {
    public static void main(String[] args) {

        int num = 10, num2 = 20, num3 = 30, num4 = 40;

        int[] arrNum = new int[5]; //- declaring oan array
        //assigning my array
        arrNum[0] = 10;
        arrNum[1] = 20;
        arrNum[2] = 30;
        arrNum[4] = 40;
//        arrNum[3] = 17; - if the element is not assigned it will default to 0 with the numbers



        System.out.println(arrNum); // - [I@1b0375b3 - hashcode
        System.out.println(Arrays.toString(arrNum));
        System.out.println(arrNum[4]);



        int[] arr = {4,5,9,7,5,3}; // - declaring and initializing right away
        System.out.println(arr.length);

//        int[] arrEmpty = {};
        int[] arrEmpty = new int[3]; // length-1
        System.out.println(Arrays.toString(arrEmpty));
        arrEmpty[0] = 100;
        arrEmpty[1] = 200;
        arrEmpty[2] = 300;
        //arrEmpty[3] = 400; //- will give us ArrayIndexOutOfBoundException

        System.out.println(Arrays.toString(arrEmpty));
        arrEmpty = arrNum;

        int a = 10;
        int b = a;

        System.out.println(Arrays.toString(arrEmpty));

        int element = arrEmpty[1];

    }
}

package sdet_academy.day_18;

import java.util.Arrays;

public class ArrayPractice {
    public static void main(String[] args) {
        /**
         * [Java, javaScript, JavaScript, Python, java, kotlin, C#]
         */
        String[] arr = {"Java", "javaScript", "JavaScript", "Python", "java", "kotlin", "C#" };
//
//        for (int i =0; i<arr.length; i++){
//            arr[i] = arr[i].toLowerCase();
//        }


        System.out.println(Arrays.toString(arr));


        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equalsIgnoreCase("java") || arr[i].toLowerCase().contains("java")) {
                count++;
            }
        }
        System.out.println(count);
    }
}

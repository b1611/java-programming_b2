package sdet_academy.day_33;

import java.util.Arrays;

public class ArrayStreams {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 1, 2, 3, 4, 5, 6, 7};
        //          [1,2,3,4,5,6,7]
        int[] result = Arrays.stream(arr).distinct().toArray();
        System.out.println(Arrays.toString(result));

        System.out.println(Arrays.toString(removeDuplicates(arr)));
    }

    public static int[] removeDuplicates(int[] arr) {
        String temp = "";
        for (int i = 0; i < arr.length; i++) {
            if (!temp.contains("" + arr[i])) {
                temp += arr[i] + ",";
            }
        }

        String[] split = temp.split(",");
        int[] result = new int[split.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(split[i]);
        }
        return result;
    }
}

package sdet_academy.day_3;

public class DynamicTriangle {
    public static void main(String[] args){

        String character = "%";
        System.out.println(character);
        System.out.println(character + " " + character);
        System.out.println(character + "  " + character);
        System.out.println(character + "   " + character);
        System.out.println(character + "    " + character);
        System.out.println(character + character + character + character + character + character + character);
    }
}

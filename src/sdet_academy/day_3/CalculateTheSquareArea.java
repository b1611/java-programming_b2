package sdet_academy.day_3;

import java.util.Scanner;

public class CalculateTheSquareArea {
    public static void main(String[] args){

        /**
         * Create an object of the Scanner
         * Create a variable for 1 side of the square assuming all sides are equal
         * Use scanner to allow user to input the size of the side of the square to calculate the area.
         * Hint: formula = a*a;
         */

        Scanner in = new Scanner(System.in);

        int sideOfTheSquare = in.nextInt();
        int result = sideOfTheSquare*sideOfTheSquare;

        System.out.println("The area of the square is " + result);


    }
}

package sdet_academy.day_3;

import java.util.Scanner;

public class MoreSquarePractice {
    public static void main(String[] args){
        /**
         * Create an object of the Scanner
         * Create a variable for 2 sides of the square
         * Use scanner to allow user to input the size of the side of the square to calculate the area.
         * Hint: formula = a*b;
         */

        Scanner scan = new Scanner(System.in);
        long sideOne;
        long sideTwo;
        long result;

        System.out.println("Enter the side one:");
        sideOne = scan.nextLong();
        System.out.println("Enter the side two:");
        sideTwo = scan.nextLong();
        result = sideOne*sideTwo;
        System.out.println("The result is " + result);

    }
}

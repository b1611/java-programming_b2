package sdet_academy.day_3;

public class WalmartReceipt {
    public static void main(String[] args){
        /**
         * Create the following variables:
         * - Milk
         * - price for milk
         * - Bread
         * - price for bread
         * - Eggs
         * - price for eggs
         * - total amount
         */

        String productOne = "Milk";
        String productTwo = "Bread";
        String productThree = "Eggs";
        double priceProductOne = 8.99;
        double priceProductTwo = 4.95;
        double priceProductThree = 7.85;
//        double total = priceProductOne + priceProductTwo + priceProductThree;

        System.out.println("******************************");
        System.out.println("*****WELCOME TO WALMART*******");
        System.out.println("******************************");
        System.out.println("Product one: " + productOne + " price - $" + priceProductOne);
        System.out.println("Product one: " + productTwo + " price - $" + priceProductTwo);
        System.out.println("Product one: " + productThree + " price - $" + priceProductThree);
        System.out.println("Total: $" + (priceProductOne + priceProductTwo + priceProductThree)); // need to put variables into
                                                                                                // parenthesis so we can do the addition not concatenation


    }
}

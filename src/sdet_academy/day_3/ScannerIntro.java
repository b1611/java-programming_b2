package sdet_academy.day_3;

import java.util.Scanner;

public class ScannerIntro {
    public static void main(String[] args){
        /**
         * Add a country of residence and language
         */

        Scanner in = new Scanner(System.in); // creating an object of the scanner


        System.out.println("Please enter your name");
        String name = in.nextLine();
        System.out.println("My name is " + name);
        System.out.println("Please enter your age");
        byte age = in.nextByte();
        System.out.println("Your age is " + age);
        System.out.println("Please enter your phone number");
        long phoneNumber = in.nextLong();
        System.out.println("Your phone number is " + phoneNumber);
        System.out.println("Please enter the country of residence");
        String country = in.next();
        System.out.println("You live in " + country);
        System.out.println("What language do you speak?");
        String language = in.next();
        System.out.println("You speak " + language);


    }
}

package sdet_academy.day_3;

public class EmployeeInfo {
    public static void main(String[] args) {

        /**
         * Please create a variables for department and starting date and print those using String concatenation
         */

        String employeeName = "John Wick";
        byte employeeId = 15;
        String employeeDateOfBirth = "01/01/1985";
        double employeeSalary = 135_000.00;
        char gender = 'F';
        long phoneNumber = 92988888888l;
        String departmentName = "\"Finance\"";
        String startingDate = "01/12/2005";


        System.out.println("The employee name is " + employeeName);
        System.out.println("The employee id is " + employeeId);
        System.out.println("Date of Birth: " + employeeDateOfBirth);
        System.out.println("Salary: " + employeeSalary);
        System.out.println("The employee gender is " + gender);
        System.out.println("The employee phone number is " + "\"" + phoneNumber + "\"");
        System.out.println("The department name is " + departmentName);
        System.out.println("The starting date is " + startingDate);


    }
}

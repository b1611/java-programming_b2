package sdet_academy.day_23;

import java.util.Arrays;

public class RemoveOdd {
    public static void main(String[] args) {

        /**
         * Remove element from an array if the element length is odd.
         */


        String[] strArr = {"Red", "Yellow", "Blue", "Green", "Purple", "Pink", "Black", "White", "Brown", "Grey"};

        //                  [Yellow, Blue, Purple, Pink, Grey] - String[]



        String temp = "";

        for (String each : strArr) {
            if(each.length()%2==0){
                temp += each;
            }
        }

        System.out.println(temp);

        String[] split = temp.split(",");
        System.out.println(Arrays.toString(split));

    }
}

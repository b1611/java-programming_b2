package sdet_academy.day_23;

import java.util.Arrays;

public class ReplaceElements {
    public static void main(String[] args) {
        /**
         * if a length of an element is odd - replace middle character with #
         * if the length of the element is even - replace the element with "0"
         */

        String[] strArr = {"Red", "Yellow", "Blue", "Green", "Purple", "Pink", "Black", "White", "Brown", "Grey"};
        //                  R#d,       0,      0,    Gr#en,      0,        0,     Bl#ck,  Wh#te,   Br#wn,   0


        String strTemp;
        for (int i = 0; i < strArr.length; i++) {
            strTemp=strArr[i];
            if(strArr[i].length()%2==1){
                strArr[i]=strTemp.substring(0,strTemp.length()/2)+"#"+ strTemp.substring(strTemp.length()/2+1);

                // System.out.println(Arrays.toString(strArr));
            }else{
                strArr[i]="0";
            }
        }
        System.out.println(Arrays.toString(strArr));
    }
}

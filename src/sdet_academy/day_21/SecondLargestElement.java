package sdet_academy.day_21;

import java.util.Arrays;

public class SecondLargestElement {
    public static void main(String[] args) {

        int[] numArr = {1,-9,8,7,-6,5,-4};
        int max = 0;
        int secondMax = 0;

        for (int each : numArr) {
            if(max<each){
                max = each;
            }
            if(each<max && each>secondMax){
                secondMax = each;
            }
        }

        System.out.println(max);
        System.out.println(secondMax);

        Arrays.sort(numArr);
        System.out.println(numArr[numArr.length-2]);
        System.out.println(numArr[1]);
    }
}

package sdet_academy.day_21;

public class NegativeElements {
    public static void main(String[] args) {

        /**
         * [1,-9,8,7,-6,5,0,-4] - given array
         * sum up all the negative elements = -9 + -6 + -4 = -19
         */

        int[] numArr = {1,-9,8,7,-6,5,0,-4};
        int sum = 0;

        for(int each:numArr){ //- iter or foreach for the shortcut
            if(each<0){
                sum+=each;
            }
        }
        System.out.println(sum);
    }
}

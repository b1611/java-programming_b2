package sdet_academy.day_21;

import java.util.Arrays;

public class SortArray{
    public static void main(String[] args) {

        int[] numArr = {2,6,52,5,25,0};

        //              52, 25, 6 ,5, 2, 0

        for (int i = 0; i < numArr.length; i++) {
            for (int j = 0; j < numArr.length; j++) {
                if(numArr[i]>numArr[j]){
                    int temp;
                    temp = numArr[i];
                    numArr[i] = numArr[j];
                    numArr[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(numArr));


        Arrays.sort(numArr);
        System.out.println(Arrays.toString(numArr));

        int[] newArr = new int[numArr.length];

        /**
         *      for (int i = 0, j = numArr.length-1; i < newArr.length; i++, j--) {
         *             newArr[i] = numArr[j];
         *         }
         */

        for (int i = 0; i < newArr.length; i++) {
            newArr[i] = numArr[numArr.length-1 - i];
        }

        System.out.println(Arrays.toString(newArr));
    }
}

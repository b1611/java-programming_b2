package sdet_academy.day_21;

import java.util.Arrays;

public class Anagram {
    public static void main(String[] args) {

        /**
         * silent and listen
         * earth and heart
         * triangle and integral
         * study and dusty
         */

        String strOne = "Silent";
        strOne = strOne.replace(" ", "").toLowerCase();
        String strTwo = "Listen";
        strTwo = strTwo.replace(" ", "").toLowerCase();
        boolean isAnagram = true;

        char[] charOne = strOne.toCharArray();
        char[] charTwo = strTwo.toCharArray();

        Arrays.sort(charOne);
        Arrays.sort(charTwo);

        if (!Arrays.equals(charOne, charTwo)){
            isAnagram = false;
        }
//        System.out.println(charOne==charTwo); - will point to the memory slot
//        System.out.println(Arrays.equals(charOne, charTwo)); - will actually compare the values and the size

        System.out.println("isAnagram = " + isAnagram);

//        ATTENTION, THE SOLUTIONS BELOW WILL HAVE A BUG IN THEM
//
//        for (int i = 0; i < strOne.length(); i++) {
//            if(strTwo.contains("" + strOne.charAt(i)) && strOne.length()==strTwo.length()){
//                isAnagram= true;
//            }else{
//                isAnagram = false;
//                break;
//            }
//        }
//
//        System.out.println(isAnagram);
//
//        for (int i = 0; i < strOne.length(); i++) {
//            if(!strTwo.contains("" + strOne.charAt(i)) ||  strOne.length()!=strTwo.length()){
//                isAnagram= false;
//            }
//        }
//
//        System.out.println(isAnagram);
//
    }
}

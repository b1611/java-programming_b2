package sdet_academy.day_21;

import java.util.Arrays;

public class ForEachLoopIntro {
    public static void main(String[] args) {

        String[] strArr = {"Hello", "Hola", "Bye", "Chao", "As ta la vista"};

        for (int i = 0; i <strArr.length ; i++) {
            System.out.println(strArr[i]);
        }
        for(String each:strArr){
            System.out.println(each);
        }

        int[] numArr = {1,2,3,3,56,64,8};
        //              2, 4, 6, 6, 132, 128, 16
        for (int i = 0; i < numArr.length; i++) {
            numArr[i] = numArr[i]*2;
        }

        System.out.println(Arrays.toString(numArr));

        for(int each:numArr){
            each=each*2;
        }

        System.out.println(Arrays.toString(numArr));
    }
}

package sdet_academy.day_21;

import java.util.Arrays;

public class ArrayPractice {
    public static void main(String[] args) {

        int[] numArr = {1, -9, 8, 7, -6, 5, 0, -4};
        String result = ""; //19876504

        for (int each : numArr) {
            result += each;
        }

        result = result.replace("-", "");
        System.out.println(result);

        String numStr = Arrays.toString(numArr);
        System.out.println(numStr);
        numStr = numStr.replace("[", "")
                .replace("]", "")
                .replace("-", "")
                .replace(",", "")
                .replace(" ", "");
        System.out.println(numStr);
    }
}

package sdet_academy.day_21;

public class StringBuilderIntro {
    public static void main(String[] args) {

        String str = "Hello"; // String is immutable
        String strResult = "";//olleH

        for (int i = str.length()-1; i >=0; i--) {
            strResult+=str.charAt(i);
        }

        System.out.println(strResult);

//StringBuilder and String Buffer are mutable
        // Buffer is synchronized(thread-safe) and builder is not
        StringBuilder stringBuilder = new StringBuilder(str); // accepts String as an argument and creates an object of StringBuilder
        StringBuilder reverse = stringBuilder.reverse();// reverse method will need to be assigned to the new variable
        System.out.println(reverse);

        StringBuffer stringBuffer = new StringBuffer(str);
        StringBuffer reverse1 = stringBuffer.reverse();
        System.out.println(reverse1);
    }
}

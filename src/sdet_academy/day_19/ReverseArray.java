package sdet_academy.day_19;

import java.util.Arrays;
import java.util.Random;

public class ReverseArray {
    public static void main(String[] args) {
/**
 * Create a program that will generate a random size array between 5 to 20;
 * Also, randomly populate each element of an array
 * Once done, - reverse your array
 */
        Random random = new Random();
        int randomArrayLength = random.nextInt(21); // 0-20
        if (randomArrayLength<5){
            randomArrayLength = 5;
        }
        System.out.println(randomArrayLength);

        double[] arr = new double[randomArrayLength];


        for (int i =0; i<randomArrayLength; i++){
            int populateArray = random.nextInt(1000);
            arr[i] = populateArray;
        }

        System.out.println(Arrays.toString(arr));

        double[] arrResult = new double[arr.length];

        for (int i = 0, j = arr.length-1; i<arr.length; i++, j--){
            arrResult[i] = arr[j];
        }

        System.out.println(Arrays.toString(arrResult));

    }
}

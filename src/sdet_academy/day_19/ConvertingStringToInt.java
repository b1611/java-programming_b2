package sdet_academy.day_19;

public class ConvertingStringToInt {
    public static void main(String[] args) {



        String str = "1";
        String str2 = "2";
        System.out.println(str+str2);

        int a = 1;
        int b = 2;
        System.out.println(a+b);

        String aStr = String.valueOf(a); // will convert our int into String
        String bStr = String.valueOf(b); // will convert our int into String
        System.out.println(aStr+bStr);

        int strInt = Integer.parseInt(str); // will convert our String into int
        int str2Int = Integer.parseInt(str2); // will convert our String into int
        System.out.println(str2Int+strInt);

        System.out.println(Double.parseDouble("2.76") + 25);

    }
}

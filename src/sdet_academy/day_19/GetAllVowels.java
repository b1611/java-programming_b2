package sdet_academy.day_19;


import java.util.Arrays;

public class GetAllVowels {
    public static void main(String[] args) {

/**
 * a, e, i, o, u, and sometimes y
 */

        char[] arrChar = {'a', 'y', 'w', 'u', 'e', 'z', 'h'};
        System.out.println(Arrays.toString(arrChar));
        String result = "";// ayue
        //[a,y,u,e]

        for (int i = 0; i < arrChar.length; i++) {
            if (arrChar[i] == 'a' || arrChar[i] == 'e' || arrChar[i] == 'i' || arrChar[i] == 'o' || arrChar[i] == 'u' || arrChar[i] == 'y') {
                result+=arrChar[i];
            }
        }

//        OPTION 1
//        char[] arrResult = new char[result.length()];
//
//        for(int i = 0; i<result.length(); i++){
//            arrResult[i] = result.charAt(i);
//        }
//
//        System.out.println(Arrays.toString(arrResult));

//      OPTION 2
        char[] arrResult = result.toCharArray();
        System.out.println(Arrays.toString(arrResult));
    }
}

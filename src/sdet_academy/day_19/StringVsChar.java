package sdet_academy.day_19;

import java.util.Arrays;

public class StringVsChar {
    public static void main(String[] args) {
        String[] strArr = {"a", "b", "c"};
        char[] charArr = {'a', 'b', 'c'};

        System.out.println(Arrays.toString(strArr));
        System.out.println(Arrays.toString(charArr));

        String str = "abc";

        char[] chars = str.toCharArray();
        System.out.println(Arrays.toString(chars));

        String[] split = str.split("");
        System.out.println(Arrays.toString(split));

        String sentence = "Java is fun";//[Java, is, fun]
        String[] sentenceArr = sentence.split(" ");
        System.out.println(Arrays.toString(sentenceArr));
        String word = "Hello*World*I*Am*Happy";
        word = word.replace("*", "_");

        String[] wordArr = word.split("_");
        System.out.println(Arrays.toString(wordArr));

    }
}

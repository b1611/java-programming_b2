package sdet_academy.day_19;

import java.util.Arrays;

public class ConvertIntArrayIntoString {
    public static void main(String[] args) {
        int[] numArr = {3,33,63,3,543,543,5,5,3};
        System.out.println(Arrays.toString(numArr));
        String str = Arrays.toString(numArr);
        System.out.println(str);
        str = str.replace("[", "").replace("]", "");
        System.out.println(str);
        String[] split = str.split(", ");
        System.out.println(Arrays.toString(split));

    }
}

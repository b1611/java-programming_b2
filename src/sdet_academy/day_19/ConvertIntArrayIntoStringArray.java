package sdet_academy.day_19;

import java.util.Arrays;

public class ConvertIntArrayIntoStringArray {
    public static void main(String[] args) {
        int[] numArr = {3,33,63,3,543,543,5,5,3};
        System.out.println(Arrays.toString(numArr));

        String result = "";

        for (int i = 0; i< numArr.length; i++){
            result+=numArr[i] + ", ";
        }

        String[] split = result.split(", ");
        System.out.println(Arrays.toString(split));

    }
}

package sdet_academy.day_2;

public class EscapeSequenceIntro {
    public static void main(String[] args){

        System.out.println("Java is fun"); // - everything in between the "" is considered a String data type
        System.out.println("My favorite language is \"JAVA\" and it's fun");
        System.out.println("\\");
        System.out.println("Java\tis\tfun");
        System.out.println(("Java   is  fun"));
        System.out.println("Java\nis\nfun");

        /**
         * print the statement that looks like the following:
         * -I buy my coffee in "Starbucks"
         * -I do not like to use backslash \
         * -Python
         * is
         * not
         * fun
         */

        System.out.println("I buy coffe in  \"Sturbucks\"");

        System.out.println("I buy my coffee in \"Starbucks\"") ;
        System.out.println("I do not like to use backslash \\");

        System.out.println("-I buy my coffee from \"Starbucks\"\n-I do not like to use backslash \\\n-Python\nis\nnot\nfun");

        System.out.println("I but my coffee in \"Starbucks\"");
        System.out.println("I do not like to use the backslash \\");
        System.out.println("Python\nis\nnot\nfun");

        System.out.println("\tpython\tis\tnot\tfun");

    }
}

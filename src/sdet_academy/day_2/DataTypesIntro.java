package sdet_academy.day_2;

public class DataTypesIntro {
    public static void main(String[] args){

        int myNumber = 125; //1. declaring and initializing variable right away
        int myNumberFive; // 2. declare the variable
        myNumberFive = 900; // initialize the variable


        System.out.println(myNumberFive);


        byte _mySecondNumber = 110;
        short $myThirdNumber = 11;
        short $myThirdNumber2 = 11;
        long myFourthVariable = 1000;
        float myVariableNumberEleven = 1111.545f; // we need to let java know to treat this as a float and not double
        //1000 = long myFavoriteVariable; - this will break the code


        System.out.println("Hello world"); // String data type

        System.out.println("11");  // String data type
        System.out.println($myThirdNumber); //int
        System.out.println(20.5); // double

        System.out.println(myNumber);
        System.out.println(myNumber-11);
        System.out.println(myNumber + 11);


        System.out.println(20.5 + 15.67);
        System.out.println(20.5 + 79.90);

        double a = 25.7;
        System.out.println(a);

        short num10 = 300;

    }
}

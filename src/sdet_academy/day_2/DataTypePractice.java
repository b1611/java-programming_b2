package sdet_academy.day_2;

public class DataTypePractice {
    public static void main(String[] args) {
        int numOne = 20;
        int numTwo = numOne;
        System.out.println(numTwo);

        numTwo = 30;
        System.out.println(numTwo%3);

        int numThree = numTwo%6;
        System.out.println(numThree);


        /**
         * double numOne = 10;
         *         System.out.println(numOne);
         *
         *         double numTwo = 20;
         *         System.out.println(numTwo/3);
         *
         *         float numFore = 30f;
         *         System.out.println(numFore%3);
         */
    }
}

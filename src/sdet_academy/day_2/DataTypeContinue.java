package sdet_academy.day_2;

public class DataTypeContinue {
    public static void main(String[] args){
        /**
         * Numerical: byte > short > int > long > float > double
         * char
         * boolean
         */

//        byte numOne;
//        numOne = 10;
            byte numOne = 10;
            short numTwo = 15;
            int numThree = 20;
            long numFour = 25;
            float numFive = 30.5F;
            double numSix = 35.9;
            char myCharacter = 'a';
            boolean myFlag = true;


            System.out.println(numOne+numTwo); // addition
            System.out.println(numThree-numOne); // deduction
            System.out.println(numFour/numFive); // division
            System.out.println(numSix*numFive); //multiplication


        int a = 100;
        System.out.println(a);
        int b = a; // remembers that a = 100;
        a = 200;
        System.out.println(a);

        System.out.println(b);


        int c = 15;
        int d = 6;
        System.out.println(c%d); //6+6 = 12. 12 < 15; 12+6 = 18; 18 > 15 - the remainder = 3;

        int e = 101;
        int h = 122;
        System.out.println(e%h); // will return the smaller number essentially.
    }
}

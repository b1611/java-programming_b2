package sdet_academy.day_2;

public class PrintTriangle {
    public static void main(String[] args){

        /**
         * Please use the * character to print an empty triangle
         */

        System.out.println("*");
        System.out.println("**");
        System.out.println("***");
        System.out.println("****");
        System.out.println("*****");

        System.out.println("   *");
        System.out.println("  ***");
        System.out.println(" *****");
        System.out.println("*******");
        System.out.println("********");

        System.out.println("    *    ");
        System.out.println("  *****  ");
        System.out.println(" ******* ");
        System.out.println("*********");

        System.out.println("   *   ");
        System.out.println("  ***  ");
        System.out.println(" ***** ");
        System.out.println("*******");

        System.out.println("    *    ");
        System.out.println("  *   *  ");
        System.out.println(" *     * ");
        System.out.println("*       *");

        System.out.println("    *");
        System.out.println("   * *");
        System.out.println("  *   *");
        System.out.println(" *     *");
        System.out.println("*********");

    }
}

package sdet_academy.day_6;

public class IntroToBooleanAndChar {
    public static void main(String[] args) {

        /**
         Task2
         * please use the if condition to determine the result of the exam:
         * if  the score is 90-100 - please print the message "A+"
         * if the score is 80-90 - please print "A"
         * if the score is 70-80 - please print "B"
         * if the score is 60-70 - please print "C"
         * if the score is <60 - print Failed the exam
         * If attendance is <80 then the student can not take the exam, otherwise allow the student to participate in the exam.
         */

        byte score = 96;
        short attendance = 85;
        boolean isAllowedToExam = true;
        char grade;


        if (attendance < 80 && attendance >= 0) {
            isAllowedToExam = false;
            System.out.println("You are not allowed to take the exam");
        } else if (attendance <= 0 || attendance > 100) {
            System.out.println("Invalid Attendance input");
        } else if (score < 60 && score >= 0) {
            System.out.println("You have failed the exam");
        } else if (score >= 60 && score < 70) {
            grade = 'C';
            System.out.println(grade);
        } else if (score >= 70 && score < 80) {
            grade = 'B';
            System.out.println(grade);
        } else if (score >= 80 && score < 90) {
            grade = 'A';
            System.out.println(grade);
        } else if (score >= 90 && score <= 100) {
            grade = 'A';
            System.out.println(grade + "+");
        } else {
            System.out.println("Invalid grade Input");
        }

    }
}

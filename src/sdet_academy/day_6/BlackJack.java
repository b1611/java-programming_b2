package sdet_academy.day_6;

import java.util.Random;

public class BlackJack {
    public static void main(String[] args) {
        /**
         * In blackjack after the player asks to keep the house 4 things may happen.
         *
         * 1. if the card total is bigger than 21 the player busts.
         *
         * 2. if the house score is bigger then the player, the player loses .
         *
         * 3. if the player score is equal to the house then they are a draw.
         *
         * 4. if the player score is bigger then the house the player wins.
         *
         * player and house scores are represented by  player and house int variables.
         *
         * check them using ifs to determine the result.
         * Please keep in mind boundary testing and possible invalid inputs
         *
         *
         */

        Random random = new Random();

        int house = random.nextInt(32); // 32 will never be generated, we will only get a number between 0 and 31
        int player = random.nextInt(32); // 32 will never be generated, we will only get a number between 0 and 31
        System.out.println(house);
        System.out.println(player);



        if (player > 21 && house > 21) {
            System.out.println("Loss");
        } else if (house > player && house <= 21 || player > 21) {
            System.out.println("House win");
        } else if (player > house && player <= 21 || house > 21) {
            System.out.println("Player win");
        } else if (player == house) {
            System.out.println("split");
        }
    }
}

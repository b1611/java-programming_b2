package sdet_academy.day_6;

public class AnsweringMachine {
    public static void main(String[] args) {

        String greeting = "Hey";

        if (greeting.equals("Hello") || greeting.equals("Hi")){
            System.out.println("Language is English");
        } else if (greeting.equals("Salut") || greeting.equals("Ce faci")){
            System.out.println("Language is Romanian");
        } else if (greeting.equals("Zdorov") || greeting.equals("Privit")){
            System.out.println("Language is Ukrainian");
        }else if (greeting.equals("Holla") || greeting.equals("Buenos Dias")){
            System.out.println("Language is Spanish");
        } else if (greeting.equals("Salom") || greeting.equals("Soz")){
            System.out.println("Language is Tajik");
        } else{
            System.out.println("No Comprente");
        }
    }
}

package sdet_academy.day_6;

public class CharExample {
    public static void main(String[] args) {

        char targetCharacter = 'k'; //107
        char anotherCharacter = 'K';

        System.out.println(targetCharacter!=anotherCharacter);

        if (targetCharacter >= 97 && targetCharacter <= 122) {
            System.out.println("This is a lower case letter");
        } else if (targetCharacter >= 65 && targetCharacter <= 90){
            System.out.println("This is an upper case letter");
        }else{
            System.out.println("This is not a letter");
        }

    }
}

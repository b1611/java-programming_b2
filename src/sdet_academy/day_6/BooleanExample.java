package sdet_academy.day_6;

public class BooleanExample {
    public static void main(String[] args) {

        boolean isRaining = false;

        if (isRaining == true) {
            System.out.println("It is raining outside");
        } else if (isRaining == false) {
            System.out.println("It is sunny outside");
        }

        if (isRaining == true) {
            System.out.println("It is raining outside");
        } else if (isRaining != true) {
            System.out.println("It is sunny outside");
        }

        if (isRaining) { // == true
            System.out.println("It is raining outside");
        } else if (!isRaining) { //==false
            System.out.println("It is sunny outside");
        }
    }
}

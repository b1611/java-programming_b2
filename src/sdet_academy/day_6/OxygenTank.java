package sdet_academy.day_6;

public class OxygenTank {
    public static void main(String[] args){

        /**
         * if you a diver and have an oxygen tank on you
         * if the oxygen level is >90 - print you are full
         * if the oxygen level is between 80 - 90 - you are ok
         * if the oxygen level is between 70 - 80 - don't go too far
         * if the oxygen level is between 60 - 70 - start to head back
         * if the oxygen level is between 50 - 60 - halfway there
         * less than 50 - print get out of the water!
         */

        byte oxygenLevel = 80;
        if (oxygenLevel > 90) {
            System.out.println("You are full");
        } else if (oxygenLevel >= 80 && oxygenLevel <= 90) {
            System.out.println("you are ok");

        } else if (oxygenLevel >= 70 && oxygenLevel <80) {
            System.out.println("don't go too far");

        } else if (oxygenLevel >= 60 && oxygenLevel < 70) {
            System.out.println("start to head back");

        } else if (oxygenLevel >= 50 && oxygenLevel < 60) {
            System.out.println("halfway there");

        } else if (oxygenLevel < 50) {
            System.out.println("get out of the water");
        }


    }
}

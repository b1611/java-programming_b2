package sdet_academy.day_8;

public class TernaryIntro {
    public static void main(String[] args) {

        int num = 4;
        String result = "";

//        if (num>10){
//            result = "Good Evening";
//        }else{
//            result = "Good Day";
//        }

        result = (num > 10) ? "Good Evening" : "Good Day";

        System.out.println(result);

    }
}

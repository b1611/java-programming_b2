package sdet_academy.day_8;

import java.util.Scanner;

public class Kahoot {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int priceOne;

        System.out.println("Please enter the CPU");
        String itemOne = in.next(); // i3

        if(itemOne.equals("i3")){
            priceOne = 150;
        }else if(itemOne.equals("i5")){
            priceOne = 200;
        }else{
            priceOne = 250;
        }

        System.out.println(priceOne);

    }
}

package sdet_academy.day_8;

public class SwitchWithString {
    public static void main(String[] args) {


        String seasonName = "Autumn";

        switch (seasonName){
            case "Winter": // if(seasonName.equals("Winter")
                System.out.println("It is freezing cold");
                break;
            case "Spring":
                System.out.println("It's getting warmer");
                break;
            case "Summer":
                System.out.println("It is nice and hot");
                break;
            case "Fall":
                System.out.println("It is raining all the time");
                break;
            default: //else block
                System.out.println("Speak Americish");
        }

        System.out.println("This statement is after the switch");

    }
}

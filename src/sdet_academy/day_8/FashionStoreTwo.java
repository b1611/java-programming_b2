package sdet_academy.day_8;

public class FashionStoreTwo {
    public static void main(String[] args) {
        /**
         * If the buyer has a store membership they will receive $100 off.
         * Calculate the price of the item based on the brand and item name
         * if the brand is Gucci
         *      the price for bag is 3000
         *      the price for belt is 500
         *      the price for shoes is 1500
         * if the brand is Channel
         *      the price for bag is 8000
         *      the price for belt is 1000
         *      the channel shoes are out of stock
         * if the brand is Versace
         *      the price for bag is 5000
         *      the price for belt is 800
         *      the price for shoes is 1300
         */
        String brand = "Channel";
        String item = "shoes";
        boolean hasMembership = true;
        double price = 0;
        double total = 0;
        if (brand.equals("Gucci")) {
            if (item.equals("bag")) {
                price = 3000;
                System.out.println("Price for Gucci bag is $ " + price);
            } else if (item.equals("belt")) {
                price = 500;
                System.out.println("Price for Gucci belt is $" + price);
            } else if (item.equals("shoes")) {
                price = 1500;
                System.out.println("Price for Gucci shoes is $" + price);
            } else {
                System.out.println("Sorry we don't have this item from Gucci");
            }
        } else if (brand.equals("Channel")) {
            if (item.equals("bag")) {
                price = 8000;
                System.out.println("Price for Channel bag is $ " + price);
            } else if (item.equals("belt")) {
                price = 1000;
                System.out.println("Price for Channel belt is $" + price);
            } else if (item.equals("shoes")) {
                System.out.println("The channel shoes are out of stock");
            } else {
                System.out.println("Sorry we don't have this item from Channel");
            }
        } else if (brand.equals("Versace")) {
            if (item.equals("bag")) {
                price = 5000;
                System.out.println("Price for Versace bag is $ " + price);
            } else if (item.equals("belt")) {
                price = 800;
                System.out.println("Price for Versace belt is $" + price);
            } else if (item.equals("shoes")) {
                price = 1300;
                System.out.println("Price for Versace shoes is $" + price);
            } else {
                System.out.println("Sorry we don't have this item from Versace");
            }
        } else {
            System.out.println("Sorry we don't have this brand");
        }
        if (hasMembership) {
            total = price - 100;
        } else {
            total = price;
        }
        System.out.println("Total is $" + total);
    }

}


package sdet_academy.day_8;

import java.util.Random;

public class RandomPickerSwitch {
    public static void main(String[] args) {
        Random random = new Random();
        int num = random.nextInt(7);

        switch (num) {
            case 0:
                System.out.println("Radu");
                break;
            case 1:
                System.out.println("James");
                break;
            case 2:
                System.out.println("Slava");
                break;
            case 3:
                System.out.println("Liuba");
                break;
            case 4:
                System.out.println("Irina");
                break;
            case 5:
                System.out.println("Angie");
                break;
            case 6:
                System.out.println("Myroslava");
                break;
            default:
                System.out.println("This Name does not exist");
        }

        /**
         * 1. Angie
         * 2. Radu
         * 3. Iryna
         * 4. Myroslava
         * 5. Luba
         * 6. Slava
         *
         */


    }
}

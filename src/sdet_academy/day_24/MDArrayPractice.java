package sdet_academy.day_24;

public class MDArrayPractice {
    public static void main(String[] args) {

        /**
         * Create an MD array that will have the following strucure:
         * - 1 row for your favorite veggies with multiple choices
         * - 2 row for your favorite drink with multiple choices
         * - 3 row for your favorite fruit with multiple choices
         */



        String[][] strArr = {
                {"potato",  "onion"},  //0
                {"water", "tea", "coffee", "red bull"}, //1
                {"banana", "strawberry", "peach", "mango", "carrot", "beet",} //2
                //     0           1           2        3
        };

        System.out.println("My favorite fruit is " + strArr[2][1]);
        System.out.println("My favorite veggie is " + strArr[0][0]);
        System.out.println("My favorite drink is " + strArr[1][1]);


        for (String[] eachRow : strArr) {
            for (String eachElement : eachRow) {
                System.out.println(eachElement);
            }
        }

        for (int i = 0; i < strArr.length; i++) {
            for (int j = 0; j < strArr[i].length; j++) {
                System.out.println(strArr[i][j]);
            }
        }

    }
}

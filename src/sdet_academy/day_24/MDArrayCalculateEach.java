package sdet_academy.day_24;

import java.util.Arrays;

public class MDArrayCalculateEach {
    public static void main(String[] args) {

        int[][] numArr = {
                {1,5,3,6}, // 15
                {0,3,63,63,3}, // 132
                {15,5,2},// 22
                {9,9,9,9}//36
        };

       //[15, 132, 22, 36]
        int sum = 0;
        int[] result = new int[numArr.length];
        int index = 0;




        //OPTION 1
        for (int[] eachRow : numArr) {
            for (int eachElement : eachRow) {
                sum+=eachElement;
            }
            result[index] = sum;
            sum = 0;
            index++;
        }

        //OPTION 2
//        for (int i = 0; i < numArr.length; i++) {
//            for (int j = 0; j < numArr[i].length; j++) {
//                sum+=numArr[i][j];
//            }
//            result[i] = sum;
//            sum=0;
//        }
//
        System.out.println(Arrays.toString(result));


    }
}

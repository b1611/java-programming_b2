package sdet_academy.day_24;

import java.util.Arrays;

public class MultidimensionalArrayIntro {
    public static void main(String[] args) {

        String[][] strArr = new String[2][3];// the length counts starting with 1

        strArr[0][0] = "Luba";
//             i  j
        strArr[0][1] = "Radu";
        strArr[0][2] = "Iryna";
        strArr[1][0] = "Slava";
        strArr[1][1] = "Angie";
        strArr[1][2] = "Myroslava";

        System.out.println(Arrays.deepToString(strArr));

        int[][] numArr = {
                {1,23,4,5},
                {5,8},
                {100,200,300}

        };

        System.out.println(Arrays.toString(numArr));

    }
}

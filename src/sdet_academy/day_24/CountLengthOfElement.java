package sdet_academy.day_24;

import java.util.Arrays;

public class CountLengthOfElement {
    public static void main(String[] args) {

        //count how many elements in Array //[4, 5, 3, 4]

        int[][] numArr = {
                {1,5,3,6}, // 4
                {0,3,63,63,3}, // 5
                {15,5,2},// 3
                {9,9,9,9}//4
        };
        int [] result = new int[numArr.length];
        int index=0;

        for (int[] each : numArr) {
            result[index]=each.length;
            index++;
        }
        System.out.println(Arrays.toString(result));


//        for (int i = 0; i <numArr.length ; i++) {
//            result[i]=numArr[i].length;
//
//        }

//        int count =0;
//
//        for (int i = 0; i <numArr.length ; i++) {
//            for (int j = 0; j <numArr[i].length ; j++) {
//                count++;
//            }
//            result[i]=count;
//            count=0;
//        }


    }
}

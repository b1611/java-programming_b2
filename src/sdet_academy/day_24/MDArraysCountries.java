package sdet_academy.day_24;

import java.util.Arrays;

public class MDArraysCountries {
    public static void main(String[] args) {

        /**
         * create an MD array with the following rows: country, state, city,
         * each of which will have multiple elements to choose from
         */

        String even = "";
        String odd = "";

        String[][] strArr = {
                {"Ukraine", "USA", "Canada", "Mexico", "Costa Rica"},
                {"Alberta", "Toronto", "Maryland", "Ontario"},
                {"Chicago", "Baltimore", "Toronto", "Kiev"}
        };

        System.out.println("I live in a country " + strArr[0][1]);
        System.out.println("I live in a state " + strArr[1][2]);
        System.out.println("I live in a city " + strArr[2][1]);

        for (String[] eachRow : strArr) {
            for (String eachElement : eachRow) {
                if (eachElement.length() % 2 == 0) {
                    even += eachElement+", ";
                } else {
                    odd += eachElement+", ";
                }
            }
        }
        System.out.println(even);
        System.out.println(odd);

        String[] split1 = even.split(", ");
        String[] split2 = odd.split(", ");

        System.out.println(Arrays.toString(split1));
        System.out.println(Arrays.toString(split2));
    }
}

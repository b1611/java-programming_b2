package sdet_academy.day_24;

import java.util.Arrays;

public class PrintWithoutLastCharacter {
    public static void main(String[] args) {

        String[][] strArr = {
                {"Ukraine", "USA", "Canada", "Mexico", "Costa Rica"},
                {"Alberta", "Toronto", "Maryland", "Ontario"},
                {"Chicago", "Baltimore", "Toronto", "Kiev"}
        };

        for (int i = 0; i < strArr.length; i++) {
            for (int j = 0; j < strArr[i].length; j++) {
                String element = strArr[i][j]; //Ukraine
                strArr[i][j] = element.substring(0, element.length()-1).toUpperCase();
            }
        }
        System.out.println(Arrays.deepToString(strArr));

    }
}

package sdet_academy.day_15;

public class KeepUniqueCharacters {
    public static void main(String[] args) {
        /**
         * Create a program that will store only unique characters
         *          String str = "JavaScript is not Java";
         *         String result = "JavScript sno";
         */
        String str = "JavaScript is not Jjava";
        String result = "";

        for (int i = 0, j = str.length()-1; i<str.length() || j>=0; i++, j--){
            String character ="" + str.charAt(i);
            character = character.toLowerCase();
            if(!result.contains(character)){
                result+=character;
            }
        }
        System.out.println(result);



    }
}

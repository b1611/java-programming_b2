package sdet_academy.day_15;

import java.util.Random;
import java.util.Scanner;

public class RandomPasswordGenerator {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrtuvwxyz0123456789_!@#$%^&*\";";

        String password = "";
        int numberOfCharacters;

        do{
            System.out.println("Please enter the length of the password");
            numberOfCharacters = scanner.nextInt();
        }while (numberOfCharacters < 5 || numberOfCharacters > 100);


        int lengthOfThePassword = numberOfCharacters; //5-25
//        int lengthOfThePassword = 5 + random.nextInt(20); //5-25

        for (int i = 0; i < lengthOfThePassword; i++) {
            int randomPicker = random.nextInt(str.length()); //72
            password += str.charAt(randomPicker);
        }
        System.out.println(password);
    }
}

package sdet_academy.day_15;

public class LowerOrUpperCase {
    public static void main(String[] args) {

        char character = 'z';
        String result = "";


        for(int i= 'a', j = 'A'; j<='Z'; i++, j++){
            if(character==i){
                result = "Lower Case";
                break;
            }else if(character==j){
                result = "Upper case";
                break;
            }else{
                result = "character is not letter";
            }
        }

        System.out.println(result);


        for (int i = 'a'; i<='z'; i++){  //a - 97, z - 122
            if(character==i){
                result = "character is lower case";
                break;
            }else{
                result = "character is not lower case";
            }

        }

        System.out.println(result);

        for (int i = 'A'; i<='Z'; i++){  //a - 97, z - 122
            if(character==i){
                result = "character is Upper case";
                break;
            }else{
                result = "character is not Upper case";
            }

        }

        System.out.println(result);

    }
}

package sdet_academy.day_15;

public class ContinueIntro {
    public static void main(String[] args) {

        for (int i = 0; i <20; i++){

            if(i>=7 && i<=17){
                continue; // java will ignore this itteration and carry on
            }
            System.out.println(i);

        }

        for (int i = 0; i<10; i++){
            if(i%2!=1){
                continue;
            }

            System.out.println(i);
        }

        for (int i = 0; i<25; i++){
            if(i%4!=0 || i==0){
                continue;
            }
            System.out.println(i);
        }

        for (int i = 1; i<25; i++){
            if(i%4!=0){
                continue;
            }
            System.out.println(i);
        }


    }
}

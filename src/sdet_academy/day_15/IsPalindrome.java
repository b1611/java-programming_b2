package sdet_academy.day_15;

public class IsPalindrome {
    public static void main(String[] args) {
        /**
         * Please create a program that will verify if the String str is Palindrome or not
         * (Examples: Civ ic, kayak, level, radar, mom, dad, maam, noon, racecar, deified, repaper).
         */

        String str = "Civ i c";
        str = str.toLowerCase().replace(" ", "");
        str = str.trim();
        boolean isPalindrome = true;

        for (int i = 0, j = str.length()-1; i<str.length()/2; i++, j--){
            if(str.charAt(i)!=str.charAt(j)){
                isPalindrome = false;
            }
        }

        System.out.println(isPalindrome);

        for (int i = 0; i<str.length()/2; i++){
            if(str.charAt(i)==str.charAt(str.length()-1-i)){
                isPalindrome =true;
            }else{
                isPalindrome = false;
                break;
            }
        }

        System.out.println(isPalindrome);


        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                isPalindrome = false;
                // 0 > 1 > 2 > 3...                4 > 3 >2 >1
            }
        }

        System.out.println(isPalindrome);

        for (int i = 0; i<str.length()/2; i++){
            if(str.charAt(i) != str.charAt(str.length()-1 -i)){
                isPalindrome = false;
                break;
                // 0 > 1 > 2 > 3...                4 > 3 >2 >1
            }else{
                isPalindrome = true;
            }
        }
        System.out.println(isPalindrome);

    }
}

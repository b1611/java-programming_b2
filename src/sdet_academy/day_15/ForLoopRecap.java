package sdet_academy.day_15;

public class ForLoopRecap {
    public static void main(String[] args) {

        int num = 10;

        for (int myIterator = 0; myIterator<num; myIterator++){
            System.out.println("myIterator = " + myIterator);
        }

        for (int startingFromTheEnd = 100; startingFromTheEnd>=num; startingFromTheEnd-=10){
            System.out.println("startingFromTheEnd = " + startingFromTheEnd);
        }

        String str = "JavaScript is not Java";
        String result = "JavScript sno";


        for (int i = 0; i<str.length(); i++){

        }

        /**
         * JavaScript is not Java = J0a0v0a0S0c0r0i0pt0 0is0 0not0 0J0a0v00a
         * Java = avaJ
         * JavaScript is not Java = JavaScript is not
         */








    }
}

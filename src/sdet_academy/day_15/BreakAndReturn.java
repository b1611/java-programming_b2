package sdet_academy.day_15;

public class BreakAndReturn {
    public static void main(String[] args) {

        for (int i = 20; i<40; i++){
            if(i>=7 && i<=17){
                return; // - return will exit a loop as well as the method where it operates
            }
            System.out.print(i);
        }

        System.out.println();

        for (int i = 0; i<40; i++){
            if(i>=7){
                break; // - will only exit the loop
            }
            System.out.print(i);
        }

//        for(int i = 0; i<30; i++){
//            if(i>20 && i%2==1){
//                break;
//            }else if(i%2==0){
//                continue;
//            }
//            System.out.println(i); // 1,3,5,7,9,...17,19. 21 23 25
//        }

    }
}

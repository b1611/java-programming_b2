package sdet_academy.day_15;

public class PrintTheRangeOfLetters {
    public static void main(String[] args) {

//        String alphabet = "abcdefghijklmnopqrstuvwxyz";

        char beginningLetter = 'a';  //97
        char endingLetter = 'z'; // 122

        for (int i = beginningLetter; i<=endingLetter; i++){
            System.out.print((char)i);
        }

    }
}

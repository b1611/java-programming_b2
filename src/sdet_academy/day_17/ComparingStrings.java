package sdet_academy.day_17;

public class ComparingStrings {
    public static void main(String[] args) {

        String str = "Java"; //literal declaration of String will create only 1 object inside of our heap memery
        //in our String pool and as long as those match, we will be pointing to the same object
        String str2 = "Java";
        System.out.println(str.equals(str2));
        System.out.println(str == str2);

        String str3 = new String("Java"); // using the new keyword will force java to create a separate object for
        // each String variable outside of the String pool, which means the variables will be pointing to a different objects
        String str4 = new String("Java");
        System.out.println(str3.equals(str4));
        System.out.println(str3==str4);

        String a = "a";
        String b = "a";
        System.out.println(a.equals(b));
        System.out.println(a==b);




        /**
         * There are 2 types of memory in java: stack and heap
         * In our heap memory there is a section called String pool.
         */


    }
}

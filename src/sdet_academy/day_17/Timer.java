package sdet_academy.day_17;

public class Timer {
    public static void main(String[] args) throws InterruptedException {
        /**
         * 0:1
         * 0:2
         * .....
         * 0:59
         * 1:1
         * 1:2...
         * 3:59
         */

        for (int min = 4; min>=0; min--){
            for (int sec = 59; sec>=0; sec--){
                System.out.println(min + ": " + sec);
                Thread.sleep(1000);
            }
            System.out.println();
        }
    }
}

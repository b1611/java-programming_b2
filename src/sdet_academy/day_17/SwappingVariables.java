package sdet_academy.day_17;

public class SwappingVariables {
    public static void main(String[] args) {

        int a = 10;
        int b = 15;
        //a = 15;
        //b = 10;
        int temp;

        temp = a;
        a = b;
        b = temp;

        System.out.println(a);
        System.out.println(b);

        String str = "Hello";
        String strTwo = "World";
        String tempTwo;

        tempTwo = str;
        str = strTwo;
        strTwo = tempTwo;
        System.out.println(str);
        System.out.println(strTwo);


        int x = 20;
        int y = 5;


        x = x + y;
        y = x - y;
        x = x - y;

        System.out.println(x);
        System.out.println(y);

    }
}

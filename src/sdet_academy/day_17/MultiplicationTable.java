package sdet_academy.day_17;

public class MultiplicationTable {
    public static void main(String[] args) {

        /**
         * 1*1 = 1
         * 1 * 2 = 2
         * 1 * 3 = 3
         * ......
         * 10 * 10 = 90
         */

        for (int i = 1; i <= 10; i++){
            for (int j = 1; j <=10; j++){
                System.out.println(i + "*" + j + " = " + i*j);
            }
            System.out.println("======================================");
        }


    }
}

package sdet_academy.day_17;

public class NestedLoopIntro {
    public static void main(String[] args) {

        String str = "Hello";
        for (int i = 0; i<str.length(); i++){
            System.out.println(str.charAt(i) + " ");
            for (int j = 0; j<str.length(); j++){
                System.out.print(str.charAt(j));
            }
        }

    }
}

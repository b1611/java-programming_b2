package sdet_academy.day_17;

public class CountLetters {
    public static void main(String[] args) {
        /**
         * Create a program that will have String str = "java"
         * The program should iterate through each charter and should print how many times this character is being duplicated
         * Example:
         j has no duplicates - 1
         a has duplicates - 2
         v has no duplicates - 1
         a has duplicates - 2
         */

        String str = "java";
        //            0123
        //          "15498" > "14589"

        int count = 0;
        /**
         *  i= 0; str.charAt(i) - j
            j = 0; str.charAt(j) - j count ++;
         j = 1; str.charAt(j) - a
         j = 2; str.charAt(j) - v
         j = 3; str.charAt(j) - a
         i= 1; str.charAt(i) - a
         j = 0; str.charAt(j) - j
         */

        for (int i = 0; i<str.length(); i++){
            for (int j = 0; j<str.length(); j++){
//                System.out.println(str.charAt(i));
//                System.out.println(str.charAt(j));
                if(str.charAt(i)==str.charAt(j)){
                    count++;
                }
            }
//            if(count>1){
//                System.out.println(str.charAt(i) + " has duplicates - " + count);
//            }else {
//                System.out.println(str.charAt(i) + " has no duplicates  - " + count);
//            }
            if(count==1){
                System.out.println(str.charAt(i) + " has no duplicates  - " + count);
            }else{
                System.out.println(str.charAt(i) + " has duplicates - " + count);
            }
            count=0;
        }

    }
}

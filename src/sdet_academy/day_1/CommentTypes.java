package sdet_academy.day_1;

public class CommentTypes {
    public static void main(String[] args){


       //1.  System.out.println("Java is fun");

        /*
        2.
        This is a comment
        it will not be executed
        This us just for practice
         */

        /**
         * 3.
         * This is a comment
         * it will not be executed
         * this is just for practice
         */

        //4. TODO Need to finish this code asap
        System.out.println("Hello World");

        //5. todo need to finish this code asap



    }
}

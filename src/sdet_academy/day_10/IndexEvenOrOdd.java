package sdet_academy.day_10;

public class IndexEvenOrOdd {
    public static void main(String[] args) {
        String str="Java is fun";
        char targetChar='z';
        boolean isIndexEven;
        if(str.isEmpty()||str.indexOf(targetChar)==-1){
            System.out.println("error");
        }else if(str.indexOf(targetChar)%2==0){
            isIndexEven=true;
            System.out.println("isIndexEven = " + isIndexEven);
        }else {
            isIndexEven = false;

            System.out.println("isIndexEven = " + isIndexEven);
        }
        System.out.println(str.indexOf(targetChar));
    }
}

package sdet_academy.day_10;

public class UniqueString {
    public static void main(String[] args) {
        String str = "Java is not the same as JavaScript. Python is not the same as JavaScript";
        String targetStr = "Python";
        boolean isUnique;


        System.out.println(str.indexOf(targetStr));
        System.out.println(str.lastIndexOf(targetStr));
        if (str.indexOf(targetStr) == -1 || str.lastIndexOf(targetStr) == -1) {
            System.out.println("Target word is not present");

        } else if (str.indexOf(targetStr) == str.lastIndexOf(targetStr)) {
            isUnique = true;
            System.out.println("isUnique = " + isUnique);

        }else {

            isUnique = false;
            System.out.println("isUnique = " + isUnique);
        }
    }
}

package sdet_academy.day_10;

public class CharToString {
    public static void main(String[] args) {

        String str = "Hello World";

        char character = str.charAt(str.length() - 1); //d

        System.out.println(character);


        String myChar = "" + character;

        System.out.println(myChar.toUpperCase());

    }
}

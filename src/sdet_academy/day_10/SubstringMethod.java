package sdet_academy.day_10;

public class SubstringMethod {
    public static void main(String[] args) {

        String str = "Today_is_Saturday";
        //            0123456789
        String strTwo = str.substring(5);// if we only provide 1 argument, we will get the substring that will include
                                                    // the character the index above
        System.out.println(strTwo);

        String strThree = str.substring(5, 8); // this will only return substring with the indexes 5-7. Character at index 8 will be
                                                //exluded
        System.out.println(strThree);

    }
}

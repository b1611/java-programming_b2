package sdet_academy.day_10;

public class RepeatMethod {
    public static void main(String[] args) {

        String str = "Hello";
        System.out.println(str + str + str + str + str);
        System.out.println(str.repeat(20));
        System.out.println(str);

    }
}

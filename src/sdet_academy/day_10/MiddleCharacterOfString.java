package sdet_academy.day_10;

public class MiddleCharacterOfString {
    public static void main(String[] args) {

        /**
         * Given a String str, please check if the index of the middle character is even.
         * If the length of the sentence is even, please print 2 middle characters.
         * please make sure you check if the str is empty or not
         */

        String str = "Hello";// length = 6
        //            012345
        String result = "";

        System.out.println(str.length());
        if(str.isEmpty()){
            result = "";
        }else if(str.length()%2==0){
            result = "" + str.charAt(str.length() / 2 - 1) + str.charAt(str.length() / 2);
        }else{
            result = "" + str.charAt(str.length()/2);
        }

        System.out.println(result);

        System.out.println(str.repeat(4));
    }
}

package sdet_academy.day_10;

public class MiddleCharacter {
    public static void main(String[] args) {
        /**
         * Write a program that will print out the middle character if length of the word is odd,
         * but if the length of the word is even - print "There is no middle character in this word"
         * - make sure your code is dynamic and works with different words(also please check if the word is not empty)
         */

        String word = "  ";

        if (word.isEmpty()||word.isBlank()) {
            System.out.println("Is empty");
        }else if (word.length() % 2 == 1) {
            System.out.println(word.charAt(word.length() / 2));
        }else {
            System.out.println("There is no middle character");
        }
    }
}

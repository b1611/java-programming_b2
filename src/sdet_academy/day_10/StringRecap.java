package sdet_academy.day_10;

public class StringRecap {
    public static void main(String[] args) {

        String str = "Java";
        //            0123
        String strTwo = "My favorite language is java";

        System.out.println(strTwo.toUpperCase().contains(str.toUpperCase()));

        str = str.toUpperCase(); // JAVA
        System.out.println(str);

        System.out.println(str.length());

        System.out.println(str.charAt(2)); // returns char accepts int as an index
        System.out.println(str.indexOf('V')); // returns you an index accepts char or String as an argument

        String strThree = "Python is good but Java 19 is better";
        System.out.println(strThree.indexOf("o"));
        System.out.println(strThree.lastIndexOf("z"));



    }
}

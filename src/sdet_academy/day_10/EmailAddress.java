package sdet_academy.day_10;

public class EmailAddress {
    public static void main(String[] args) {

        String email = "ChuckNorris@gmail.com"; // length - 21
        //              MikeTyson@gmail.com

        /**
         * psedu code:
         * 1. Substring
         * 2.
         */
//1
        System.out.println(email.length());
        String result = email.substring(email.length()-9);
        System.out.println(result);

        //2
        int indexOfAt = email.indexOf("@");
        String resultTwo = email.substring(indexOfAt+1);
//        String resultTwo = email.substring(indexOfAt+1, email.length()); - the second argument is redundant
        System.out.println(resultTwo);



    }
}

package sdet_academy.day_10;

public class TrimMethod {
    public static void main(String[] args) {

        String str = "    Hello World      ";
        System.out.println(str);
        str = str.trim();
        System.out.println(str);

    }
}

package sdet_academy.day_10;

public class LastCharacterIsUpperCase {
    public static void main(String[] args) {

        String name = "Java is fuN"; // length = 0
        String lastChar = ""; // looking for index -1

        if (name.isEmpty()){
            System.out.println("error");
        } else{
            lastChar = "" +name.charAt(name.length()-1); // looking for index -1
            String s = lastChar.toUpperCase();
            if (lastChar.equals(s)){
                System.out.println("It is upper case");
            } else {
                System.out.println("It is not upper case");
            }
        }
        System.out.println(lastChar);

    }
}

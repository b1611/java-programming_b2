package sdet_academy.day_10;

public class UniqueOrNot {
    public static void main(String[] args) {
        String str = "Today is Saturday";
        String targetCharacter = "t";
        boolean isUnique = false;

        if (str.indexOf(targetCharacter) == -1 && str.lastIndexOf(targetCharacter) == -1) {
            System.out.println("Not identified");
        } else if (str.indexOf(targetCharacter) == str.lastIndexOf(targetCharacter)) {
            isUnique=true;
        } else if (str.indexOf(targetCharacter) != str.lastIndexOf(targetCharacter)) {
            isUnique = false;
        }

        System.out.println(str.indexOf(targetCharacter));
        System.out.println(str.lastIndexOf(targetCharacter));
        System.out.println(isUnique);
    }
}

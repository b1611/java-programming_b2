package sdet_academy.day_10;

public class ReplaceCharacters {
    public static void main(String[] args) {
        String str = "Java, is fun, especially, on, Saturdays";

        System.out.println(str.replace("a", "z"));
        System.out.println(str.replace(',', '*'));
//        str = str.replace("a", "z");
        System.out.println(str);
        System.out.println(str.replace(",", ""));

        String strTwo = "Hello World";
        strTwo = strTwo.replaceFirst("l", "_");
        System.out.println(strTwo);



    }
}

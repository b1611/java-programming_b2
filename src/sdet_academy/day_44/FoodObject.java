package sdet_academy.day_44;

public class FoodObject {
    public static void main(String[] args) {
        Food food = new Food("Something tasty");
        Fruit fruit = new Fruit("tasty fruit");
        Strawberry strawberry = new Strawberry();

        food.eat();
        fruit.eatFood();
        strawberry.eatFood();

        strawberry.cook();
        strawberry.eat();
        System.out.println(strawberry.weight);
        System.out.println(strawberry.getWeight());
    }
}

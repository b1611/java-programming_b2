package sdet_academy.day_44;

public class Strawberry extends Fruit{
    public boolean isRipe;
    public boolean isRed;

    public Strawberry(){
        System.out.println("Strawberry constructor");
    }

    public void makeCompot(){
        System.out.println("Boiling the strawberry to make compot");
    }

    public boolean getIsRipe(){
        return isRipe;
    }

}

package sdet_academy.day_44;

public class Food {
    public String name;
    public boolean isEdible;
    public double weight = 15.00;

    public Food(){
        System.out.println("Food Constructor");
    }

    public Food(String name){
        System.out.println("The name of the food is " + name);
    }

    public void cook(){
        System.out.println("cooking food");
    }

    public void eat(){
        System.out.println("Eating food");
    }

    public void feed(){
        System.out.println("Feeding food");
    }
}

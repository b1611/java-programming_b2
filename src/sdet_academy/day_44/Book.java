package sdet_academy.day_44;

public class Book extends Library{
    public String bookName;
    public String bookAuthor;
    private int bookYear;

    public Book(String bookName){
        this.bookName = bookName;
    }

    public Book(int bookYear, String bookAuthor){
        this("Lord of the Rings");
        this.bookYear=bookYear;
        this.bookAuthor = bookAuthor;
    }

    public Book(){
        System.out.println("No arg Book constructor");
    }

    public Book(String bookName, String bookAuthor, int bookYear){
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookYear = bookYear;
    }


    public Book(String bookName, String bookAuthor, int bookYear, String libraryName, String libraryLocation){
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookYear = bookYear;
        super.libraryName = libraryName;
        super.libraryLocation = libraryLocation;
    }

    public Book(String libraryName, String libraryLocation){
        super(libraryName, libraryLocation);
    }

    public void readingBook(){
        System.out.println("Reading the " + bookName + ", written by " + bookAuthor);
    }

    public void wasPublished(){
        System.out.println("This book was published in " + bookYear);
    }

    public void setBookYear(int bookYear) {
        this.bookYear = bookYear;
    }

    public String getBookName(){
        return bookName;
    }
}

package sdet_academy.day_44;

public class Animal {
    public final String species = "Bird";
    public boolean isFlying;

    protected void fly(){
        System.out.println("Our " + species + " is flying: " + isFlying);
    }

    public static void sleep(){
        System.out.println("Our animal is sleeping");
    }
}

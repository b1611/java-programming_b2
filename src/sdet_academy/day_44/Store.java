package sdet_academy.day_44;

public class Store {
    public String storeName;
    protected String storeLocation;
    int storeNumber; // default access modifier
    private String managerForTheStore;

    protected void sellSomething(){
        System.out.println("Selling Something");
    }

    void closeStore(){
        System.out.println("Closing our store for good");
    }

    public String getManagerForTheStore(){
        return managerForTheStore;
    }

    public int getStoreNumber() {
        return storeNumber;
    }
}

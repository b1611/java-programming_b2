package sdet_academy.day_44;

public final class Eagle extends Animal {
    public boolean isWhiteEagle;


    public void hunt() {
        System.out.println("Eagle is hunting");
    }

    public void dive() {
        System.out.println("Eagle is diving");
    }

    @Override
    public void fly() {
        System.out.println("THe eagle is sleeping well");
    }
}

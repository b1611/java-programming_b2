package sdet_academy.day_44;

public class BookObject {
    public static void main(String[] args) {

        Book book = new Book("Harry Potter", "Joan King Rawling", 2007, "Washington", "Philadelphia");
//        book.bookName = "Harry Potter";
//        book.bookAuthor = "Joan King Rawling";
//        book.libraryLocation = "Trenton";
//        book.libraryName = "Washington Library";
        book.readingBook();
        book.libraryIsOpen();
        book.libraryIsClosed();

        Book book2 = new Book("Lincoln Library", "Baltimore, MD");
        book2.libraryIsOpen();
        book2.libraryIsClosed();
        book2.setBookYear(2014);
        book2.wasPublished();

        Book book3 = new Book(2020, "Tomplinson");
        book3.readingBook();

    }
}

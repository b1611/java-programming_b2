package sdet_academy.day_44;

public class Library {
    public String libraryName;
    public String libraryLocation;

    public Library(){
        System.out.println("No arg library constructor");
    }

    public Library(String libraryName, String libraryLocation){
        this.libraryName = libraryName;
        this.libraryLocation = libraryLocation;
    }

    public void libraryIsOpen(){
        System.out.println("Library " + libraryName + " is open 8 am - 5 pm every day");
    }

    public void libraryIsClosed(){
        System.out.println("Library, located at " + libraryLocation + " is closed after 5 pm, no exceptions!");
    }
}

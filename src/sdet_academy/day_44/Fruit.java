package sdet_academy.day_44;

public class Fruit extends Food{
    public boolean isSweet;
    public boolean isSour;
    public String fruitName;
    public double weight = 10.0;

    public Fruit(){
        System.out.println("Fruit constructor");
    }

    public Fruit(String fruitName){
        super(fruitName);
        System.out.println("Anything in here");
    }

    public void makeCompot(){
        System.out.println("Boiling the fruits to make compot");
    }

    public void eat(){
        System.out.println("Eating the fruit");
    }

    public void eatFood(){
        super.eat();
    }

    public double getWeight(){
        return super.weight;
    }

    /**
     * this. - refers to the method or variable of the current class
     * this() - refers to the constructor of the current class
     * super. - refers to the method or variable of the parent class
     * super() - refers to the constructor of the parent class
     */
}

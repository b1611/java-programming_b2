package sdet_academy.day_39;

import static sdet_academy.day_39.City.cityIsClean;
import static sdet_academy.day_39.City.cityIsWarm;
import static sdet_academy.day_39.City.isWarm;

public class CityObject {
    public static void main(String[] args) {
        City city = new City();

        System.out.println(city.getCitySize());
        city.population = 500_000;
        System.out.println(city.getCitySize());
        city.name = "Toronto";
        city.age = 2023-1780;
        city.coordinates.put("000.543", "542.424");
        city.visit();
        city.live();

        City city2 = new City(1_000_000);
        System.out.println(city2.getCitySize());

        City city3 = new City(true, "Washington DC");
        city3.live();

        City city4 = new City("Tokio", true);
        city4.visit();

        // We can access static variables or methods by referencing the class where they live
        //or by statically importing them
        City.isClean = true;
        cityIsClean();

        isWarm = true;
        System.out.println(cityIsWarm());

    }
}

package sdet_academy.day_39;

import java.util.*;

public class EmployeeObject {
    public static void main(String[] args) {
        Employee employee = new Employee();
        Employee.companyName = "Favor";
        employee.name = "Nick Second Name";
        employee.gender = 'M';
        employee.id = 134535;
        employee.title = "Engineer";
        employee.salary = 200_000;
        employee.work();
        employee.getSalary();
        System.out.println(Employee.getCompanyName());
        Employee employee1 = new Employee();
        employee1.name = " Myra Rab";
        employee1.id = 2464374;
        Employee employee2 = new Employee();
        employee2.name = "Liuba B";
        employee2.id = 4354748;
        Map<String,Integer> map = new HashMap<>();
        map.put(employee1.name, employee1.id);
        map.put(employee2.name, employee2.id);
        map.put(employee.name, employee.id);
        System.out.println(map);
        Employee employee3 = new Employee();
        employee3.name = "Liuba B";


        List<Employee> list = new ArrayList<>();
        list.add(employee);
        list.add(employee1);
        list.add(employee2);

        ArrayList<Employee> list2 = new ArrayList<>();
        list2.add(employee);
        list2.add(employee1);
        list2.add(employee2);
        list2.add(employee3);

        Map<String, Integer> map2 = new HashMap<>();

        for (Employee eachEmployee : list) {
            map2.put(eachEmployee.name, eachEmployee.id);
        }
        System.out.println(map2);

        Map<Employee, String> map3 = new HashMap<>();
        map3.put(employee, employee.name);
        map3.put(employee1, employee1.name);
        map3.put(employee2, employee2.name);

        System.out.println(map3);
        System.out.println(list2);

        Set<String> set = new LinkedHashSet<>();
        for (Employee eachEmployee : list2) {
            set.add(eachEmployee.name);
        }

        System.out.println(set);
    }
}

package sdet_academy.day_39;

public class Employee {
    public String name;
    public int id;
    public double salary;
    public String title;
    public char gender;
    public static String companyName;

    public void work(){
        System.out.println("Employee " + name + "with id "+ id + " gender " + gender + " works at company" + companyName);
    }
    public static String getCompanyName() {
        return companyName;
    }
    public void getSalary(){
        System.out.println("Employee with title " + title + " have salary " + salary);
    }
}

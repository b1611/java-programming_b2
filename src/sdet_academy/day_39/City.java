package sdet_academy.day_39;

import java.util.LinkedHashMap;
import java.util.Map;

public class City {
    public String name;
    public int age;
    public Map<String, String> coordinates = new LinkedHashMap<>();
    public long population;
    public boolean isCapital;
    public String citySize;
    public static boolean isClean;
    public static boolean isWarm;

    public City(){}

    public City(int population){
        this.population = population;
    }

    public City(boolean isCapital, String name){
        this.isCapital = isCapital;
        this.name = name;
    }
    public City(String name, boolean isCapital){
        this.isCapital = isCapital;
        this.name = name;
    }


    public void visit() {
        System.out.println("Visiting " + name + " located at " + coordinates);
    }

    public void live() {
        System.out.println("Living in the " + name + " that is " + age + " years old");
    }

    public String getCitySize() {
        if (population > 0 && population < 1_000_000) {
            citySize = "small";
        } else if (population >= 1_000_000 && population < 2_000_000) {
            citySize = "medium";
        } else if (population >= 2_000_000) {
            citySize = "large";
        }
        return citySize;
    }

    public static void cityIsClean(){
        System.out.println(isClean);
    }

    public static boolean cityIsWarm(){
        return isWarm;
    }

}

package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ReverseArrayList {
    public static void main(String[] args) {
        /**
         * Create a custom method that will reverse an ArrayList<Integer>
         * [1,2,3,4,5,6]
         * [6, 5 , 4 ... 1]
         * result: [6,5,4,3,2,1]
         */
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6));
        System.out.println(list);
        System.out.println(reverseArrayList(list));
    }

    public static ArrayList<Integer> reverseArrayList(ArrayList<Integer> list) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (int i = list.size() - 1; i >= 0; i--) {
            resultList.add(list.get(i));
        }
        return resultList;
    }
}

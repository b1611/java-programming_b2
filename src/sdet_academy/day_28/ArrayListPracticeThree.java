package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListPracticeThree {
    public static void main(String[] args) {
        /**
         * [Java, C#, C++, Kotlin, Ruby, Pascal, JavaScript, Scala, Groovy, PHP, TypeScript, C]
         * [4, 2, 3, 6, 4, 6.....1]
         */
//
//        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "C#", "C++", "Kotlin", "Ruby", "Pascal", "JavaScript", "Scala", "Groovy", "PHP", "TypeScript", "C"));
//        ArrayList<Integer> resultList = new ArrayList<>();
//        for (int i = 0; i < list.size(); i++) {
//            resultList.add(list.get(i).length());
//                    //     Java.length()
//        }

//        System.out.println(resultList);

        ArrayList<String> list = ConvertStringIntoListMethod.convertStringIntoArrayListMethod("Java, C#, C++, Kotlin, Ruby, Pascal, JavaScript, Scala, Groovy, PHP, TypeScript, C");
        System.out.println(list);
        for (String each : list) {
            System.out.println(each);
        }
    }
}

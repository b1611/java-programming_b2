package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListInt {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,6,5,4,7,8,9));

        System.out.println(list);
        System.out.println(list.size());
        System.out.println(list.isEmpty());

        list.remove(0);
        System.out.println(list);
        list.remove(7); // - we will only be able to remove an element by the index when
        // dealing with ArrayList of Integers
        System.out.println(list);

    }
}

package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class FindSecondMinimum {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(4, 1, -47, 2, 0, 6, 1, 2, 0));
        //result = 0;
        System.out.println(findMinimum(list));
    }

    public static int findMinimum(ArrayList<Integer> list){
        int min= Integer.MAX_VALUE;
        int secondMin= Integer.MAX_VALUE;

        for (int i = 0; i < list.size() ; i++) {
            if(min>list.get(i)){
                min=list.get(i);

            }

        }
        for (int i = 0; i < list.size() ; i++) {
            if(list.get(i)>min && list.get(i)<secondMin){
                secondMin=list.get(i);
            }

        }
        return secondMin;
    }
}

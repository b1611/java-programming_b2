package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListPracticeTwo {
    public static void main(String[] args) {
        /**
         * [1,2,3,4,5,6,7,8] > [odd, even, odd, even ..... even]
         */

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,7,8));
        ArrayList<String> resultList = new ArrayList<>();
        // [] > [odd]
        // [odd] > [odd, even]

        for (int i = 0; i < list.size(); i++) {
            if(list.get(i)%2==0){
                resultList.add("even");
            }else{
                resultList.add("odd");
            }
        }
        System.out.println(resultList);
        System.out.println(evenOrOdd(list));
    }
    public static ArrayList<String> evenOrOdd(ArrayList<Integer> list){
        ArrayList<String> resultList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i)%2==0){
                resultList.add("even");
            }else{
                resultList.add("odd");
            }
        }
        return resultList;
    }
}

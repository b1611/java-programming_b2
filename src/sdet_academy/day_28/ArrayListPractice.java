package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListPractice {
    public static void main(String[] args) {

        /**
         * Multiply each element by 2
         * [1,3,5,7,9,5,6,4] > [2,6,10,14,18,10,12,8]
         */

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 3, 5, 7, 9, 5, 6, 4));
        System.out.println(list);

        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i)*2);
    //      arr[i] = arr[i]*2 - analogy for the array
        }
        System.out.println(list);
    }
}

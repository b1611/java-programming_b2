package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListIntro {
    public static void main(String[] args) {
        String[] strArr = new String[5];
        strArr[0] = "Luba";
        System.out.println(Arrays.toString(strArr));
        strArr[4] = "Iryna";
        strArr[3] = "Angie";
        strArr[0] = "Myroslava";
        System.out.println(Arrays.toString(strArr));

//        System.out.println(strArr.length);
//        System.out.println(strArr[0].length());

        ArrayList<String> strList = new ArrayList<>();
        System.out.println(strList);
        strList.add("Luba");
        System.out.println(strList);
        strList.add("Iryna");
        strList.add("Angie");
        strList.add("Myroslava");
        strList.add("Myroslava");

        System.out.println(strList);

        String[] strArrTwo = {"Luba", "Slava", "Radu"};
        System.out.println(Arrays.toString(strArrTwo));

        ArrayList<String> strListTwo = new ArrayList<>(Arrays.asList("Slava", "Luba", "Radu", "Angie"));
        System.out.println(strListTwo);

        ArrayList<String> strListThree = new ArrayList<>(Arrays.asList(strArrTwo));
        System.out.println(strListThree);

        strList.add("Nick"); // add() will append an element into our ArrayList
        System.out.println(strList);
        System.out.println(strList.get(2)); // get() will get us an element at the index provided

        for (int i = 0; i < strList.size(); i++) {
            System.out.println(strList.get(i));
        }

        System.out.println(strList.size()); // size() will return us integer with the size of our ArrayList/List
        System.out.println(strList.get(strList.size()-1));

        strList.set(strList.size()-1, "Max"); // set() will accept 2 arguments,
        // integer for the index of the element you want to change, and a new value for that element
        System.out.println(strList);

//        strList.clear(); // clear() will remove all elements from our ArrayList
//        System.out.println(strList);

        strList.remove(0); // remove() is an overloaded method and can accept index integer,
        // that will remove the element at the specified index
        System.out.println(strList);

        strList.remove("Myroslava");
        System.out.println(strList);
        strList.remove("Maxxx");
        System.out.println(strList);
        // strList.remove(10); - will give us index out of bound

        System.out.println(strList.contains("Maxxx")); // contains() will return us true or false, depending if the
        // element we are looking for is present in our ArrayList


    }
}

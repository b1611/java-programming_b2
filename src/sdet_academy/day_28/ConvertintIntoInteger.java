package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ConvertintIntoInteger {
    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5};
        String[] strArr = {"Hello", "World", "Bye"};

        ArrayList<String> list = new ArrayList<>(Arrays.asList(strArr));
        System.out.println(list);
        System.out.println(list.get(0));

        //ArrayList<Integer> intList = new ArrayList<>(Arrays.asList(arr)); - will give us compile error

        Integer[] numArr = {1,2,3,4,5};
        ArrayList<Integer> intList = new ArrayList<>(Arrays.asList(numArr)); // - will work because it's a non-primitive array

        ArrayList<Integer> integerList = convertPrimitiveIntMethod(arr);
    }

    public static ArrayList<Integer> convertPrimitiveIntMethod(int[] arr){
        ArrayList<Integer> resultList = new ArrayList<>();
        for (int each : arr) {
            resultList.add(each);
        }
        return resultList;
    }
}

package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class FindMinimum {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,3,6,-45,1,0,4,5,7));
        // result: 0

        System.out.println(findMinimum(list));
    }

    public static int findMinimum(ArrayList<Integer> list){
        int min = Integer.MAX_VALUE;
        for (Integer each : list) {
            if(min>each){
                min = each;
            }
        }
        return min;
    }
}

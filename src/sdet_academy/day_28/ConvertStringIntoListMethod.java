package sdet_academy.day_28;

import java.util.ArrayList;
import java.util.Arrays;

public class ConvertStringIntoListMethod {
    public static void main(String[] args) {
        /**
         * create a custom method that will accept String and return ArrayListOfStrings
         */
        String str = "Java, C#, C++, Kotlin, Ruby, Pascal, JavaScript, Scala, Groovy, PHP, TypeScript, C";
        System.out.println(convertStringIntoArrayListMethod(str));
        ArrayList<String> list = convertStringIntoArrayListMethod(str);
        for (String each : list) {
            System.out.println(each);
        }

    }
    public static ArrayList<String> convertStringIntoArrayListMethod(String str){
        ArrayList<String> resultList = new ArrayList<>(Arrays.asList(str.split(", ")));
//        ArrayList<String> resultList = new ArrayList<>(Arrays.asList("Java", "C#"));
        return resultList;
    }
}

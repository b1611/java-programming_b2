package sdet_academy.day_34;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetTheName {
    public static void main(String[] args) {

        /**
         * Mr. Schafer@gmail
         * Mr Smith@yahoo
         * Ms Davis@work
         */

        String nameOne = "Mr. Schafer@gmail";
        String nameTwo = "Mr Smith@yahoo";
        String nameThree = "Ms Davis@work";

        Pattern pattern = Pattern.compile("M(r|s)\\.? ?(\\w+)");
        Matcher matcher = pattern.matcher(nameOne);
        Matcher matcherTwo = pattern.matcher(nameTwo);
        Matcher matcherThree = pattern.matcher(nameThree);

        matcher.find();
        matcherTwo.find();
        matcherThree.find();

        String actualNameOne = matcher.group(2);
        System.out.println(actualNameOne);
        String actualNameTwo = matcherTwo.group(2);
        System.out.println(actualNameTwo);
        String actualNameThree = matcherThree.group(2);
        System.out.println(actualNameThree);

    }
}

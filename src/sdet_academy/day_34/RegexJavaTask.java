package sdet_academy.day_34;

import java.util.Arrays;

public class RegexJavaTask {
    public static void main(String[] args) {

        String word= "DC501GCCCA989GDSSG41";// 501 + 989 + 41 = ?
        String[] regexArr = word.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
        System.out.println(Arrays.toString(regexArr));

    }
}

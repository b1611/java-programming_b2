package sdet_academy.day_34;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MathRegex {
    public static void main(String[] args) {
        /**
         * 100 divided by 20
         * Group indexes:
         * 0 - the entire String
         * 1 - group #1
         * 2 - group #2
         * 3 - group #3
         */

        Pattern pattern = Pattern.compile("(\\d+)( \\D+)(\\d+)"); // this is our regex
        Matcher matcher = pattern.matcher("100 module        by 20"); // this is the String we are working on

        int result = 0;
        matcher.find();

        if(matcher.group(2).contains("divided")) {
            result = Integer.parseInt(matcher.group(1)) / Integer.parseInt(matcher.group(3));
        }else if(matcher.group(2).contains("multiply")){
            result = Integer.parseInt(matcher.group(1)) * Integer.parseInt(matcher.group(3));
        }else if(matcher.group(2).contains("add") || matcher.group(2).contains("plus")){
            result = Integer.parseInt(matcher.group(1)) + Integer.parseInt(matcher.group(3));
        }else if(matcher.group(2).contains("remainder") || matcher.group(2).contains("module")){
            result = Integer.parseInt(matcher.group(1)) % Integer.parseInt(matcher.group(3));
        }else if(matcher.group(2).contains("minus") || matcher.group(2).contains("subtract")){
            result = Integer.parseInt(matcher.group(1)) - Integer.parseInt(matcher.group(3));
        }

        System.out.println(result);
    }
}

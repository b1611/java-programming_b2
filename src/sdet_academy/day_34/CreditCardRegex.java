package sdet_academy.day_34;

public class CreditCardRegex {
    public static void main(String[] args) {

        String creditCard = "1111 2222 3333 1234"; // XXXX-XXXX-XXXX-1234
        String maskedCreditCard = creditCard.replaceAll("\\d{4}.\\d{4}.\\d{4}.", "XXXX-XXXX-XXXX-");
        System.out.println(maskedCreditCard);

    }
}

package sdet_academy.day_5;

public class Weather {
    public static void main(String[] args){
        /**
         * If the temp is below 0 - print run for your life
         * if the temp is between 0 and 32 - print it's freezing
         * if the temp is between 33 and 55 - print it's cold
         * if the temp is between 56 and 75 - print it's warm
         * if the temp is between 76 and 100 - print it's hot
         * if the temp is above a 100 - print it's scorching hot
         */

        double temp = -87;
        if (temp < 0) {
            System.out.println("Run for your life");
        } else if (temp >= 0 && temp <= 32) {
            System.out.println("It's freezing");
        } else if (temp >= 33 && temp <= 55) {
            System.out.println("It's cold.");
        } else if (temp >= 56 && temp <= 75) {
            System.out.println("It's warm");
        } else if (temp >= 76 && temp <= 100) {
            System.out.println("It's hot");
        } else {
            System.out.println("Its scarching hot");
        }

    }
}

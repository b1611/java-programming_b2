package sdet_academy.day_5;

public class CastingIntro {
    public static void main(String[] args){
        byte numOne = 15;
        short numTwo = numOne;
        int numThree = 36;
        long numFour = numThree;
        int numFive = 99;
        double numSix = numFive; // 99.0

        double numSeven = 96.7;
        int numEight = (int) numSeven; //96
        System.out.println(numEight);

        System.out.println(numSeven/0.25);
        System.out.println(numEight/0.25);

        float numNine = 10.7f;
        short numTen = (short) numNine;
        System.out.println(numTen);



    }
}

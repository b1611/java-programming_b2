package sdet_academy.day_5;

public class IfPracticeWithString {
    public static void main(String[] args){
        String word = "Java";
        String wordTwo = "Java";
        String wordThree = "java";

        if(word.equals(wordTwo) && word.equals(wordThree)){
            System.out.println("everything equals");
        }else {
            System.out.println("not everything equals");
        }

        if(word.equals(wordTwo) || word.equals(wordThree)){
            System.out.println("everything equals");
        }else {
            System.out.println("not everything equals");
        }


        if(word.equals(wordTwo) && !word.equals(wordThree)){
            System.out.println("everything equals");
        }else {
            System.out.println("not everything equals");
        }


        if(word.equals(wordTwo) || !word.equals(wordThree)){
            System.out.println("everything equals");
        }else {
            System.out.println("not everything equals");
        }

    }
}

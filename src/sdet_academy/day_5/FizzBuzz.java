package sdet_academy.day_5;

public class FizzBuzz {
    public static void main(String[] args){

        /**
         * If the number is divisible by 3 and 5 - print FizzBuzz
         * if the number is divisible by 3 and not 5 - print Fizz
         * if the number is divisible by 5 and not 3 - print Buzz
         * if the number is 0 - print the number is zero
         */

        int num = 0;
//1.
        if (num%3==0 && num%5==0 && num!=0){
            System.out.println("FizzBuzz");
        }else if(num%3==0 && num%5!=0){
            System.out.println("Fizz");
        }else if(num%3!=0 && num%5==0){
            System.out.println("Buzz");
        }else if(num==0){
            System.out.println("The number is zero");
        }

//2.
        if (num==0){
            System.out.println("The number is zero");
        }else if(num%3==0 && num%5!=0){
            System.out.println("Fizz");
        }else if(num%3!=0 && num%5==0){
            System.out.println("Buzz");
        }else if(num%3==0 && num%5==0){
            System.out.println("FizzBuzz");
        }

//3.
        if (num%3==0 && num%5==0 && num!=0){
            System.out.println("FizzBuzz");
        }else if(num%3==0 && num%5!=0){
            System.out.println("Fizz");
        }else if(num%3!=0 && num%5==0){
            System.out.println("Buzz");
        }else {
            System.out.println("The number is zero");
        }

//4.
        if (num==0){
            System.out.println("The number is zero");
        }else if(num%3==0 && num%5!=0){
            System.out.println("Fizz");
        }else if(num%3!=0 && num%5==0){
            System.out.println("Buzz");
        }else{
            System.out.println("FizzBuzz");
        }
    }
}

package sdet_academy.day_5;

public class EvenOrOddVersion2 {
    public static void main(String[] args) {

        /**
         * Given you have a variable:
         * if the variable is even - print even
         * if the variable is odd - print odd
         * if the variable is 0 - print 0
         */

        int num1 = 0;

        if (num1 == 0) {
            System.out.println("Zero");
        } else if (num1 % 2 != 0) {
            System.out.println("Odd");
        } else if (num1 % 2 != 1) {
            System.out.println("Even");
        }


        if(num1 % 2 != 0) {
            System.out.println("Odd");
        }else if (num1 % 2 != 1 && num1!=0) {
            System.out.println("Even");
        }else{
            System.out.println("Zero");
        }

    }
}

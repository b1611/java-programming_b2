package sdet_academy.day_5;

public class WatchShop {
    public static void main(String[] args) {
        /**
         * Create a program that will determine the price of the watch based on the brand
         * - if the brand is Rolex - the price is 1000
         * - if the brand is Tisot - the price is 500
         * - if the brand is Citizen - the price is 200
         * - if the brand is Movado - the price is 800
         * - if the brand is Casio - the price is 200
         *
         *
         */
        String watch = "Citizen";
        double price = 0;

        if (watch.equals("Rolex")) {
            price = 1000;
            System.out.println("Price is $ " +price);
        } else if (watch.equals("Tisot")) {
            price = 500;
            System.out.println("Price is $ " +price);
        } else if (watch.equals("Citizen") || watch.equals("Casio")){
            price = 200;
            System.out.println("Price is $ " +price);
        } else if (watch.equals("Movado")){
            price = 800;
            System.out.println("Price is $ " +price);
        } else {
            System.out.println("Invalid");
        }
    }
}

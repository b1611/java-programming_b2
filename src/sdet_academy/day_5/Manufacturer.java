package sdet_academy.day_5;

public class Manufacturer {
    public static void main(String[] args) {
        /**
         * Create a program that will determine the country of the manufacturer based on the vehicle make:
         * - if vehicle is toyota, or honda or nissan - the manufacturer is Japan
         * - if vehicle is Mercedes, BMW or AUDI - the manufacturer is Germany
         * - if vehicle is Ford, Chrysler or GMC - the manufacturer is USA
         */
        String car = "dddd";

        if (car.equals("Toyota") || car.equals("Honda") || car.equals("Nissan")) {
            System.out.println("Its Japan");
        } else if (car.equals("Mercedes") || car.equals("BMW") || car.equals("AUDI")) {
            System.out.println("Its Germany");
        } else if (car.equals("Ford") || car.equals("Chrysler") || car.equals("GMC")) {
            System.out.println("Its USA");
        } else {
            System.out.println("Invalid");
        }

    }
}

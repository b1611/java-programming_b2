package sdet_academy.day_5;

public class CastingPractice {
    public static void main(String[] args) {
        short num1 = 45;
        byte num2 = (byte)num1;
        float num3 = num1;

        double num4 = 45.69;
        int num5 = (int) num4;
        System.out.println(num4==num5);
        System.out.println(num4!=num5);


    }
}

package sdet_academy.day_5;

public class OperatorsWIthIfStatement {
    public static void main(String[] args) {

        int a = 15;
        int b = 15;
        int c = 25;

        if (a <= b && b < c) {
//           true      true = true
//           false     true = false
//           true      false = false
//           false     false = false

            System.out.println("c is the greater number");
        } else {
            System.out.println("c is not the greatest number");
        }


        int x = 20;
        int y = 15;
        int z = 30;

        if(x<y || y>z){
//         true     true = true
//         false    true = true
//         true     false = true
//         false    false = false

            System.out.println("if block is executed");
        }else{
            System.out.println("Else block is executed");
        }


    }
}
package sdet_academy.day_5;

public class BarberShop {
    public static void main(String[] args){

        /**
         * Create a program that will determine the price for the haircut based on the gender;
         * if customer is a female - the haircut price is $20, the tips - 5
         * if customer is a male - the haircut price is $15, the tips - 3
         * if customer is a child - the haircut price is $12, the tips - 2
         * The grand total should include haircut price + tax(8%) + tip
         */

        double price;
        String gender = "Male";
        double total = 0;
        double tips;
        double tax = 0.08;

        if (gender.equals("Male")) {
            price = 15;
            tips = 3;
            total = (price + (price * tax)) + tips;
        } else if (gender.equals("Female")) {
            price = 20;
            tips = 5;
            total = (price + (price * tax)) + tips;
        } else if (gender.equals("Child")) {
            price = 12;
            tips = 2;
            total = (price + (price * tax)) + tips;
        }
        System.out.println("Total $ = " + total);
    }
    }


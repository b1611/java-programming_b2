package sdet_academy.day_5;

public class IfPractice {
    public static void main(String[] args){
        double doubleOne = 10.5;
        double doubleTwo = 25.5;
        double doubleThree = 35.0;

        int intOne = (int) doubleOne;
        int intTwo = (int) doubleTwo;
        int intThree = (int) doubleThree;

        /**
         * Create the following conditional statements:
         * - If doubleOne equals to intOne and doubleTwo equals to intTwo - print equal, else print not equal
         * - if doubleOne equals to intOne or doubleTwo equals to intTwo - print equal, else print not equal
         * - If doubleOne equals to intOne and doubleThree equals to intThree - print equal, else print not equal
         * - if doubleOne equals to intOne or doubleThree equals to intThree - print equal, else print not equal
         *
         */

        if(intOne==doubleOne && intTwo==doubleTwo){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }

        if(intOne==doubleOne || intTwo==doubleTwo){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }

        if(intOne==doubleOne && intThree==doubleThree){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }

        if(intOne==doubleOne || intThree==doubleThree){
            System.out.println("equal");
        }else{
            System.out.println("not equal");
        }

    }
}

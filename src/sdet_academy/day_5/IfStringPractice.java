package sdet_academy.day_5;

public class IfStringPractice {
    public static void main(String[] args){
        String make = "Toyota";
        String model = "Sienna";
        String makeTwo = "Honda";
        String modelTwo = "Odyssey";
        double price = 0;

        /**
         * Create an if condition:
         * - if the make is toyota and model is sienna - set the price to 50000 and print "total price is " + price
         * add an else block for invalid input.
         * - if the make is honda and the model is not Odyssey - invalid input, else set the price to 55000 and print the following:
         * "total price is " + price
         */

        if(make.equals("Toyota") && model.equals("Sienna")){
            price=50000;
            System.out.println("The price for this vehicle is $" + price);
        } else {
            System.out.println("invalid input");
        }


        if(makeTwo.equals("Honda") && !modelTwo.equals("Odyssey")){
            System.out.println("invalid input");
        } else {
            price = 55000;
            System.out.println("The price for this vehicle is $" + price);
        }


    }
}

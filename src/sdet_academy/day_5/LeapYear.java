package sdet_academy.day_5;

public class LeapYear {
    public static void main(String[] args){

        /**
         * Create a program that will determine if it's a leap year or not
         */

        long year = 2000;

        if(year%4==0 && year!=0){
            System.out.println("It's a leap year");
        }else{
            System.out.println("It's not a leap year");
        }

    }
}

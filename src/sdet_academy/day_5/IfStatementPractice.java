package sdet_academy.day_5;

public class IfStatementPractice {
    public static void main(String[] args){
        int numOne = 100;
        double numTwo = 200.5;
        long numThree = 111111;

        /**
         * Create a conditional statement:
         * - if numOne is greater than numTwo - print numOne is greater
         * - if numTwo is greater than numThree - print numTwo is greater
         * - if numOne is less than numThree - print numOne is less, else print numThree is greater
         */

        if(numOne>numTwo){
            System.out.println("NumOne is greater");
        }

        if(numTwo>numThree){
            System.out.println("NumTwo is greater");
        }

        if(numOne<numThree){
            System.out.println("NumOne is less than NumThree");
        }else {
            System.out.println("NumOne is greater than NumThree");
        }

    }
}

package sdet_academy.day_35;

import java.util.HashMap;
import java.util.Map;

public class EvenOrOdd {
    public static void main(String[] args) {
/**
 * Example {1=odd, 2=even, 3=odd, 4=even, 5=odd, 7=odd, 8=even, 10=even}
 * {1=odd, 2=0, 3=odd, 4=0, 5=odd, 7=odd, 8=0, 10=0}
 */

        Map<Integer, String> map = new HashMap<>();
        map.put(2, "even");
        map.put(10, "even");
        map.put(4, "even");
        map.put(8, "even");
        map.put(1, "odd");
        map.put(3, "odd");
        map.put(5, "odd");
        map.put(7, "odd");

        System.out.println(map);

        for (Integer eachKey : map.keySet()) {
            System.out.println(eachKey);
            if(eachKey%2==0){
                map.replace(eachKey, "0");
            }
        }

        System.out.println(map);
    }
}

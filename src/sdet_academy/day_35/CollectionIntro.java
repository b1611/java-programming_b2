package sdet_academy.day_35;

import java.util.*;

public class CollectionIntro {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));
        System.out.println(list);
        System.out.println(list.get(2));

        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));
        System.out.println(list2);
        System.out.println(list2.get(2));

        Set<String> set = new HashSet<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));
        //  List                    ArrayList
        System.out.println(set);
        set.add("Nick");
        System.out.println(set);

        HashSet<String> set2 = new HashSet<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));
        System.out.println(set2);
        set2.add("Nick");
        System.out.println(set2);

        Set<String> set3 = new LinkedHashSet<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));
        System.out.println(set3);
        set3.add("Nick");
        System.out.println(set3);

        System.out.println(uniqueList(list2));
        System.out.println(uniqueOrderedList(list2));
    }
    public static Set<String> uniqueList(ArrayList<String> list){
        return new HashSet<>(list);
    }
    public static Set<String> uniqueOrderedList(ArrayList<String> list){
        return new LinkedHashSet<>(list);
    }
}

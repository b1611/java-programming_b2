package sdet_academy.day_35;

import java.util.PriorityQueue;
import java.util.Queue;

public class QueueIntro {
    public static void main(String[] args) {
        Queue<String> queue = new PriorityQueue<>();
        queue.offer("Myroslava");
        queue.offer("Myroslava");
        queue.offer("Slava");
        queue.offer("Luba");
        queue.offer("Iryna");
        queue.offer("Angie");
        queue.offer("Radu");
        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue);
        queue.remove();
        System.out.println(queue);
        System.out.println(getName(queue, "lu ba"));

    }
    public static String getName(Queue<String> queue, String targetName){
        for (String each : queue) {
            if(each.equalsIgnoreCase(targetName.replace(" ", ""))){
                return each;
            }
        }
        return null;
    }
}

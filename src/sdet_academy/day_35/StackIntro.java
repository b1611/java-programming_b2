package sdet_academy.day_35;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class StackIntro {
    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("Luba");
        stack.push("Angie");
        stack.push("Angie");
        stack.push("Iryna");
        stack.push("Radu");
        stack.push("Radu");
        stack.push("Slava");
        stack.push("Myroslava");

        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack);
        System.out.println(stack.get(0));
        System.out.println(stack);
        stack.pop();
        System.out.println(stack);

        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));

        Stack<String> stack2 = new Stack<>();
        stack2.addAll(list2);
        stack2.pop();
        System.out.println(stack2);
    }
}

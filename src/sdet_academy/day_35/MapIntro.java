package sdet_academy.day_35;

import java.util.HashMap;
import java.util.Map;

public class MapIntro {
    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>();
        //   key      value
        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.isEmpty());

        map.put(1, "Slava");
        map.put(1, "Slava");
        map.put(3, "Angie");
        map.put(4, "Luba");
        map.put(100, "Iryna");
        map.put(200, "Radu");

        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.get(100));
        System.out.println(map.get("Iryna"));

//        map.clear(); - will delete all the elements in our map
//        System.out.println(map);
        map.remove(4); // - if trying to delete non existing key, nothing will happen
        System.out.println(map);

        map.remove(100, "Iryna");
        System.out.println(map);

        System.out.println(map.get(5));
        System.out.println(map.getOrDefault(1, "Element is not present"));

        System.out.println(map.containsKey(1));
        System.out.println(map.containsValue("Radu"));

//        map.replace(1, "Super Slava");
        map.put(1, "Super Slava");
        System.out.println(map);

    }
}

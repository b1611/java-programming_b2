package sdet_academy.day_35;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class IteratorIntro {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>
                (Arrays.asList("Luba", "Luba", "Slava", "Radu", "Radu", "Angie", "Iryna", "Iryna"));

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            iterator.next();
            iterator.remove();
            System.out.println(list);
        }
    }
}

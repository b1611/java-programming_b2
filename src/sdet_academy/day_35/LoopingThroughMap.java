package sdet_academy.day_35;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LoopingThroughMap {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Iryna", "B");
        map.put("Luba", "B");
        map.put("Angie", "A");
        map.put("Slava", "M");
        map.put("Radu", "N");
        System.out.println(map);

        Map<String, String> map2 = new LinkedHashMap<>();
        map2.put("Iryna", "B");
        map2.put("Luba", "B");
        map2.put("Angie", "A");
        map2.put("Slava", "M");
        map2.put("Radu", "N");
        System.out.println(map2);

        for (String eachKey : map2.keySet()) {
            System.out.println(eachKey);
        }

        for (String eachValue : map2.values()) {
            System.out.println(eachValue);
        }

        for (Map.Entry<String, String> eachEntrySet : map2.entrySet()) {
            System.out.println(eachEntrySet);
        }

        for (String eachKey : map2.keySet()) {
            map.replace(eachKey, eachKey.replace("a", "#"));
        }

        List<String> a = map.entrySet().stream().map(x -> x.getKey().replace("a", "%%%")).collect(Collectors.toList());
        System.out.println(a);
        System.out.println(map);
    }
}

package sdet_academy.day_41;

public class Alligator {
    public String sizeAlligator;
    public static boolean isAgressive;
    public Alligator(){
        System.out.println("Constructor");
    }
    static{
        isAgressive=true;

        System.out.println("Static block");
    }
    {
        sizeAlligator="large";
        System.out.println("instance block");
    }
    public static boolean isAngry(){

        return isAgressive;
    }
    public String getSize(){

        return sizeAlligator;
    }

    public static void main(String[] args) {
        Alligator alligator=new Alligator();
        Alligator alligator1=new Alligator();
        System.out.println(alligator1.getSize());
        System.out.println(isAngry());

    }
}

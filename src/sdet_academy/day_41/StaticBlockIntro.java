package sdet_academy.day_41;

public class StaticBlockIntro {
    public static String animalType;
    public String animalColor;

    public StaticBlockIntro(){
        System.out.println("This is a constructor");
    }

    static {
        StaticBlockIntro staticBlockIntro = new StaticBlockIntro();
        staticBlockIntro.animalColor = "Red";
        System.out.println(staticBlockIntro.animalColor);
        System.out.println("This is a static block 1");
        animalType = "Dog";
        System.out.println(animalType);
    }

    public static void main(String[] args) {

        StaticBlockIntro staticBlockIntro = new StaticBlockIntro();
//        StaticBlockIntro staticBlockIntro1 = new StaticBlockIntro();
//        StaticBlockIntro staticBlockIntro2 = new StaticBlockIntro();

    }
}

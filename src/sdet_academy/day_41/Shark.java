package sdet_academy.day_41;

import static sdet_academy.day_40.FootballPlayer.play;

public class Shark {
    public String sharkBreed;
    public String sharkSize;

    public Shark() {
        System.out.println("This is a constructor for Shark");
    }
//    public Shark(String sharkBreed, String sharkSize){
//        this.sharkBreed = sharkBreed;
//        this.sharkSize = sharkSize;
//    }

    static {
        System.out.println("This is a static block for the shark");
    }

    {
        this.sharkSize = "Great";
        this.sharkBreed = "White Shark";
        System.out.println(sharkSize);
        System.out.println(sharkBreed);
    }

    public static void main(String[] args) {
        Shark shark = new Shark();
        Shark shark2 = new Shark();
    }
}

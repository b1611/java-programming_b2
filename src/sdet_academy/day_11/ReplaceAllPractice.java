package sdet_academy.day_11;

public class ReplaceAllPractice {
    public static void main(String[] args) {
        String wow = "My name is {Slava}, my father's name is {Johny} and my brother's name is {Bravo}";

        String myName = "Sonny";
        String father = "Tom";
        String brother = "Jerry";

        String result;

        result = wow.replaceAll("\\{Slava}",myName).replaceAll("\\{Johny}",father).replaceAll("\\{Bravo}",brother);
        System.out.println(result);
    }
}

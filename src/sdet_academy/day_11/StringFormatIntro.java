package sdet_academy.day_11;

public class StringFormatIntro {
    public static void main(String[] args) {

        String str = "Hello World";
        System.out.println(str.replace("l", "0"));
        System.out.println(str.replaceFirst("l", "0"));

        String strTwo = "Today is %s"; // Today is Tuesday
        System.out.println(strTwo);
        String day = "Tuesday";

        System.out.println(String.format(strTwo, day));


        String strThree = "Today I'm %d years old";
        int age = 18;
        System.out.println(String.format(strThree, age));

        String strFour = "Today temperature is %f degrees";
        float temp = 79.9f;
        System.out.println(String.format(strFour, temp));


    }
}

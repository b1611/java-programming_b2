package sdet_academy.day_11;

public class ReplaceAllIntro {
    public static void main(String[] args) {

        String str = "I live in {city} city in {state} state and {country} country";
        String city = "Philadelphia";
        String state = "Pennsylvania";
        String country = "USA";

        String result = str.replaceAll("\\{city}", city)
                .replaceAll("\\{state}", state)
                .replaceAll("\\{country}", country);
        System.out.println(result);

    }
}

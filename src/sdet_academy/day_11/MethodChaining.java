package sdet_academy.day_11;

public class MethodChaining {
    public static void main(String[] args) {

        /**
         * Str = "Java is fun" -> "JAvA*is*fu";
         */

        String str = "Java is fun";
        String target = "a";
        str = str.replace(target, target.toUpperCase()).replace(" ", "*").substring(0, str.length() - 1);
        System.out.println(str);


    }
}

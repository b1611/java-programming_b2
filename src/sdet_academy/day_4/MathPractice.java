package sdet_academy.day_4;

public class MathPractice {
    public static void main(String[] args){

        byte numOne = 10;
        byte numTwo = numOne++;
        System.out.println(numOne);
        System.out.println(numTwo);
        System.out.println(numOne>numTwo);
        System.out.println(numOne/5==1);
        System.out.println(numOne%2==0);
        System.out.println(numTwo%2==0);
        System.out.println(numOne!=numTwo);
        System.out.println(numOne==numTwo);


    }
}

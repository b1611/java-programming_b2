package sdet_academy.day_4;

public class MathPracticeTwo {
    public static void main(String[] args) {

        byte num1 = 10;
        short num2 = 20;
        int num3 = 30;
        long num4 = 40;
        float num5 = num4;
        double num6 = 60;

        System.out.println(num4);
        System.out.println(num5);

        System.out.println(num6 / 10 > 5);
        System.out.println(num6 / 10 != 10);
        num3 = num2;
        System.out.println(num3 == num2);
        System.out.println(num1 %2==0);

    }
}

package sdet_academy.day_4;

import java.util.Scanner;

public class BestBuyReceipt {
    public static void main(String[] args){

        Scanner shop = new java.util.Scanner(System.in);


        System.out.println("Enter the name of product one");
        String nameOne = shop.next();
        System.out.println("Enter the name of product Two");
        String nameTwo = shop.next();
        System.out.println("Enter the name of product Three");
        String nameThree = shop.next();

        System.out.println("Enter price " + nameOne );
        double priceOne = shop.nextDouble();
        System.out.println("Enter price " +  nameTwo);
        double priceTwo = shop.nextDouble();
        System.out.println("Enter price " + nameThree);
        double priceThree = shop.nextDouble();

        String storeName = "BestBuy";
        System.out.println("Welcome to " + storeName);
        System.out.println(nameOne + " Price: $" + priceOne);
        System.out.println(nameTwo + " Price: $" + priceTwo);
        System.out.println(nameThree + " Price: $" + priceThree);
        System.out.println("Total amount: $" + (priceOne + priceTwo + priceThree));


    }
}

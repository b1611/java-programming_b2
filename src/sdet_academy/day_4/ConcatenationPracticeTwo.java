package sdet_academy.day_4;

public class ConcatenationPracticeTwo {
    public static void main(String[] args) {

        int egg = 2;
        int pepper = 1;
        int tomato = 1;

        String ing1 = "Egg";
        String ing2 = "Pepper";
        String ing3 = "Tomato";

        System.out.println("Omelet recipe ");

        System.out.println("Cooking the omelet with this ingredients \n" + "\n" + egg + " " + ing1 + "\n" + pepper + " " + ing2 + "\n" + tomato + " " + ing3);

    }
}
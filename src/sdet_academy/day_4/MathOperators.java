package sdet_academy.day_4;

public class MathOperators {
    public static void main(String[] args) {

        int numOne = 10;
        int numTwo = 15;

        System.out.println(numOne > numTwo);
        System.out.println(numOne < numTwo);
        System.out.println(numOne != numTwo);

        int numThree = 20;
        int numFour = 20;

        System.out.println(numThree <= numFour);
        System.out.println(numThree >= numFour);
        System.out.println(numThree == numFour);


//        numFour+=10;
        numFour = numFour + 10;
        System.out.println(numFour);

//        numThree-=10;
        numThree = numThree - 10;
        System.out.println(numThree);

//        numTwo/=5;
        numTwo = numTwo / 5;
        System.out.println(numTwo);

//        numOne*=10;
        numOne = numOne * 10;
        System.out.println(numOne);



    }
}

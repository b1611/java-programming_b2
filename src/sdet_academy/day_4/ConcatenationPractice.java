package sdet_academy.day_4;

public class ConcatenationPractice {
    public static void main(String[] args) {
        int buildingNumber;
        String streetName;
        String city;
        String state;
        String country;

        buildingNumber = 1;
        streetName = "Johnson Street";
        city = "New York";
        state = "New York";
        country = "USA";

        System.out.println("I live at the following address\n" + buildingNumber + " " + streetName + " " + city + " " + state + " " + country);

    }
}

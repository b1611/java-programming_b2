package sdet_academy.day_4;

import java.util.Scanner;

public class ScannerRecap {
    public static void main(String[] args){

        /**
         * Create a program that will use scanner to accept the input and will calculate the perimeter of the triangle
         */

        Scanner in = new Scanner(System.in);
        int sideOne;
        int sideTwo;
        int sideThree;

        System.out.println("Welcome to our program");
        System.out.println("Please enter the size of the first side of the triangle:");
        sideOne = in.nextInt();
        System.out.println("Please enter the size of the second side of the triangle:");
        sideTwo = in.nextInt();
        System.out.println("Please enter the size of the third side of the triangle:");
        sideThree = in.nextInt();

        System.out.println("The perimeter of the triangle is " + (sideOne + sideTwo + sideThree));

        //int a = 15;
        int a;
        a = 25;



    }
}

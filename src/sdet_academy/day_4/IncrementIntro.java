package sdet_academy.day_4;

public class IncrementIntro {
    public static void main(String[] args){

        int a = 10;
        System.out.println(a);
        int b = a++;
        System.out.println(b);
        System.out.println(a);

        int c = 20;
        System.out.println(c);
        int d = --c;
        System.out.println(d);
        System.out.println(c);
    }
}

package sdet_academy.day_37;

import java.util.*;

public class ReverseArrayToMap {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 4, 7, 8));
        System.out.println(reverseList(list));
    }

    public static Map<Integer, Integer> reverseList(ArrayList<Integer> list) {
        Map<Integer, Integer> map = new HashMap<>();
        if (list.size() % 2 == 1) {

            for (int i = 0, j = list.size()-1; i <= list.size()/2; i++, j--) {
                map.put(list.get(i), list.get(j));
            }
        }else{
            for (int i = 0, j = list.size()-1; i < list.size()/2; i++, j--) {
                map.put(list.get(i), list.get(j));
            }
        }
        return map;
    }
}


package sdet_academy.day_9;

public class UpperCaseOrLowerCase {
    public static void main(String[] args) {

        String name = "voldemort";

        String str = name.toUpperCase(); //VOLDEMORT

        if(name.equals(str)){
            System.out.println("name is uppercase");
        }else{
            System.out.println("name is lowercase");
        }

    }
}

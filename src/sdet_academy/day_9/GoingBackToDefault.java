package sdet_academy.day_9;

public class GoingBackToDefault {
    public static void main(String[] args) {

        String str = "Java"; //avaJ

        String manipulated = str;
        manipulated = manipulated.toUpperCase();

        System.out.println(manipulated);

        manipulated = str;
        System.out.println(manipulated);

    }
}

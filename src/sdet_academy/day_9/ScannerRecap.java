package sdet_academy.day_9;

import java.util.Scanner;

public class ScannerRecap {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter num");
        int num = scanner.nextInt();

        System.out.println("Enter numTwo");
        double numTwo = scanner.nextDouble();

        System.out.println("Enter name");
        String name = scanner.nextLine();



        System.out.println(num);
        System.out.println(numTwo);
        System.out.println(name);
    }
}

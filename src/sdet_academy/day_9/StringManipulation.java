package sdet_academy.day_9;

public class StringManipulation {
    public static void main(String[] args) {

//        String name = "John";
//        String str = "My name is John";
//
//        System.out.println(name.equals(str));
//        System.out.println(str.contains(name));


        String nameOne = "JaVa";
        String nameTwo = "jAvA";


        System.out.println(nameOne.equals(nameTwo));
        System.out.println(nameOne.equalsIgnoreCase(nameTwo));

        if(nameOne.equals(nameTwo)){
            System.out.println("Names match");
        }else if (!nameOne.equals(nameTwo)){
            System.out.println("Names don't match");
        }

        /**
         * Given strOne = "Java" and strTwo = "JavaScript"
         * create a conditions to check if
         * - if strOne and strTwo are equals
         * - if one of them contains the other
         */

        String strOne="Java";
        String strTwo="JavaScript";
        if (strOne.equals(strTwo)) {
            System.out.println("Equal");
        }else{
            System.out.println("Not equal");
        } if(strTwo.contains(strOne)){
            System.out.println("Yes");
        }else{
            System.out.println("NO");
        }

        System.out.println(strTwo.contains(strOne));

        System.out.println(strOne.toUpperCase());
        strOne = strOne.toUpperCase();
        System.out.println(strOne);

        strOne = "python";
        System.out.println(strOne);


    }
}

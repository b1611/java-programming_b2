package sdet_academy.day_9;

public class StringManipulationContinue {
    public static void main(String[] args) {

        String str = "Slava";
        // indexes    012345678
        System.out.println(str.length()); //will return us the count of characters in our String(count starts with 1)

        System.out.println(str.isEmpty()); //will count a space as a character
        System.out.println(str.isBlank()); //will not count a space as a character

//        int lastIndex = str.length()-1;
        System.out.println(str.charAt(str.length()-1));

        System.out.println(str.charAt(str.length()/2));

        if(str.isEmpty() || str.length()==0){
            System.out.println("str is empty");
        }        else if(str.length()%2==0){
            System.out.println("Str is even");
        }else {
            System.out.println("str is odd");
        }

        String word = "python";
        String word2 = word.toUpperCase(); //PYTHON

        if(word.length()==word2.length()){
            System.out.println("same length");
        }else {
            System.out.println("not the same length");
        }


        System.out.println(word.charAt(0));//first character
        System.out.println(word.charAt(word.length()-1));// last character
        System.out.println(word.charAt(word.length()/2));//middle character

    }
}

package sdet_academy.day_9;

public class StartsWithEndsWith {
    public static void main(String[] args) {
        String str = "Java is fun";

        System.out.println(str.startsWith("J")); //will return boolean if the argument matches the first character
        System.out.println(str.endsWith("n")); //will return boolean if the argument matches the last character

        char character = str.charAt(0);// option + enter(mac) or alt + enter (windows) to introduce the local variable

        String characterString = "" + str.charAt(0);// the easies way to convert char to String is to concatenate "" beforehand


        if(characterString.equals("J")){
            System.out.println("true");
        }else{
            System.out.println("false");
        }

        String lastCharacterString = "" + str.charAt(str.length() - 1);

        if(lastCharacterString.equals("n")){
            System.out.println("true");
        }else{
            System.out.println("false");
        }


        char gender = 'm';

        System.out.println(str.toUpperCase());
    }
}

package sdet_academy.day_9;

import java.util.Random;

public class HomeWork {
    public static void main(String[] args) {
        /**
         Create a program, for car traveling. Try to use random class where possible. You can create additional variables if needed
         Create String variables: carMake, carModel(use random class to randomly pick different vehicles)
         Numberic variables: MPG(Miles Per Gallon), distance to travel, distance till tank is empty(calculate it in the formula)
         If your distance to travel is more than distance till tank is empty - print "Get fuel now"
         Otherwise keep driving.
         Try to use Ternary for this exercise of possible.
         Print a nice big statement with the vehicle make and model and advise if the driver should continue driving or find the nearest gas station
         */
        Random random = new Random();

        String carMake;
        String carModel;
        int MPG = 0;
        int gasTank = 10;
        long distanceToTravel = 300;
        int makePicker = random.nextInt(3);
        int modelPicker = random.nextInt(2);
        String message = "";

        if (makePicker == 0) {
            carMake = "BMW";
            if (modelPicker == 0) {
                carModel = "X5";
                MPG = 7;
            } else {
                carModel = "M5";
                MPG = 13;
            }
        } else if (makePicker == 1) {
            carMake = "AUDI";
            if(modelPicker == 0){
                carModel = "T7";
                MPG = 25;
            }else {
                carModel = "A4";
                MPG = 32;
            }
        } else {
            carMake = "VOLVO";
            if(modelPicker == 0){
                carModel = "XC90";
                MPG = 25;
            }else{
                carModel = "XC70";
                MPG = 37;
            }
        }

        long distanceTillEmpty = MPG * gasTank;
        message =  distanceTillEmpty>distanceToTravel ? "Keep Driving" : "Get Fuel Now!";
        System.out.println("You are driving " + carMake + " " + carModel + " - " + message +
                "You are driving " + carMake + " " + carModel + " - " + message +
                "You are driving " + carMake + " " + carModel + " - " + message +
                "You are driving " + carMake + " " + carModel + " - " + message +
                "You are driving " + carMake + " " + carModel + " - " + message +
                "You are driving " + carMake + " " + carModel + " - " + message);


    }
}

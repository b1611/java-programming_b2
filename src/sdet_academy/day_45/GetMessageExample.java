package sdet_academy.day_45;

public class GetMessageExample {
    public static void main(String[] args) {

        String str = "Hello";
//        System.out.println(str.substring(30));
        try {
            System.out.println(str.substring(30));
        }catch (StringIndexOutOfBoundsException e){
            System.out.println(e.getMessage());
        }
        System.out.println("END");
    }
}

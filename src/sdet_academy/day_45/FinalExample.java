package sdet_academy.day_45;

public class FinalExample {
    final int num=10;



    public final void printSomething(){
        System.out.println("Something");
    }

    public final void printSomething(String str){
        System.out.println("Something " + str);
    }

    public final void printSomething(int ... nums){
        for (int each : nums) {
            System.out.println(each);
        }
    }
}

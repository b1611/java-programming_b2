package sdet_academy.day_45;

import java.util.Arrays;

public class FinalObject {
    public static void main(String[] args) {
        FinalExampleChild finalExampleChild = new FinalExampleChild();
        int firstNum = 10;
        int secondNum = 20;
        String strWord = "Hello";

        final int FIRST_NUM = 10;
        final int SECOND_NUM = 20;
        final String STR_WORD = "Hello";
        int maxValue = Integer.MAX_VALUE;

        DaysOfTheWeek tuesday = DaysOfTheWeek.TUESDAY;
        System.out.println(tuesday.equals("TUESDAY"));
        System.out.println(tuesday.toString().equals("TUESDAY"));
        System.out.println(tuesday);

        DaysOfTheWeek[] values = DaysOfTheWeek.values();

        for (DaysOfTheWeek eachEnum : values) {
            System.out.println(eachEnum);
        }

        System.out.println(Arrays.toString(values));

    }
}

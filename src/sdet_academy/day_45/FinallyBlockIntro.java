package sdet_academy.day_45;

public class FinallyBlockIntro {
    public static void main(String[] args) {



        String str = "Hello";

        try {
            System.out.println(str.charAt(20));
            System.out.println("THere was no exception");
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("Catching the exception");
        }catch (ArrayIndexOutOfBoundsException e2){
            System.out.println("Catching the exception");
        }
        finally {
            System.out.println("Finally block is executed");
        }

        System.out.println("END");
    }
}

package sdet_academy.day_45;

public class ExceptionIntro {
    public static void main(String[] args)  {

        //        Thread.sleep(2000); - will give us checked exception

//        int[] arr = {1,2,3,40};
//        System.out.println(arr[10]); //  - will give us unchecked(runtime) exception


//        int[] arr = {1,2,3,4};
//
//        try {
//            System.out.println(arr[10]);
//        }catch (StringIndexOutOfBoundsException e){
//            System.out.println("handled StringIndexOutOfBoundsException with the catch");
//        }catch (ArrayIndexOutOfBoundsException e){
//            System.out.println("handled ArrayIndexOutOfBoundsException with the catch");
//        }

        int a = 10;
        int b = 0;

        try {
            System.out.println(a/b);
        }catch (ArithmeticException e){
            System.out.println("In the catch block");
//            b=1;
            try{
                System.out.println(a / b);
            }
            catch(ArithmeticException e2) {
                System.out.println("Inside of the nested catch");
            }
        }




        System.out.println("This is the end of the main method");
    }

    public static void printSomething() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println("i = " + i);
            Thread.sleep(1000);
        }
    }
}

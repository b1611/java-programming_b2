package sdet_academy.day_43;

public class Vehicle {
    public int numberOfWheels = 4;
    public int horsePowers;
    public boolean isElectric;

    public void ride(){
        System.out.println("Riding a vehicle");
    }

    public void issues(){
        System.out.println("The vehicle has issues");
    }
}

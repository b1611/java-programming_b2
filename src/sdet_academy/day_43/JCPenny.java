package sdet_academy.day_43;

import sdet_academy.day_44.Store;

public class JCPenny extends Store {

    @Override
    protected void sellSomething(){
        System.out.println("Selling Something at JC Penny");
    }

//    void closeStore(){
//        System.out.println("Closing our store for good");
//    }
}

package sdet_academy.day_43;

public class Puppy{

    public void drinkMilk(){
        System.out.println("Puppy drinks milk");
    }

    public void sleep(){
        System.out.println("Puppy is sleeping");
    }
}

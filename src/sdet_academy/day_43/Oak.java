package sdet_academy.day_43;

public class Oak extends Trees{
    public boolean isHard;

    public void fall(){
        System.out.println("The oak is falling");
    }

    public void stayingGreen(){
        System.out.println("Oak is not staying green over the winter");
    }
}

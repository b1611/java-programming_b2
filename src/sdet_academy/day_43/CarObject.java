package sdet_academy.day_43;

public class CarObject {
    public static void main(String[] args) {
        Vehicle vehicle = new Tesla();
        Tesla tesla = new Tesla();
        Mercedes mercedes = new Mercedes();

        System.out.println(tesla.numberOfWheels);

        vehicle.ride();
        tesla.ride();
        tesla.issues();

        mercedes.issues();
        mercedes.ride();

    }
}

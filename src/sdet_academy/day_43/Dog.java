package sdet_academy.day_43;

public class Dog extends Animal {

    public void bark() {
        System.out.println("The dog is barking");
    }

    public void sleep() {
        System.out.println("The dog is sleeping");
    }

    public void drinkWater() {
        Puppy puppy = new Puppy();
        puppy.drinkMilk();
    }
}

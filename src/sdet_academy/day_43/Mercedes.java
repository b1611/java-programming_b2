package sdet_academy.day_43;

public class Mercedes extends Vehicle{

    public void issues(){
        System.out.println("The Mercedes has issues");
    }

    public void notImpressive(){
        System.out.println("Not a dream car for the money!");
    }
}

package sdet_academy.day_43;

public class TestInheritance {
    public static void main(String[] args) {
        Animal animal = new Animal();
        Dog dog = new Dog();
        Puppy puppy = new Puppy();

        animal.sleep();
        animal.hunting();

        dog.sleep();

        puppy.drinkMilk();
        puppy.sleep();
    }
}

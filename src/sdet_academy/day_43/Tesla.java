package sdet_academy.day_43;

public class Tesla extends Vehicle{
    public int numberOfWheels = 3;

    public void issues(){
        System.out.println("The Tesla has issues");
    }

    public void chargingVehicle(){
        System.out.println("Charging my electric vehicle");
    }
}

package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveEndsWith {
    public static void main(String[] args) {
        /**
         * Given an ArrayList<String>, remove the elements that end with vowels: a, e, i , u, y , o
         *     [Java, Python, Kotlin, Ruby, Scala, C++]
         *     [Python, Kotlin, C++]
         */

        String str = "Java";
        System.out.println(str.endsWith("a"));
        System.out.println(str.startsWith("j"));

        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "Python", "Kotlin", "Ruby", "Scala", "C++"));
        //OPTION 1
//        list.removeIf(s -> s.endsWith("a")
//                || s.endsWith("e")
//                || s.endsWith("i")
//                || s.endsWith("u")
//                || s.endsWith("y")
//                || s.endsWith("o"));

//      OPTION 2
        list.removeIf(s->endsWithLetter(s));
        System.out.println(list);
    }

    public static boolean endsWithLetter(String str) {
        if (str.endsWith("a")
                || str.endsWith("e")
                || str.endsWith("i")
                || str.endsWith("u")
                || str.endsWith("y")
                || str.endsWith("o")){
            return true;
        }
        return false;
    }
}

package sdet_academy.day_31;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class SortArrayOfStrings {
    public static void main(String[] args) {

        String[] strArr = {"Cat", "cat", "Dog", "Map", "Gas", "Bat"};
        //[bat, cat, dog, gas, map, cat]
        Object[] objects = Arrays.stream(strArr).sorted().map(s -> s.toLowerCase()).toArray();
        Object[] objects1 = Arrays.stream(strArr).map(s -> s.toLowerCase()).sorted().toArray();
        System.out.println(Arrays.toString(objects));
        System.out.println(Arrays.toString(objects1));

        int[] numArr = {1,2,31,6,54,6,59,84,51};
        int[] result = Arrays.stream(numArr).sorted().toArray();
        System.out.println(Arrays.toString(result));
    }
}

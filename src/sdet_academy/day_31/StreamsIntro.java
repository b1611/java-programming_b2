package sdet_academy.day_31;

import java.util.Arrays;

public class StreamsIntro {
    public static void main(String[] args) {

        //MAP will modify each element
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * 10;
        }
        System.out.println(Arrays.toString(arr));


        int[] arrResult = Arrays.stream(arr).map(x -> x / 10).toArray();
        System.out.println(Arrays.toString(arrResult));

        int[] arrSquare = Arrays.stream(arrResult).map(n -> n * n).toArray();
        System.out.println(Arrays.toString(arrSquare));

        //DISTINCT will remove duplicates

        int[] arrNum = {1, 1, 2, 3, 4, 5, 6, 4, 55, 44};
        //             [1, 2, 3, 4, 5, 6, 55, 44]


        int[] arrNumResult = Arrays.stream(arrNum).distinct().toArray();
        System.out.println(Arrays.toString(arrNumResult));

//        System.out.println(Arrays.toString(removeDuplicates(arrNum)));

        // FILTER will keep the elements base on the condition

        int[] arrNumTwo = {1,-5,2,3,-7,0,-8};
        //                  [1, 2, 3, 0]
        int[] arrNumTwoResult = Arrays.stream(arrNumTwo).filter(x -> x >= 0).toArray();

        System.out.println(Arrays.toString(removeNegative(arrNumTwo)));

        System.out.println(Arrays.toString(arrNumTwoResult));

    }





    public static int[] onlyUnique(int[] arr) {
        int count;
        String temp = "";

        for (int i = 0; i < arr.length; i++) {
            count = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    count++;
                }
            }
            if (count == 1) {
                temp += arr[i] + ",";
            }
        }
        String[] split = temp.split(",");
        int[] result = new int[split.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(split[i]);
        }
        return result;
    }


    //        int[] arrNum = {1,1,2,3,4,5,6,4,55,44};
    //        [1, 2, 3, 4, 5, 6, 55, 44]
    public static int[] removeDuplicates(int[] arr) {
        String temp = "";

        for (int i = 0; i < arr.length; i++) {
            if (!temp.contains(String.valueOf(arr[i]))) {
                temp += "" + arr[i] + ",";
            }
        }
        String[] split = temp.split(",");
        int[] result = new int[split.length];

        for (int i = 0; i < split.length; i++) {
            result[i] = Integer.parseInt(split[i]);
        }
        return result;
    }

    //      int[] arrNumTwo = {1,-5,2,3,-7,0,-8};
    //                  [1, 2, 3, 0]
    public static int[] removeNegative(int[] arr){
        String temp = "";
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]>=0){
                temp+=arr[i] + ",";
            }
        }
        String[] split = temp.split(",");
        int[] result = new int[split.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(split[i]);
        }
        return result;
    }
}

package sdet_academy.day_31;

import java.util.Arrays;

public class StreamsContinue {
    public static void main(String[] args) {

        //distinct(remove duplicates)
        String[] strArr = {"Java", "java", "python", "python", "java", "python"};
        Object[] objects = Arrays.stream(strArr).distinct().toArray();
        System.out.println(Arrays.toString(objects));

        // map(modifying all the elements)
        Object[] objects1 = Arrays.stream(strArr).map(s -> s.replace("a", "_")).toArray();
        System.out.println(Arrays.toString(objects1));

        // filter(keeping certain items based on the condition)
        Object[] objects2 = Arrays.stream(strArr).filter(s -> s.length() <= 4).toArray();
        System.out.println(Arrays.toString(objects2));

        int[] arrNum = {1,2,3,4,5,6,7,8,9,10};

        int sum2 = 0;
        for (int each : arrNum) {
            sum2+=each;
        }
        System.out.println(sum2);

        double asDouble = Arrays.stream(arrNum).average().getAsDouble();
        System.out.println(asDouble);

        int asInt = Arrays.stream(new int[]{1, 2, 3, 4, 5}).max().getAsInt();
        System.out.println(asInt);

        double asDouble1 = Arrays.stream(new double[]{2.2, 3.5, 5, 5.7}).max().getAsDouble();
        System.out.println(asDouble1);

        double asDouble2 = Arrays.stream(new double[]{2.2, 3.5, 5, 5.7}).min().getAsDouble();
        System.out.println(asDouble2);

        double sum = Arrays.stream(new double[]{2.2, 3.5, 5, 5.7}).sum();
        System.out.println(sum);


    }
}

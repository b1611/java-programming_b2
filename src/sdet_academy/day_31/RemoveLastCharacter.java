package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveLastCharacter {
    public static void main(String[] args) {
        /**
         * 1. Given a list of strings, return a list where each string has it's last character removed
         * [Java, Python, Pascal]
         * [Jav, Pytho, Pasca]
         * 2. Make every element to UpperCase()
         * [Jav, Pytho, Pasca]
         * [JAV, PYTHO, PASCA]
         */
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "Python", "Pascal"));
        list.replaceAll(s -> s.substring(0, s.length() - 1).toUpperCase());
        System.out.println(list);
    }
}

package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class KeepTheRange {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
        Collections.shuffle(list);
        // list, int begin, int end
        // list, 4, 7
        // result [4,5,6,7]
        System.out.println(keepRangeMethod(list, 4, 7));
    }

    public static List<Integer> keepRangeMethod(ArrayList<Integer> list, int begin, int end) {
        return list.stream().filter(x -> x >= begin && x <= end).sorted().collect(Collectors.toList());
    }
}

package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FrequencyPractice {
    public static void main(String[] args) {
        /**
         *
         * String str = "zzpppoyyyyy" // create a program that will store only unique elements
         * and elements with frequency
         * of more than 3
         * Return the result as a String
         * "zzpppoyyyyy" = "oyyyyy"
         */


        String str = "ZZPPPyyyyA";
        ArrayList<String> list = new ArrayList<>(Arrays.asList(str.split("") ));
        System.out.println(list);

        List<String> collect = list.stream().filter(x -> Collections.frequency(list, x) > 3 || Collections.frequency(list, x) == 1).collect(Collectors.toList());
        System.out.println(collect);

    }
}

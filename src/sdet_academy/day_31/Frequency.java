package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Frequency {
    public static void main(String[] args) {
        String str = "aabcccdeeeeeeffi";
        ArrayList<String> list = new ArrayList<>(Arrays.asList(str.split("")));
        //[a, a, b, c, c, c, d, e, e, e, e, e, e, f, f, i]
        System.out.println(list);

        List<String> collect = list.stream().filter(x -> Collections.frequency(list, x) > 1).collect(Collectors.toList());
        System.out.println(collect);
    }
}

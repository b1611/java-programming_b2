package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveX {
    public static void main(String[] args) {
        /**
         * Given a list of strings, return a list where each string has all its "x" removed.
         * ["ax", "bb", "cx"]) → ["a", "bb", "c"]
         */

        ArrayList<String > list =new ArrayList<>(Arrays.asList("ax", "bb", "cx"));
        list.replaceAll(s-> s.replace("x", ""));
        System.out.println(list);
    }
}

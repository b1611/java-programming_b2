package sdet_academy.day_31;

import java.util.Arrays;

public class Recap {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};
        //            [1,3,5]

       keepOddElements(arr);


        int sum = 0;

        for (int each : arr) {
            sum += each;
        }

        int sum1 = Arrays.stream(arr).sum();

        System.out.println(sum);
        System.out.println(sum1);


    }

    public static int[] keepOddElements(int[] arr) {
        String temp = "";

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 1) {
                temp += "" + arr[i] + "#";
            }
        }
        String[] split = temp.split("#");

        int[] result = new int[split.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(split[i]);
        }
        return result;
    }

    public static int[] keepOddElementStream(int[] arr){
        return Arrays.stream(arr).filter(x -> x % 2 == 1).toArray();
    }
}

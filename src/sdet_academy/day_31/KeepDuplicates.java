package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KeepDuplicates {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("Jav a","Ja va", "c++", "ja VA","pyth ON", "c#", "PYTHON", "P Y TH O N", "Pa ascal", "K ot lin", "kotLIN"));
        //[java, java, java, python, python, python]
        List<String> collect = list.stream().map(x -> x.replace(" ", "").toLowerCase()).collect(Collectors.toList());
        System.out.println(collect);
        List<String> result = collect.stream().filter(x -> Collections.frequency(collect, x) > 1).collect(Collectors.toList());
        System.out.println(result);

        List<String> list1 = list.stream().map(x -> x.replace(" ", "").toLowerCase())
                .filter(x -> Collections.frequency(list, x) > 1).collect(Collectors.toList());
        System.out.println(list1);

        List<String> collect1 = collect.stream().distinct().collect(Collectors.toList());
        System.out.println(collect1);

        List<String> collect2 = collect.stream().filter(x -> Collections.frequency(collect, x) == 1).collect(Collectors.toList());
        System.out.println(collect2);

        int blue = Collections.frequency(collect, "blue");
        System.out.println(blue);

    }
}

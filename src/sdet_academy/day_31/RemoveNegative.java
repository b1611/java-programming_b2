package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RemoveNegative {
    public static void main(String[] args) {
        /**
         * Given an ArrayList of integers create a custom method:
         * 1. remove all elements <0
         * 2. sort an ArrayList
         * Example:
         * [1,3,1,-2,94,3,-10,4,531,-71]
         * [1,1,3,3,4,94,531]
         */
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 3, 1, -2, 94, 3, -10, 4, 531, -71));
        System.out.println(removeNegative(list));
    }

    public static ArrayList<Integer> removeNegative(ArrayList<Integer> list) {
        list.removeIf(n -> n < 0);
        Collections.sort(list);
        return list;
    }
}

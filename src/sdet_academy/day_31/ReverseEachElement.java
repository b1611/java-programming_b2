package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;

public class ReverseEachElement {
    public static void main(String[] args) {

        /**
         * Given an ArrayList<String>
         * [red, blue, grey, purple, green]
         * Result:
         * [der, uelb, yerg, elprup, neerg]
         */

        ArrayList<String> list = new ArrayList<>(Arrays.asList("red", "blue", "grey", "purple", "green"));
        list.replaceAll(s->reverseString(s));
        System.out.println(list);

    }

    public static String reverseString(String str){
        String result = "";
        for (int i = str.length()-1; i >=0; i--) {
            result+=str.charAt(i);
        }
        return result;
    }
}

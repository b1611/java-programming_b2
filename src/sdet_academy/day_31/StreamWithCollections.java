package sdet_academy.day_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StreamWithCollections {
    public static void main(String[] args) {

        String str = "aabcdddc";

        ArrayList<String> list = new ArrayList<>(Arrays.asList(str.split("")));
        //[a, a, b, c, d, d, d, c]
        System.out.println(list);

        int frequency = Collections.frequency(list, "d");
        System.out.println(frequency);


        String str2 = "abbbcddeeeeef";
        //              [a,b,b,b,c,d,d,e,e,e,e,e,f]
        //              [b,b,b,d,d,e,e,e,e,e]

        ArrayList<String> list2 = new ArrayList<>(Arrays.asList(str2.split("")));
        System.out.println(list2);

        List<String> collect = list2.stream().distinct().collect(Collectors.toList());
        //                                              .toArray

        System.out.println(collect);

        ArrayList<Integer> list3 = new ArrayList<>(Arrays.asList(0,1,0,2,3,5,4,6));
        List<Integer> collect1 = list3.stream().map(n -> n * n).collect(Collectors.toList());
        System.out.println(collect1);

        ArrayList<Integer> list4 = new ArrayList<>(Arrays.asList(0,1,0,2,3,5,4,6));
        List<Integer> collect2 = list4.stream().filter(x -> x < 3).collect(Collectors.toList());
        System.out.println(collect2);

        List<Integer> collect3 = list4.stream().sorted().collect(Collectors.toList());
        System.out.println(collect3);

        Collections.reverse(collect3);
        System.out.println(collect3);

        collect3.stream().filter(x->x>4).collect(Collectors.toList());
    }
}

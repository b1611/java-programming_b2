package sdet_academy.day_27;

public class CallingAnotherMethod {
    public static void main(String[] args) {
        String str = "Today is a great day in USA"; // tasK: keep all the vowels and reverse the String

        System.out.println(OverloadingPracticeTwo.keepVowels(str));
        System.out.println(reverseAndKeepVowelsStr(str));

    }

    public static String reverseAndKeepVowelsStr(String str){
        StringBuilder stringBuilder = new StringBuilder(str);
        return OverloadingPracticeTwo.keepVowels(stringBuilder.reverse().toString());
    }
}

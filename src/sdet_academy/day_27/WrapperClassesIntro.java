package sdet_academy.day_27;

public class WrapperClassesIntro {
    public static void main(String[] args) {
        int a = 15; // primitive
        Integer b = a; //unboxing

        Integer x = 15; //non-primitive
        int y = x; // auto-boxing

        Integer z = new Integer(15);

        System.out.println(a==x);
        System.out.println(a==z);
        System.out.println(x==z);

        Integer result = a + x + y + z;
        System.out.println(a + x + y + z);
        System.out.println(result);
    }
}

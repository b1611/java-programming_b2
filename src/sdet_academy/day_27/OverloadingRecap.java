package sdet_academy.day_27;

public class OverloadingRecap {
    public static void main(String[] args) {
        /**
         * Create a custom method and overload it so we can have the following
         * 1. Method will accept String and char and will return the frequency of the char in the String
         * Example:
         * "Java is fun", 'a'
         * result: 2
         * 2. Overload the method to accept String and int index, that will return a character at the index
         * Example:
         * "Java is fun", 6
         * result: s
         */

        String str = "Java is fun";
        char a = 'a';
        int index = 6;
        System.out.println(overloadingRecap(str, index));

    }

    public static int overloadingRecap(String str, char a) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == a) {
                count++;
            } else if (!str.contains(String.valueOf(a))) {
                return -1;
            }
        }
        return count;
    }

    public static char overloadingRecap(String str, int index) {
        return str.charAt(index);
    }
}
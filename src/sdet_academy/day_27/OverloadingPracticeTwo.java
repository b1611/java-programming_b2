package sdet_academy.day_27;

public class OverloadingPracticeTwo {
    public static void main(String[] args) {
        /**
         * Create a method that will accept String and keep only the vowel letter.
         * Example:
         * "Java is fun"
         * result: aaiu
         * Overload this method, that will accept String and Integer
         * and will return only vowels up to the index of your Integer
         * Example:
         * "Java is fun", 7
         *  012345678910
         * result: aai fun
         * Vowels: a, e, i, o, u, y
         */

        String str = "Java is fun";
        System.out.println(keepVowels(str));
        System.out.println(keepVowels(str, 7));

    }

    public static String keepVowels(String str) {
        //OPTION 1
//     return   str.replace("b", "")
//                .replace("c", "")
//                //.........
//                .replace("z", "");

        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'a' ||
                    str.charAt(i) == 'e' ||
                    str.charAt(i) == 'i' ||
                    str.charAt(i) == 'o' ||
                    str.charAt(i) == 'u' ||
                    str.charAt(i) == 'y') {
                result += str.charAt(i);
            }
        }
        return result;
    }

    public static String keepVowels(String str, Integer num) {
        //OPTION 1
//        String beginning = str.substring(0, num);
//        String ending = str.substring(num);
//        String result = "";
//
//        for (int i = 0; i < beginning.length(); i++) {
//            if (str.charAt(i) == 'a' ||
//                    str.charAt(i) == 'e' ||
//                    str.charAt(i) == 'i' ||
//                    str.charAt(i) == 'o' ||
//                    str.charAt(i) == 'u' ||
//                    str.charAt(i) == 'y') {
//                result += str.charAt(i);
//            }
//        }
//
//        return result + ending;

//          OPTION 2
        String result = "";

        for (int i = 0; i < num; i++) {
            if (str.charAt(i) == 'a' ||
                    str.charAt(i) == 'e' ||
                    str.charAt(i) == 'i' ||
                    str.charAt(i) == 'o' ||
                    str.charAt(i) == 'u' ||
                    str.charAt(i) == 'y') {
                result += str.charAt(i);
            }
        }
        return result + str.substring(num);
    }
}

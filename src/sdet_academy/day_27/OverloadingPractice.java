package sdet_academy.day_27;

public class OverloadingPractice {
    public static void main(String[] args) {
        /**
         * Create an custom method that will accept the int[] and will return the average of the array as an int
         * 1. Example:
         * [1,2,3,4,5]
         * result = 3
         *
         * Overload this method, to accept an int and int[] and if the int is present in our array,
         * we will return the index of it.
         * Example:
         * 3, [1,3,5,6,3,7]
         * result  = 1
         *    * Example:
         * 9, [1,3,5,6,7]
         * result  = -1
         */

        int[] numArr = {1,2,3,4,5};
        System.out.println(getSomething(numArr));
        System.out.println(getSomething(9, numArr));

    }
    public static int getSomething(int[] numArr){
        int sum = 0;
        for (int each : numArr) {
            sum+=each;
        }
        return sum/numArr.length;
    }

    public static int getSomething(int num, int[] numArr){
        for (int i = 0; i < numArr.length; i++) {
            if(numArr[i]==num){
                return i;
            }
        }
        return -1;
    }
}

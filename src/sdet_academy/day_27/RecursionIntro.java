package sdet_academy.day_27;

public class RecursionIntro {
    public static void main(String[] args) {
        printSomething(1);
    }

    public static void printSomething(int num){
        if(num<=30){
            System.out.println(num);
            printSomething(num);
        }
    }
}

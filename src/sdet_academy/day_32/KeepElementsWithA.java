package sdet_academy.day_32;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class KeepElementsWithA {
    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<>(Arrays.asList("Java", "Python", "python", "JavaScript", "Kotlin", "Scala"));
        System.out.println(removeWithoutA(list));
    }

    public static List<String> removeWithoutA(ArrayList<String> list){
       // OPTION 1
        return list.stream().filter(x->x.toLowerCase().contains("a")).collect(Collectors.toList());
        // OPTION 2
//        return list.stream().filter(x->x.contains("a") || x.contains("A")).collect(Collectors.toList());
        //OPTION 3
//            list.removeIf(x->!x.toLowerCase().contains("a"));
//            return list;
    }
}

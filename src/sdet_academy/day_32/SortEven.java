package sdet_academy.day_32;

import java.util.ArrayList;
import java.util.Arrays;

public class SortEven {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        // System.out.println(sortEvens(list));
        System.out.println(sortOdds(list));

    }

    public static ArrayList<Integer> sortEvens(ArrayList<Integer> list) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 0) {
                result.add(list.get(i));
            }
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 == 1) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public static ArrayList<Integer> sortOdds(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.size(); j++) {
                int temp;
                if (list.get(i) % 2 == 0) {
                    temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list;
    }
}

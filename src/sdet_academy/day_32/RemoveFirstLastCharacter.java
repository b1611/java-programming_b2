package sdet_academy.day_32;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveFirstLastCharacter {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("yellow", "green", "red", "brown", "purple"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("yellow", "green", "red", "brown", "purple"));

        long startTimeModifyingList = System.nanoTime();
        System.out.println(removeFirstLastCharacter(list));
        long endTimeModifyingList = System.nanoTime();
        System.out.println("Modifying existing list will take V1 " + (endTimeModifyingList-startTimeModifyingList) + " nanoSec");


        long startTimeModifyingList2 = System.nanoTime();
        System.out.println(removeFirstLastCharacter2(list2));
        long endTimeModifyingList2 = System.nanoTime();
        System.out.println("Modifying existing list will take V1 " + (endTimeModifyingList2-startTimeModifyingList2) + " nanoSec");

    }


    public static ArrayList<String> removeFirstLastCharacter(ArrayList<String> list) {
        list.replaceAll(x -> x.substring(1, x.length() - 1));
        return list;
    }

    public static List<String> removeFirstLastCharacter2(ArrayList<String> list) {
        return list.stream().map(s -> s.substring(1, s.length() - 1)).collect(Collectors.toList());
    }
}

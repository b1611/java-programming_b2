package sdet_academy.day_7;

public class CarDealership {
    public static void main(String[] args) {
        /**
         * 3 Variables(Make and Model and Price):
         * if Make is not on the list - print "Sorry we don't have this vehicle in stock"
         * if Make = Toyota then:
         *      if Model = "Camry" - print "The price for Toyota Camry is 30000"
         *      if Model = "Corolla" - print "The price for Toyota Camry is 25000"
         *      if the Model input is incorrect print "Sorry we don't have this Model right now"
         * if Make = BMW:
         *      if Model = "X1" - print "The price for BMW X1 is 64000"
         *      if Model = "X5" - print "The price for BMW X5 Camry is 100000"
         *      if the Model input is incorrect print "Sorry we don't have this Model right now"
         * if Make = "Mercedes":
         *      if Model = "GLE" - print "The price for Mercedes GLE is 110000"
         *      if Model = "TLC" - print "The price for Mercedes TLC is 45000"
         *      if the Model input is incorrect print "Sorry we don't have this Model right now"
         */

        String make = "Toyota";
        String model = "Sienna";
        double price = 0;


        if (make.isEmpty() || model.isEmpty()) {
            System.out.println("Either make or model is empty");
        } else if (make.equals("Toyota")) {
            if (model.equals("Camry")) {
                price = 30000;
                System.out.println("The price for Toyota Camry is $" + price);
            } else if (model.equals("Corolla")) {
                price = 25000;
                System.out.println("The price for Toyota Corolla is $" + price);
            } else {
                System.out.println("Sorry, we don't have Toyota " + model);
            }
        } else if (make.equals("BMW")) {
            if (model.equals("X1")) {
                price = 64000;
                System.out.println("The price for BMW X1 is $" + price);
            } else if (model.equals("X5")) {
                price = 100000;
                System.out.println("The price for BMW X5 is $" + price);
            } else {
                System.out.println("Sorry, we don't have BMW " + model);
            }
        } else if (make.equals("Mercedes")) {
            if (model.equals("GLE")) {
                price = 110000;
                System.out.println("The price for Mercedes GLE is $" + price);
            } else if (model.equals("TLC")) {
                price = 45000;
                System.out.println("The price for Mercedes TLC is $" + price);
            } else {
                System.out.println("Sorry, we don't have Mercedes " + model);
            }
        } else {
            System.out.println("Sorry we don't have " + make);
        }
    }


}


package sdet_academy.day_7;

public class HomeDepot {
    public static void main(String[] args){

        /**
         * If the brand is Milwaukee
         *      - if the product is drill - price is 250
         *      - if the product is screwdriver - price is 350
         * if the brand is Dewalt
         *      - if the product is saw - price is 130
         *      - if the product is hammer - price is 500
         * if the brand is Mokitta
         *      - if the product is compressor - price is 240
         *      - if the product is sprayer - price is 70
         */

        String brand = "Dewaltttttt";
        String product = "saw";
        double price = 0;

        if(brand.isEmpty() || product.isEmpty()){
            System.out.println("Either brand or product is empty");
        }else if(brand.equals("Milwaukee")){
            if(product.equals("drill")){
                price = 250;
                System.out.println("The price for Milwaukee Drill is " + price);
            }else if(product.equals("screwdriver")){
                price = 350;
                System.out.println("The price for Milwaukee Screwdriver is " + price);
            }
        }else if(brand.equals("Dewalt")){
            if(product.equals("saw")){
                price = 130;
                System.out.println("The price for Milwaukee Saw is " + price);
            }else if(product.equals("hammer")){
                price = 500;
                System.out.println("The price for Milwaukee Hammer is " + price);
            }
        }else if(brand.equals("Mokitta")){
            if(product.equals("compressor")){
                price = 240;
                System.out.println("The price for Milwaukee Compressor is " + price);
            }else if(product.equals("sprayer")){
                price = 70;
                System.out.println("The price for Milwaukee Sprayer is " + price);
            }
        }else{
            System.out.println("Sorry we no longer carry " + brand);
        }









    }
}

package sdet_academy.day_7;

import java.util.Random;

public class EvenOrOdd {
    public static void main(String[] args) {
        Random random = new Random();

        int num = random.nextInt(100);
        System.out.println(num);

        if (num == 0) {
            System.out.println("0");
        } else if (num % 2 == 0) {
            System.out.println("even");
        } else {
            System.out.println("odd");

        }
    }
}

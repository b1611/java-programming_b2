package sdet_academy.day_7;

public class FashionStore {
    public static void main(String[] args) {

        /**
         * If the buyer has a store membership they will receive $100 off.
         * Calculate the total price
         * if the brand is Gucci
         *      the price for bag is 3000
         *      the price for belt is 500
         *      the price for shoes is 1500
         * if the brand is Channel
         *      the price for bag is 8000
         *      the price for belt is 1000
         *      the channel shoes are out of stock
         * if the brand is Versace
         *      the price for bag is 5000
         *      the price for belt is 800
         *      the price for shoes is 1300
         */

        String brand = "Versace";
        double totalPrice;
        boolean hasMembership = true;
        double discountAmount = 0;
        double itemOnePrice = 0;
        double itemTwoPrice = 0;
        double itemThreePrice = 0;

        if(hasMembership){
            discountAmount = 100;
        }

        if(brand.equals("Gucci")){
            itemOnePrice = 3000;
            itemTwoPrice = 500;
            itemThreePrice = 1500;
        }else if(brand.equals("Channel")){
            itemOnePrice = 8000;
            itemTwoPrice = 1000;
            itemThreePrice = 0;
        }else if(brand.equals("Versace")){
            itemOnePrice = 5000;
            itemTwoPrice = 800;
            itemThreePrice = 1300;
        }

        totalPrice = itemOnePrice + itemTwoPrice + itemThreePrice - discountAmount;
        System.out.println("The total price is " + totalPrice);


    }
}

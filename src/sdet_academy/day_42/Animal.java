package sdet_academy.day_42;

public class Animal {
    /**
     * There are 4 access modifiers in Java:
     * - public
     * - protective
     * - default
     * - private
     */
    private String name;
    private String breed;
    private int age;

    //Example of a Getter gives us read only access
    public String getName(){
        return name;
    }

    public String getBreed(){
        return breed;
    }

    public int getAge(){
        return age;
    }

    //Example of Setters gives us write only access
    public void setName(String name){
        this.name = name;
    }

    public void setBreed(String breed){
        this.breed=breed;
    }

    public void setAge(int age){
        this.age=age;
    }
}

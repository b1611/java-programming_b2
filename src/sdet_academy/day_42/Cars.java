package sdet_academy.day_42;

public class Cars {
    private String make;
    private String model;
    private int year;
    private boolean isElectric;
    private boolean isFromCopart;

    public void  setMake(String make){
        this.make=make;
    }
    public void setModel (String model){
        this.model=model;
    }
    public void setYear (int year){
        this.year=year;
    }
    public void setIsElectric (boolean isElectric){
        this.isElectric=isElectric;
    }

    public void setFromCopart(boolean isFromCopart){
        this.isFromCopart=isFromCopart;
    }
    public String getMake(){
        return make;
    }
    public String getModel(){
        return model;
    }
    public int getYear(){
        return year;

    }
    public boolean isElectric(){
        return  isElectric;

    }
    public boolean isFromCopart(){
        return isFromCopart;
    }
}

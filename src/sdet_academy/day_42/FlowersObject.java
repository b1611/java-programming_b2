package sdet_academy.day_42;

public class FlowersObject {
    public static void main(String[] args) {
        Flowers flowers=new Flowers();

        flowers.setTypeOfFlower("Roses");
        flowers.setSeasonOfBlooming("Spring");
        flowers.setAnnual(true);
        System.out.println(flowers.getTypeOfFlower());
        System.out.println(flowers.getSeasonOfBlooming());
        System.out.println(flowers.getIsAnnual());
    }
}

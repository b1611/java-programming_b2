package sdet_academy.day_42;

public class FishingObject {
    public static void main(String[] args) {
        Fishing fishing=new Fishing();

        fishing.setFishType("Bass");
        System.out.println("The type of fish is "+fishing.getFishType());

        fishing.setSize(26);
        System.out.println(fishing.getSize());

        fishing.setPredator(true);
        System.out.println(fishing.getIsPredator());
    }
}

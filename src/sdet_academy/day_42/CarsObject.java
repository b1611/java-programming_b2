package sdet_academy.day_42;

public class CarsObject {
    public static void main(String[] args) {
        Cars cars = new Cars();

        cars.setIsElectric(false);
        cars.setFromCopart(true);
        cars.setMake("Audi");
        cars.setModel("Q5");
        cars.setYear(2018);
        System.out.println(cars.getMake());
        System.out.println(cars.getYear());
        System.out.println(cars.getModel());
        System.out.println(cars.isFromCopart());
        System.out.println(cars.isElectric());
    }
}

package sdet_academy.day_42;

public class Flowers {
    private String typeOfFlower;
    private String seasonOfBlooming;
    private boolean isAnnual;

    public String getTypeOfFlower() {
        return typeOfFlower;
    }

    public String getSeasonOfBlooming() {
        return seasonOfBlooming;
    }

    public boolean getIsAnnual() {
        return isAnnual;
    }

    public void setTypeOfFlower(String typeOfFlower) {
        this.typeOfFlower = typeOfFlower;
    }

    public void setSeasonOfBlooming(String seasonOfBlooming) {
        this.seasonOfBlooming = seasonOfBlooming;
    }

    public void setAnnual(boolean isAnnual) {
        this.isAnnual = isAnnual;
    }
}

package sdet_academy.day_42;

public class Fishing {
    private  String fishType;
    private int size;
    private boolean isPredator;


    public String getFishType(){
        return fishType;
    }
    public int getSize(){
        return size;
    }
    public boolean getIsPredator(){
        return isPredator;
    }

    public void setFishType(String fishType){
        this.fishType=fishType;
    }
    public void setSize(int size){
        this.size=size;
    }
    public void setPredator(boolean isPredator){
        this.isPredator=isPredator;
    }
}

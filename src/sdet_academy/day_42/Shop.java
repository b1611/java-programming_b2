package sdet_academy.day_42;

public class Shop {
    private String productName;
    private String brand;
    private double price;

    public String getProductName(){
        return productName;
    }
    public String getBrand (){
        return brand;
    }
    public double getPrice(){
        return price;
    }
    public void setProductName(String productName){
        this.productName=productName;

    }
    public void setBrand(String brand){
        this.brand = brand;

    }
    public void setPrice (double price){
        this.price=price;
    }
}

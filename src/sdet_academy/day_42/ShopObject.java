package sdet_academy.day_42;

public class ShopObject {
    public static void main(String[] args) {

        Shop shop = new Shop();
        shop.setProductName("Fridge");
        System.out.println(shop.getProductName());
        shop.setBrand("BRAUN");
        System.out.println(shop.getBrand());
        shop.setPrice(1500.99);
        System.out.println(shop.getPrice());

    }
}

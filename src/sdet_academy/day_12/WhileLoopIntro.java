package sdet_academy.day_12;

public class WhileLoopIntro {
    public static void main(String[] args) {

        int apple = 0; //6
        int apple2 = 0; //6
        int watermelon = 15;

        while (apple<5){
            System.out.println("apple = " + apple);
            apple++;
        }

        while(apple2<5){
            System.out.println("apple = " + apple2);
            apple2+=2;
        }

        while (watermelon>5){
            System.out.println("watermelon = " + watermelon);
            watermelon--;
        }

    }
}

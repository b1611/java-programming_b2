package sdet_academy.day_12;

public class NameTask {
    public static void main(String[] args) {
        /**
         * Given a String name:
         * - check if the name is not empty
         * - remove any excess spaces we may have in the beginning or at the end
         * - make sure we have at least first name and last name, if so, remove the last name
         * - if we have a middle name - remove it
         * Examples for names:
         *      Louisa May  Alcott
         *      joanne king rowling
         */
        //String name = "Louisa Alcott";

        String name = "  Louisa May  Alcott ";
        name = name.trim();

        if(name.isEmpty() || name.isBlank() || name.indexOf(" ") == -1){
            name = "Invalid name";
        }else{
                if (name.indexOf(" ") == name.lastIndexOf(" ")){
                    name = name.substring(0, name.indexOf(" "));
            }else {
                    name = name.substring(0, name.indexOf(" ")) + name.substring(name.lastIndexOf(" "));
            }
        }
        System.out.println(name);
    }
}

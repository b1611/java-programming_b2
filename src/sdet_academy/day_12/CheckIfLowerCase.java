package sdet_academy.day_12;

public class CheckIfLowerCase {
    public static void main(String[] args) {

        String str = "HElLo WoRld"; //11
        //            0123456....
        int count = 0;
        int indexForChar = 0;

        while (count < str.length()) {
            String character = "" + str.charAt(indexForChar);
            String temp = character.toLowerCase();

            if (!character.equals(" ")) {
                if (temp.equals(character)) {
                    System.out.println(character + " - is lower case");
                }else{
                    System.out.println(character + " - is not lower case");
                }
            }else {
                System.out.println(character + " - is not a character");
            }
            count++;
            indexForChar++;
        }
    }
}

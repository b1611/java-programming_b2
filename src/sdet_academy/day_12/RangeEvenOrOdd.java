package sdet_academy.day_12;

import java.util.Random;

public class RangeEvenOrOdd {
    public static void main(String[] args) {
        Random random = new Random();

        int count= 0;


        /**
         * 0 - 0
         * 1 - odd
         * 2 - even
         * 3 - odd
         * ...
         * 19 - odd
         */

        while(count<20){
            int num = random.nextInt(20);
            if (num==0){
                System.out.println("num is 0 - " + num);
            }else if(num%2==1){
                System.out.println("num is odd - " +  num);
            }else {
                System.out.println("num is even - " + num);
            }
            count++;
        }
    }
}

package sdet_academy.day_12;

public class StringManipulationRecap {
    public static void main(String[] args) {

        //join()

        String str = String.join("\\", "My", "name", "is", "Alex");
//        String str = "My" + " " + "name" + " " + "is" + " " + "Alex";

        System.out.println(str);

        String str1 = "Hello World";
        String str2 = "hello";

        System.out.println(str1.startsWith("H"));
        System.out.println(str1.toUpperCase().endsWith("D"));

        str1 = str1.toUpperCase();
        str2 = str2.toLowerCase();
        System.out.println(str1);
        System.out.println(str2);
        String substring = str1;
        System.out.println( substring);

        String replace = str1.replace("L", "R");
        System.out.println(replace);

        String s = str1.replaceFirst("L", "R");
        System.out.println(s);

        str1 = "HELLO Wworld";
        int indexOfW = str1.toUpperCase().indexOf("y");
        int lastIndexOfW = str1.toUpperCase().lastIndexOf("y");
        System.out.println(indexOfW);
        System.out.println(lastIndexOfW);
        System.out.println(str1.length());


        System.out.println(str1.charAt(str1.length()-1));

        String repeat = str1.repeat(5);
        System.out.println(repeat);


    }
}

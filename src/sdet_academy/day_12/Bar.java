package sdet_academy.day_12;

import java.util.Scanner;

public class Bar {
    public static void main(String[] args) {
        /**
         * Create String variable: answer. Also, int variable numberOfDrinks.
         * Using any loop you would like, please create a following program.
         * If customer answers yes for another drink, barman sever him a drink and numberOfDrinks is incrementing
         * If customer says no - print the number of drinks the customer had
         */
        Scanner scanner = new Scanner(System.in);

        String answer= "Yes";
        int numberOfDrinks = -1;

        while (answer.equalsIgnoreCase("Yes") && numberOfDrinks <3){
            System.out.println("Would you like another drink?");
            answer=scanner.nextLine();
            numberOfDrinks++;
        }
        System.out.println("you had " + numberOfDrinks + " drinks" );
    }
}

package sdet_academy.day_38;

public class DogObject {
    public static void main(String[] args) {

        Dog rex = new Dog();
        Dog rex1 = new Dog();  //dead code

        rex.name = "Rex";
        rex.size = "Large";
        rex.breed = "German Shepard";
        rex.age = 5;
        rex.color = "Golden";


        rex.bark();
        rex.eat();
        rex.givesKisses();
        System.out.println(rex.color);


        Dog luna = new Dog();
        System.out.println(luna.color);
        luna.eat();
        luna.bark();

        Dog muhtar = new Dog();

        muhtar.givesKisses();
        muhtar.breed = "Labrador";
        muhtar.givesKisses();
        muhtar.breed = "Doberman";
        muhtar.givesKisses();


    }
}

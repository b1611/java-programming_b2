package sdet_academy.day_38;

public class Cat {
    public String name;
    public String color;
    public String breed = "Persian";


    public void meow() {
        System.out.println(name + " says meow!");
    }

    public void play() {
        System.out.println("Our " + breed + " likes to play. And his name is " + name);
    }

    public void hunt() {
        System.out.println("Our cat's color is " + color + " and it likes to hunt");
    }

}

package sdet_academy.day_38;

public class CatObject {
    public static void main(String[] args) {

        Cat cat = new Cat();
        cat.name = "Leo";
        cat.color = "Black";
        cat.breed = "Bald";

        cat.meow();
        cat.play();
        cat.hunt();

    }
}

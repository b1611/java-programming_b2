package sdet_academy.day_38;

public class ParrotObject {
    public static void main(String[] args) {

        Parrot parrot = new Parrot();

        parrot.name = "Rio";
        parrot.isFlying = false;
        System.out.println(parrot.breed);
        parrot.fly();
        parrot.sleep();

        Parrot parrot2  = new Parrot("Yago", "Tropical Parrot", true, true, 2);
        parrot2.fly();
        parrot2.sleep();
        System.out.println(parrot2.name);
        System.out.println(parrot2.breed);


    }
}

package sdet_academy.day_38;

public class Parrot {
    public String name;
    public String breed;
    public boolean isFlying;
    public boolean isTalking;
    public int age;

    //default/no args constructor
    public Parrot(){}

    public Parrot(String name, String breed, boolean isFlying, boolean isTalking, int age){
        this.name = name;
        this.breed = breed;
        this.isFlying = isFlying;
        this.isTalking = isTalking;
        this.age = age;
    }


    public void fly(){
        System.out.println("Our " + name + " is flying: " + isFlying);
    }

    public void sleep(){
        System.out.println("Our " + name + " is sleeping");
    }

}

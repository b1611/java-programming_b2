package sdet_academy.day_38;

public class Dog {
    //data
    public String name;
    public String breed;
    public String color = "Grey";
    public String size;
    public int age;


    //behavior
    public void bark(){
        System.out.println("The dog " + name + " is barking");
    }

    public void eat(){
        System.out.println("Our dog size is " + size + " and it is eating a lot");
    }

    public void givesKisses(){
        System.out.println("Our dog breed is " + breed + " and it is very friendly");
    }
}
